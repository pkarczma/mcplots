We thank the referee for the constructive comments, which we have
implemented as follows: 

points 1, 2: BOINC is now mentioned already in the introduction, where
LHC@home is first discussed. We also included a new section 7
discussing LHC@home and Test4Theory in more detail. 

point 3: the discussion of the chi2 calculations in section 2.4 has
been enlarged with more detailed comments on the treatment of the 
statistical (MC) uncertainties. We have also included a paragraph
outlining an improved treatment we would like to include as a future
revision, and providing a more explicit disclaimer of what we believe
are the limitations of the current setup, as follows:

"A future revision that could be considered would be to reinterpret
the statistical MC uncertainties in terms of uncertainties on the
calculated chi2 values themselves. This would allow a better
distinction between a truly good description (low chi2 with small
uncertainty) and artificially low chi2 values caused by low MC
statistics (low chi2 with large uncertainty). Such improvements would
certainly be mandatory before making any rigorous conclusions using
this tool, as well as for objective interpretations of direct
comparisons between different generators. For the time being, the tool
is not intended to provide such quantitative discriminations, but
merely to aid authors in making a qualitative assessment of where
their codes and tunes work well, and where there are problems."

point 4: we currently use Rivet 1.x with AIDA. An explicit comment
about this (and mentioning that we will move to Rivet 2.x with YODA in
future) has been included in section 3.1. 

point 5: footnote about SVN access moved to text body in section 3.2. 

point 6: corrected histogram designation mismatches.

point 7: section 4.4.1; made it explicit that tune groups apply globally. 

point 8: Section 5. Emphasized which operations are limited to
official mcplots authors (specifically, all operations on the back end
of the mcplots-dev and mcplots web sites themselves). Also emphasized
that this section is nonetheless not only intended for future mcplots
authors but also as an aid to people who create their own standalone
copies of mcplots from the SVN repository. We see that as a
potentially growing use case in the future, eg for internal validation
in experiments, and for MC pre-release validation by MC authors.

points 9 and 10: we have 

Q: "In Section 3.1, the relation between the
generator files (e.g. the ones in mcrun_alp and utils_alp) and the
existing generator information in runAll.sh is not very clear. As far
as I can tell from a quick look, there is a rungen.sh script bridging
between the two but this should be clarified in the text."

A: inserted additional comments about runAll.sh, rungen.sh, and
runRivet.sh in the beginning of section 6.1 (Implementing a new event
generator). (We assume the referee's mention of section 3.1 (Histogram
and Data Format) was a typo and 6.1 was intended.)

Q: "Also, the lines related to ALPGEN in runAll.sh have two integers
instead of a “-” after $mode $conf. This could also be described."

A: done in section 6 (the param after $mode $conf is reserved for
generator-specific params ; in case of Alpgen we currenly have
Njet,IsExclusive ) 

Q: "Finally, the precise description of how merging with different
runs to fill the phase-space is performed is not totally clear from
the existing description. More precisely, the authors should clarify
how the merging of these samples (e,g, coming from different
multiplicities from ALPGEN, or coming from different pTMin, pTMax
generator cuts) articulate with the entries in rivet-histograms.map
presented in Section 4.3. " 

A: done in subsection 6.2. Several additional references to the
illustration of merging, fig 11, inserted, with more elaborate
comments on which steps in the figure correspond to which steps in the text. 
Various merging schemes are encoded in the merging scripts
(merge.sh). When merging the histogram, the scheme to use is selected
on the basis of the rivet-histograms.map entries, that are encoded/can
be extracted from the source histogram path and name. 

