   MCPLOTS
   =======

Project website:
----------------

  http://mcplots.cern.ch


Code repository:
----------------

  Web access:
    
    WebSVN: http://svnweb.cern.ch/world/wsvn/mcplots/
      Trac: https://svnweb.cern.ch/trac/mcplots/browser
  
  To checkout do:
    
    $ svn co https://svn.cern.ch/reps/mcplots
  
  or (anonymous readonly access):
    
    $ svn co http://svn.cern.ch/guest/mcplots


E-groups:
---------

  https://e-groups.cern.ch/e-groups/EgroupsSearch.do?searchValue=mcplots


Bug tracking:
-------------

  https://its.cern.ch/jira/browse/MCPLOTS


Cloud service (virtual machines):
---------------------------------

  https://openstack.cern.ch/
  (project "PH mcplots")


Machines:
---------
  
  Two machines serving now for the project:
    
    Real name      Alias           Purpose                          System
    ---------------------------------------------------------------------------
    mcplots1       mcplots         public access                    SLC6 x86_64
    
    mcplots2       mcplots-dev     development, BOINC interface     SLC6 x86_64
  
  Both are virtual machines allocated with <https://openstack.cern.ch> service.
  Aliases added through the <http://network.cern.ch> service.
  
  On both machines project files are situated in directory:
    
    /home/mcplots
  


Installation steps:
-------------------
  
  Note: following the instruction you will change several system
  configuration files, please, preserve originals before any change,
  for example:
    
    $ cp httpd.conf httpd.conf.orig
  
  This way you will be able to see changes easily if you will need to
  re-think configuration in future:
  
    $ diff -u httpd.conf.orig httpd.conf
  
  
  Install packages:
    $ ssh mcplots-dev
    $ sudo yum install httpd mysql mysql-server php php-mysql \
                       ImageMagick optipng root root-physics \
                       subversion gcc gcc-c++
  
  Lock installed version of ROOT to avoid plotter break in case of automatic update:
  
    $ rpm -qa | grep ^root- | sudo tee -a /etc/yum/pluginconf.d/versionlock.list
  
  Checkout website files:
    $ cd /home
    $ svn co https://svn.cern.ch/reps/mcplots/trunk mcplots
    $ chmod a+w mcplots
  
  Build plotter tool:
    $ cd /home/mcplots/plotter
    $ make
  
  Run MySQL and configure it:
    $ sudo chkconfig mysqld on
    $ sudo service mysqld start
    $ sudo mysql_secure_installation
  
  Create user mcplots:
    $ mysql --user=root --password=root
    mysql> SELECT Host, User FROM mysql.user;
    mysql> CREATE USER 'mcplots'@'localhost';
    mysql> GRANT ALL PRIVILEGES ON *.* TO 'mcplots'@'localhost';
    mysql> SELECT Host, User FROM mysql.user;
    mysql> quit
  
  Create new database:
    $ mysql -u mcplots
    mysql> CREATE DATABASE mcplots;
    mysql> SHOW DATABASES;
    mysql> quit
  
  Update database:
    $ cd /home/mcplots/www
    
    create symlink dat/ pointing to histograms repository (see section "MC production")
    and update database:
    
    $ ./updatedb.sh
  
  Edit file /etc/httpd/conf/httpd.conf to set web site root directory:
    
    DocumentRoot "/home/mcplots/www"
  
  Run Apache web server:
    $ sudo chkconfig httpd on
    $ sudo service httpd start
  
  Reconfigure firewall to allow HTTP incoming traffic:
    $ sudo system-config-firewall-tui
    
  And disable SELinux:
    $ sudo nano /etc/selinux/config
      (set SELINUX=disabled)
    $ sudo reboot
  
    See more info at:
      http://ask.xmodulo.com/open-port-firewall-centos-rhel.html
      http://www.crypt.gen.nz/selinux/disable_selinux.html
      https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Security-Enhanced_Linux/sect-Security-Enhanced_Linux-Enabling_and_Disabling_SELinux-Disabling_SELinux.html
      http://computernetworkingnotes.com/manage-system-security/how-to-change-selinux-mode.html
  
  Now you can open project page by navigating to
    
    http://mcplots-dev.cern.ch
  
  To setup public machine follow instructions from <READMEupdateServer.txt>.


COPILOT/BOINC interface setup steps (only for mcplots-dev machine):
-------------------------------------------------------------------

  Install additional packages:
    $ sudo yum install xmlstarlet php-json gnuplot rpcbind nfs-utils
  
  Setup NFS share directory for communication with copilot host:
  * contact copilot team to setup access permissions on copilot host.
  
  * specify new mount point in file "/etc/fstab":
    
    t4tc-nfs-1:/nfs/shared/mcplots     /opt/copilot     nfs   _netdev,rw,noatime,soft,timeo=50  0 0
  
  * run NFS services:
    
    $ sudo chkconfig rpcbind on
    $ sudo chkconfig nfslock on
    $ sudo chkconfig nfs on
    $ sudo service rpcbind start
    $ sudo service nfslock start
    $ sudo service nfs start
    
    Reference:
      https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Storage_Administration_Guide/ch-nfs.html
      https://cassjohnston.wordpress.com/2011/12/22/rhel-6-as-nfs-client/
      
  
  * mount shared directory:
  
    $ sudo mkdir -p /opt/copilot
    $ sudo mount /opt/copilot
  
  Setup cron job for MC production machinery:
    $ crontab -e
    
    add line (do run every 30 minutes):
    
      0,30 * * * * /home/mcplots/scripts/cron-update-production.sh >> /home/mcplots/update-production.log 2>&1
    
    save (Ctrl + O) and exit (Ctrl + X)


COPILOT/BOINC cluster control
-----------------------------

  To subscribe a new revision of "scripts/mcprod" for production on BOINC cluster
  (example for revision #1808):
  
    $ ssh mcplots-dev
    $ cd /home/mcplots/scripts
    $ ./control-production.sh set 1808
  
  To check which revisions are subscribed for production navigate to the page:
  
    http://mcplots-dev.cern.ch/production.php?view=control



Performance settings:
---------------------

  - enable MySQL query cache (gives ~ 5 times page generation speedup):
      
      set 64 Mb cache size in file /etc/my.cnf, section [mysqld]:
      
        query_cache_size = 64M
      
      and restart mysql:
      
        $ sudo service mysqld restart
      
      references:
        http://dev.mysql.com/doc/refman/5.0/en/query-cache.html
        http://www.docplanet.org/mysql/mysql-query-cache-in-depth/
        http://www.mysqlperformanceblog.com/2006/07/27/mysql-query-cache/
  
  - increase number of concurrent connections to MySQL server:
      
      The default limit is a maximum of 100 connections which is
      less than the default limit on http connections (~ 250)
      to Apache server and leads to problems in case of high load.
      
      Set the limit of 300 connection in file /etc/my.cnf, section [mysqld]:
      
        max_connections = 300
      
      and restart mysql:
      
        $ sudo service mysqld restart
      
      references:
        http://dev.mysql.com/doc/refman/5.0/en/too-many-connections.html
  
  - disable file access time update:
      
      Each file read operation leads to file write operation (to record
      acces time) which decrease file I/O performance.
      
      To disable the update add the option "noatime" to corresponding
      file system in file "/etc/fstab" and reboot the system.
      
      To check the option is active check the file:
      
        $ cat /proc/mounts
  
  - exclude scanning of nfs share and /home during update of `locate` database
      
      Database of `locate` utility updated each night by full scan of the
      file system. The /home/mcplots contents a huge amount of files
      which leads to file IO performance penalty during the scan.
      
      Edit the file "/etc/updatedb.conf" to updated exclude paths:
        * add "nfs" to PRUNEFS option
        * add "/home" to PRUNEPATHS option
  
  - enable on-the-fly compression of web site content to speed up page load time
      
      Edit /etc/httpd/conf/httpd.conf file, add to end of the file:
      
        AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript application/x-javascript application/json
      
      Restart web server:
      
        $ sudo service httpd restart
      
      reference:
        http://httpd.apache.org/docs/2.2/mod/mod_deflate.html
  
  - do not run X11/graphical login on system start
      
      Graphical login is not necessary, text mode is enough for server operation.
      Set defaul runlevel '3': in the file /etc/inittab change:
      
        id:5:initdefault:
      
      to
      
        id:3:initdefault:
      
      and reboot the system.


Security settings:
------------------

  - deny http access to .svn/ subdirectories:
  
      add to httpd.conf:
      
      <Directory ~ ".*\.svn">
        Order allow,deny
        Deny from all
        Satisfy All
      </Directory>
  
  - disable answer to http TRACE requests:
  
      add to httpd.conf:
      
      TraceEnable off
  
  - comment ScriptAlias and Directory options for /cgi-bin/ path in httpd.conf
  
  - do not use function phpinfo()
  
  - disable network access to mysql (only local requests are allowed):
  
      add following option to file /etc/my.cnf , section [mysqld]:
        
        skip-networking
  
  - disable avahi service (Zeroconf):
    
      /sbin/service avahi-daemon stop
      /sbin/chkconfig avahi-daemon off

  - email updates in log files:
    
      yum install logwatch
      
      nano /etc/sysconfig/yum-autoupdate
      YUMMAIL=0
      
      nano /etc/logwatch/conf/services/http.conf
      # display only summary for 403/404 error codes
      $http_rc_detail_rep-403 = 20
      $http_rc_detail_rep-404 = 20
      
    Reference:
      http://www.linuxquestions.org/questions/linux-software-2/logwatch-too-verbose-don%27t-want-404-messages-869764/#post5207276


Relocation of mcplots-dev
-------------------------

old machine: pcphsft74
new machine: lcggenser4

A) on pcphsft74:

1. remove update cron job:

     $ crontab -e
     
     # remove line with "cron-update-production.sh" job
     # save (Ctrl + O) and exit (Ctrl + X)

2. wait until current update job finished - last line in
   file
   
     /home/mcplots/update-production.log
   
   should be "Update finished"

3. dump the mcplots database:

     $ cd /home/mcplots
     $ mysqldump -u mcplots mcplots > mcplots.sql
     $ gzip mcplots.sql

3. create backup of histograms pool:

     $ cd /home/mcplots
     $ rm -rf pool/failed
     $ tar zcf pool.tgz pool/

B) on lcggenser4:

1. follow all instructions from "Installation steps" section,
   only skip addition of update job to crontab.

2. copy backups:
     $ cd /home/mcplots
     $ scp pcphsft74:/home/mcplots/mcplots.sql.gz .
     $ scp pcphsft74:/home/mcplots/pool.tgz .

3. restore database:
     $ cd /home/mcplots
     $ gunzip mcplots.sql.gz
     $ mysql -u mcplots mcplots < mcplots.sql

4. restore histograms pool:
     $ cd /home/mcplots
     $ tar zxf pool.tgz

5. update site contents, for example with revision #1024:
     $ cd /home/mcplots/www
     $ rm dat
     $ ln -s /home/mcplots/pool/1024/dat
     $ ./updatedb.sh

6. check everything looks fine on:
     http://lcggenser4/
     http://lcggenser4/production.php

7. if everything is fine, add update job to cron
   (follow corresponding instructions from "Installation steps" section)

C) Repoint mcplots-dev alias with <http://network.cern.ch> service.

D) Clean old machine pcphsft74:

1. remove mcplots database:
     $ echo "drop database mcplots" | mysql -u mcplots

2. remove files:
     $ rm -rf /home/mcplots

3. stop web and mysql servers:
    $ /sbin/chkconfig httpd off
    $ /sbin/service httpd stop
    $ /sbin/chkconfig mysqld off
    $ /sbin/service mysqld stop

4. unmount copilot shared directory:
     $ umount /opt/copilot
   
   and remove corresponding mount point from file "/etc/fstab"

5. contact copilot team to disable access permissions for pcphsft74

E) === The End ===


Etc
---
  As of 2012-03-07 the latest version of ImageMagick 6.2.8.0-12.el5
  available from CERN SLC5 software repository breaks the conversion
  from .eps to .png.
  The temporal work-around is to downgrade to the previous version
  and disable automatic update of ImageMagick.
  
  - downgrade to the previous version:
  
    $ sudo yum downgrade ImageMagick
  
  - pin the downgraded version of ImageMagick package to prevent update:
  
    $ sudo su
    $ rpm -qa | grep ImageMagick >> /etc/yum/pluginconf.d/versionlock.list
    $ exit
  
  On 2012-06-25 the ImageMagick package was updated in repository and
  the issue was fixed.
  
  - package unpinned to allow automatic update:
  
    $ sudo su
    $ echo > /etc/yum/pluginconf.d/versionlock.list
    $ yum update
    $ exit


Running MC production:
----------------------

  Machinery to run MC production and produce histograms situated at:
  
    scripts/mcprod
  
  Currently there are following files inside:
    
    - wrapper programs to run generators and Rivet:
        alpgen/
        pythia8/
        vincia/
        phojet/
        rivetvm/
    
    - user's analyses (which are not yet available in Rivet distribution):
        analyses/
    
    - histograms merging tools:
        merge/
    
    - generators steering files, tunes and observables definitions:
        configuration/
    
    - scripts for one single run of MC production (these scripts call / use
      all files mentioned above):
        rungen.sh
        runRivet.sh
    
    - script to do multiple runs:
        runAll.sh
  
  For example, to do a single run of Pythia8 proton-proton 7 TeV Min-Bias, do:
    
    $ ./runRivet.sh local pp uemb-soft 7000 - - pythia8 8.165 tune-4c 1000 31415
  
  Results will be placed in the dat/ subdirectory.
  
  To see description of parameters, do:
    
    $ ./runRivet.sh
  
  To perform multiple runs use `runAll.sh` utility. The utility accepts four
  parameters:
  
  - Mode:
    
    * list     - print list of all runs
    * local    - run production on local machine
    * dryrun   - the same as 'local' but without real execution of generator
                 and Rivet, usefull for testing
    * lxbatch  - run production on CERN LXBATCH service
  
  - Number of events per run
  
  - Filter string (optional, is a grep expression). This is usefull if you want
    to perform only selected runs.
    
    For example, list all runs with 'jets' process:
      
      $ ./runAll.sh list 100k " jets "
    
    List all ee runs of Pythia6 with default tune:
      
      $ ./runAll.sh list 1M "ee .* pythia6 .* default"
    
    Skip this parameter if you want to do all runs.
  
  - LXBATCH queue name (optional, default is 2nd). You need to change this parameter
    if jobs take more time to complete (for example in case of high number of
    events > 1M). To list all available queues:
      
      $ bqueues -u `whoami`
  
  If you change something in MC production machinery (for example, add new analyses),
  please, do 'dryrun' to be sure everything is working:
  
    $ ./runAll.sh dryrun 100
  
  To do full testing you can do local runs with small number of events:
  
    $ ./runAll.sh local 100
  
  When you are sure everything is OK, commit the changes to svn and do full production:
  
    $ ./runAll.sh local 100k
    
    Results will be in dat/ subdirectory.
  
  Currently, total number of all runs is tens of thousands and full production
  takes months on a single machine. Use 'lxbatch' mode to do
  all runs in parallel on LXBATCH which will reduce run time to ~ 1 day:
  
    $ ./runAll.sh lxbatch 100k
    
    Results will be in batch-(date)-(time)/results/ subdirectory
    (one .tgz file per each succesfully finished job).
  
  Note, LXBATCH service is available only from lxplus machines.
  
  To see progress of jobs on LXBATCH, do:
  
    $ bjobs
  
  Once all jobs finished, collect results unpacking tarballs:
  
    $ find batch-(date)-(time)/results -name '*.tgz' | xargs -t -L 1 tar zxf
  
  Results will be in subdirectory dat/.


Preparation of release:
-----------------------
  
  Histograms from various runs of MC production machinery are situated in
  two directories of mcplots-dev machine:
    
    - results of runs on BOINC cluster:
        
        /home/mcplots/pool
      
      directory structure is:
        <revision2>/         - histograms
        <revision1>.tgz      - archived histograms from old revisions
        failed/<revision2>/  - log files of failed jobs
      
    - manual runs and combinations of several runs:
        
        /home/mcplots/release
      
      directory structure is:
      
        <revision1.comment1>/
        <revision2.comment2>/
        ...
  
  The <revision> number mentioned above corresponds to the revision of
  MC production scripts from mcplots repository:
    
    https://svnweb.cern.ch/trac/mcplots/browser/trunk/scripts/mcprod
  
  and is neccessary to give you an idea of exact configuration which
  was used to produce histograms.
  
  In the easest case you can simply use the data provided by BOINC cluster, for example:
    
    $ cd /home/mcplots/www
    $ ln -sf /home/mcplots/pool/1177/dat
    $ ./updatedb.sh
  
  After this commands the site will point to histograms corresponging to revision 1177 of
  the production machinery.
  
  But typically you also want to have intermediate productions of histograms
  following new developments (addition of new generators, analyses, etc).
  In this case you have to do runs manually (see previous section) and put
  results to /home/mcplots/release directory.
  
  For example, you would like to update mcplots-dev with histograms produced
  with newly added generator "epos". First of all submit jobs to produce histograms:
  
    $ ssh lxplus
    $ cd ~/mcplots/scripts/mcprod
    $ ./runAll.sh lxbatch 100k " epos "
    
  Note the revision of production machine which was used to make runs
  (see line "SVN revision"):
  
    $ cat batch-(date)-(time)/info.txt
  
  Lets suppose the revision is 1374.
  
  Once all jobs are finished (see progress with `bjobs` utility) collect results:
  
    $ ssh mcplots-dev
    $ mkdir -p /home/mcplots/release
    $ cd /home/mcplots/release
    $ mkdir 1374.epos
    $ cd 1374.epos
    $ cp ~/mcplots/scripts/mcprod/batch-(date)-(time)/info.txt .
    $ cat info.txt
    $ find ~/mcplots/scripts/mcprod/batch-(date)-(time)/results -name '*.tgz' | xargs -t -L 1 tar zxf
  
  Now all histograms from epos production are situated in:
  
    /home/mcplots/release/1374.epos/dat
  
  Repoint the site to display these histograms:
  
    $ cd /home/mcplots/www
    $ ln -sf /home/mcplots/release/1374.epos/dat
    $ ./updatedb.sh
  
  Navigating to <http://mcplots-dev.cern.ch> you will see the histograms
  from epos production.
  
  The next possibility in release preparation is to stack several productions
  on top of each other. This way you can combine "base" production with high
  statistics (from BOINC cluster) and several small-statistics productions
  with new features. See following script for an example:
  
    scripts/release.sh
  


Adding a new analysis:
----------------------

  To add a new analysis, the analysis has to be implemented in Rivet.
  All available analyses for the current rivet version 1.8.1 can be found in

  /afs/cern.ch/sw/lcg/external/MCGenerators_hepmc2.06.05/rivet/1.8.1/x86_64-slc5-gcc43-opt/share/Rivet/analyses.html

  Notice that for a new Rivet or compiler version, this path differ slightly.
  Once you have picked an analysis, all observables, albeit in teX format, can be
  found in the same directory. Say we wanted to add the DELPHI analysis

  DELPHI_2002_069_CONF_603

  then all the observables can be found in

  /afs/cern.ch/sw/lcg/external/MCGenerators_hepmc2.06.05/rivet/1.8.1/x86_64-slc5-gcc43-opt/share/Rivet/DELPHI_2002_069_CONF_603.plot

  Again notice that for new Rvet version, this path will slighty differ.
  With this as guideline, we can add a new analysis by changing the input table in the
  configuration directory:

    $ vi scripts/mcprod/configuration/rivet-histograms.map

  To add the new analysis to the analysis map, add a line for the new analysis
  in the format

    beam   process_name     energy   pTHatMin,pTHatMax,mHatMin,mHatMax    RivetAnalysisName_AnalysisHistogramName   ObservableName    CutName

  for each histogram in the rivet analysis, e.g. if we want to add the DELPHI
  analysis on b fragmentation, add the lines

    ee    zhad   91.2   -,-,-,-    DELPHI_2002_069_CONF_603_d01-x01-y01   b-f-prim         delphi2002
    ee    zhad   91.2   -,-,-,-    DELPHI_2002_069_CONF_603_d02-x01-y01   b-f-weak         delphi2002
    ee    zhad   91.2   -,-,-,-    DELPHI_2002_069_CONF_603_d04-x01-y01   b-f-prim-mean    delphi2002
    ee    zhad   91.2   -,-,-,-    DELPHI_2002_069_CONF_603_d05-x01-y01   b-f-weak-mean    delphi2002

  where the "-" means that no generation cut will be applied. If all
  generation cuts are irrelevant, just a single "-" will do. 
    
  Here, remember that some analyses include the same observables. If that
  is the case, choose the observable name identical, so that the plots will
  be displayed together.

  When performing a run, the runAll script will try to find input cards for
  the generators ending named according to

    generator-process.params

  If the analysis requires a new process, parameter cards for this process 
  have to be added as well.

  Finally, the correspondence between internal names and names appearing on 
  the web page have to be defined. For this, edit the web page configuration 
  file:

    $ vi www/mcplots.conf

  This file needs to be edited in several places. If a new process has been
  defined, add it to the list of processes in the format

    process_name = ! Name on web page !

  e.g.

    zhad = ! Z(hadronic) !

  Then, for each new observable, define the correspondence between internal
  and external observable name by adding lines in the format

    observable_name = submenu on web page ! observable name on web page !   plotter name in LaTex format

  e.g. 

    b-f-prim  = b fragmentation ! f<sub>prim</sub>  ! b quark fragmentation function f_b^{prim}

   If, which is very likely for a new analysis, a new cut name has been
  defined, add a line defining the cut correspondence in the format

    cut_name = ! Cut name on web page ! Plotter cut name in LaTeX format

  e.g.

    delphi2002 = ! DELPHI  ! DELPHI_{2002}

  to the list of cut definitions.

  After these steps, a new analysis has been added to the production system
  and web page structure. Now, run the MC production and update the data used to 
  generate the page, as indicated in the paragraph "Running MC production".




Adding a new generator
----------------------

  From the point of view of MC production machinery generator looks likes
  a black box which reads input parameters from steering file and writes
  output events to HepMC file:
  
         [steering file]   -->   (GENERATOR)   -->   [HepMC file]
  
  Steering file should consist all neccessary parameters to run generator:
    
    * specification of physics process and cuts
    * number of events, seed of random number generator, etc...
  
  Templates of steering files should be placed to:
    
    scripts/mcprod/configuration/(generator)-(process).params
  
  To attach generator following files/directories should be modified:
    
    * scripts/mcprod/(generator)/
      
      this is optional directory which consist wrapper script/program
      to run generator and is neccessary if, for example, generator
      provided as a library
    
    * scripts/mcprod/rungen.sh
      
      this is generator guiding script which do all the work to decode
      input parameters, prepare steering file, run generator and
      produce HepMC output file
      
      To add new generator follow to:
        
        function run()
        ...
          case "$generator" in
          ...
      
      and add new section. Script in this section should do:
        
        - check input parameters
        - prepare steering file
        - compile wrapper program (optional)
        - prepare command to execute generator program
      
      Take into account that generator which you want to add should be installed
      in CERN repository of MC generators at:
        
        /afs/cern.ch/sw/lcg/external/MCGenerators*
      
      Contact <genser-dev@cern.ch> if this is not the case.
    
    * scripts/mcprod/runAll.sh
      
      this script performs runs with all possible combinations of generator/version/
      beam/energy/... To add new generator edit function "list_runs()"
  
  Finally, to set name of generator which appear on site edit file:
    
    www/mcplots.conf
      section [abbreviations]
        see below comment "# generators names"
