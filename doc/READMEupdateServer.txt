----------------------------
Updating production server
----------------------------

I. For first time you have to add your public key to list of authorized keys of production server
   in order to user updateServer.sh script.
   Procedure described below:

   Sending public key to production server:

     1. if you do not have public_key, create it by:

       $ ssh-keygen -t rsa

       1.a) optionally set name for file storing public key (if you leave  it blank, it will be  called id_rsa.pub)
       1.b) pass phrase leave blank (just press Enter)
      
     2. copy your public key to authorized keys of lcggenser1.cern.ch

       $ cat ~/.ssh/id_rsa.pub | ssh mcpdevel@lcggenser1.cern.ch

   NOTE: if you set name of file storing public key different than id_rsa.pub, you will have to change the name
         of id_rsa.pub to your entered name (in the command above).


II. Run updateServer.sh script to update it.

    $  cd /home/mcplots/scripts
    $  ./updateServer.sh

    By option -r XXX : It is possible to set the revision number of SVN. The server will be 
    updated to this revision. Omitting this option, number will be set to max SVN revision number.
    
    By option -d /path/to/dat : It is possible to set path to dat directory, which stores new .dat  (.params)
    files. INCLUDE dat directory at the end. Without this option only code of
    server's scripts will be updated.

III.EXAMPLES
    ####################
    - Update code of server scripts to last revision:      

    $ ./updateServer.sh

    ####################
    - Update code of server scripts to certain revision:      

    $ ./updateServer.sh -r 285
  
    ####################
    - Update dat directory with new .dat (.params) files:      

    $ ls /path/to/dir/
    .
    ..
    dat
    other...

    $ ./updateServer.sh -d /path/to/dir/dat

    #####################
    - Update code and data:
    
    $ ./updateServer.sh -r 285 -d /path/to/dir/dat
    

