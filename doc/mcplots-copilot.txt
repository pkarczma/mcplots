   MCPLOTS - COPILOT
   =================

Interface
---------

  Copilot is a job submission interface for the LHC@home 2.0 / Test4Theory
  project. Mcplots uses copilot to run jobs on LHC@home 2.0 volunteers cloud.
  
  Following is a job flow between mcplots and copilot:
  
    [mcplots] 1-> [copilot] 2-> [volunteers] 3-> [copilot] 4-> [mcplots]
  
  1. mcplots upload jobs to copilot (to input queue)
  2. copilot distribute jobs to volunteers for execution
  3. copilot collect completed job from volunteers
  4. mcplots download completed jobs (from output queue)
  
  Communication between mcplots and copilot (paths <1> and <4>) is organized
  through NFS shared directory.
  
  Currently the NFS share root for mcplots is:
  
    t4tc-nfs-1:/nfs/shared/mcplots
  
  There are following subdirectories:
  
    input/          - input queue
    
    output/         - output queue
    
    state/          - various information on copilot state  (updates each minute)
      jm_*              - current lengths of job managers internal queues
      volunteers        - history of number of volunteers machines connected to copilot
  
  To submit the job to copilot simply copy the job to the input/ directory:
  
    $ cp job1.run input/
  
  To get the result of completed job copy back file from the output/ directory:
  
    $ mv output/res1.tgz ~/results/


Environment
-----------

  On volunteer machine the job is executed inside the "BOINC" edition of the
  CernVM (http://cernvm.cern.ch) virtual machine which is a minimal SLC5 system
  with access to "CernVM File System" network file system.
  
  Each virtual machine has the following configuration:
    * 10 Gb of disk space (~ 8 Gb available for job)
    * 256 Mb of memory (~ 150 Mb available for job)
    * 2 CPU (or 1 if the host has only 1 CPU)
    * network access (but could be very limited)
    * runtime is 24 hours
  
  Job execution inside vistual machine is controlled by agent which:
    1. query copilot for new job
    2. download job to a temporary directory
    3. run job and wait until it is finished
    4. compress the contents of the temporary directory and
       send the tarball back to copilot
  
  Job itself should be an executable compatible with SLC5 system, for example
  shell script, binary, etc. This is up to the job to setup any specific environment.
  Additional files could be picked either through cvmfs file system or packaged
  inside the job file.
  
  The best is if the job mostly require CPU but not disk or network I/O.


copilot-config
--------------

  The script `copilot-config` is available for job during execution to
  communicate the job state for progress visualisation.
  
  The script has following options:
  
    option           purpose
    ------           -------
  --stdout-file      : return path to file there the job should redirect stdout stream
  --stderr-file      : -/- stderr stream
  --html-dir         : job progress output directory (served by http server so that volunteers
                         can see the job progress)
  --print-logs       : print various logs to debug CVMFS
  --user-data        : print BOINC data (user id, machine id, etc.)
