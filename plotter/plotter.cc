// c++
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>

// ROOT
#include <TCanvas.h>
#include <TGraphAsymmErrors.h>
#include <TROOT.h>
#include <TError.h>
#include <TStyle.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TString.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TPad.h>
#include <TColor.h>
#include <TPRegexp.h>

using namespace std;

bool debug = false;
const int UNDEFINED = -999; // marker for parameters which are not defined in steering files

// convert string to value
template<typename T>
T cast(const string& s)
{
  istringstream iss(s);
  T x;
  iss >> x;
  return x;
}


TString Latex2Root(TString latex)
{
  if (debug)
    cout << "Latex2Root()\n"
         << "  in = " << latex << endl;
  
  latex.ReplaceAll("~", " ");
  latex.ReplaceAll("$", "");
  latex.ReplaceAll("\\;", "");
  latex.ReplaceAll("rangle", "#GT");
  latex.ReplaceAll("langle", "#LT");
  latex.ReplaceAll("\\rightarrow", " #rightarrow ");
  latex.ReplaceAll("\\to", " #rightarrow ");
  latex.ReplaceAll(" to ", " #rightarrow ");
  latex.ReplaceAll("\\perp", "T");
  latex.ReplaceAll("perp", "T");
  latex.ReplaceAll("rm T", "T");
  latex.ReplaceAll("\\left", "");
  latex.ReplaceAll("\\right", "");
  latex.ReplaceAll("\\mathrm", "");
  //latex.ReplaceAll("\\mathcal","");
  latex.ReplaceAll("\\!", "");
  latex.ReplaceAll("\\cos", "cos");
  latex.ReplaceAll("\\ln", "ln");
  latex.ReplaceAll("\\log", "log");
  latex.ReplaceAll("\\millibarn", "mb");
  latex.ReplaceAll("millibarn", "mb");
  latex.ReplaceAll("\\microbarn", "#mub");
  latex.ReplaceAll("microbarn", "#mub");
  latex.ReplaceAll("\\rpsquare\\GeV", "/GeV^{2}");  
  
  TPRegexp("\\\\,").Substitute(latex, " ", "g");
  TPRegexp("unit\{\([^}]+)}").Substitute(latex, "[$1]", "g");
  latex.ReplaceAll("[[", "[");  
  latex.ReplaceAll("]]", "]");  
  latex.ReplaceAll("_\\text{", "_{");  
  latex.ReplaceAll("^\\text{", "^{");  
  latex.ReplaceAll("\\text{", "{");  
  
  // Note: these expressions will terminate at the next } encountered,
  // so will fail if arguments contain }
  // Temporary solution is to identify such cases one by one and remove
  // the extraneous } already above. 
  // Note: \bla is matched by the regexp \\bla is matched by \\\\bla in C++
  TPRegexp("\\\\mathrm{\([^}]+)}").Substitute(latex, "$1", "g");
  TPRegexp("mathrm{\([^}]+)}").Substitute(latex, "$1", "g");
  TPRegexp("\\\\mathcal{\([^}]+)}").Substitute(latex, "$1", "g");
  TPRegexp("mathcal{\([^}]+)}").Substitute(latex, "$1", "g");
  TPRegexp("\\_text{\([^}]+)}").Substitute(latex,"_{$1}", "g");
  TPRegexp("\\^text{\([^}]+)}").Substitute(latex, "^{$1}", "g");
  TPRegexp("\\\\text{\([^}]+)}").Substitute(latex, "$1", "g"); 
  TPRegexp("text{\([^}]+)}").Substitute(latex, "$1", "g");
  TPRegexp("\\\\frac{\([^}]+)}").Substitute(latex, "#frac{$1}", "g");
  // Needed by test14
  TPRegexp("frac{\([^}]+)}").Substitute(latex, "#frac{$1}", "g");
  
  // Differentials
  latex.ReplaceAll("{d}", "d");  
  TPRegexp("d{\([^}]+)}").Substitute(latex, "d$1", "g");

  // Sums
  TPRegexp("sum{\([^}]+)}").Substitute(latex, "sum($1)", "g");
  
  // Translation of _x to _{x}
  TPRegexp("_(\\\\*[a-zA-Z]+)").Substitute(latex, "_{$1}", "g");
  latex.ReplaceAll("_{mu}", "_{#mu}");   // See test15

  // Translation of ^x to ^{x}
  TPRegexp("\\^\([^{}])").Substitute(latex, "^{$1}", "g");
  
  TPRegexp("bar \([^ ]+)").Substitute(latex, "bar{$1}", "g");

  // Remove spurious {} and ; characters 
  latex.ReplaceAll("{}", "");
  latex.ReplaceAll("; ", " ");
  latex.ReplaceAll(" ;", " ");
  latex.ReplaceAll(" , ", " ");
  latex.ReplaceAll("()", "");

  // translate LaTeX control character '\' to ROOT's LaTeX control character '#'
  latex.ReplaceAll("\\", "#");

  // Catch any characters that were not prefaced by '\'
  latex.ReplaceAll("#sigma", "sigma");
  latex.ReplaceAll("sigma", "#sigma");
  latex.ReplaceAll("#Sigma", "Sigma");
  latex.ReplaceAll("Sigma", "#Sigma");
  latex.ReplaceAll("#xi_", "xi_");
  latex.ReplaceAll("xi_", "#xi_");
  latex.ReplaceAll("#Xi", "Xi");
  latex.ReplaceAll("Xi", "#Xi");
  latex.ReplaceAll("#Lambda", "Lambda");
  latex.ReplaceAll("Lambda", "#Lambda");
  latex.ReplaceAll("deltaeta", "Delta #eta");
  latex.ReplaceAll("#Delta", "Delta");
  latex.ReplaceAll("Delta", "#Delta");
  latex.ReplaceAll("#eta", "eta");
  latex.ReplaceAll("eta", "#eta");
  latex.ReplaceAll("#phi", "phi");
  latex.ReplaceAll("phi", "#phi");
  latex.ReplaceAll("2pi", "2#pi");
  latex.ReplaceAll("#geq", "geq");
  latex.ReplaceAll("geq", "#geq");
  latex.ReplaceAll("#rho", "rho");
  latex.ReplaceAll("rho", "#rho");
  latex.ReplaceAll("#mub", "mub");
  latex.ReplaceAll("mub", "#mub");
  latex.ReplaceAll("reciprocalGeV", "GeV^{-1}");  
  latex.ReplaceAll("#rpsquare#GeV", "/GeV^{2}");  
  latex.ReplaceAll("#GeV", "GeV");  
  // Use natural units
  latex.ReplaceAll("GeV/c", "GeV");  
  latex.ReplaceAll("{GeV}/c", "GeV");  // See test7

  if (debug)
    cout << "  out = " << latex << endl;
  
  return latex;
}

struct RGBColor {
  RGBColor():
    red(0), green(0), blue(0)
  {}
  
  double red;
  double green;
  double blue;
};

RGBColor getRGBfromString(const string& s)
{
    RGBColor rgb;
    istringstream ss(s);

    ss >> rgb.red >> rgb.green >> rgb.blue;
    if (ss.fail( )) {
        throw ("not a color");
    }

    return rgb;
}

// create new color in palette
int prepareColor(const RGBColor& rgb)
{
  // starting index for user colors:
  static Int_t index = 1700;
  
  // create new color in palette:
  TColor* col = new TColor(index, rgb.red, rgb.green, rgb.blue);
  
  // increase index for further use
  index++;
  
  return col->GetNumber();
}


struct Bin {
  Bin():
    xlow(0), xfocus(0), xhigh(0),
    yval(0), yerrminus(0), yerrplus(0)
  {}
  
  double xlow;
  double xfocus;
  double xhigh;
  double yval;
  double yerrminus;
  double yerrplus;
  
  bool isEmpty() const {
    return ((yval == 0.) && (yerrminus == 0.) && (yerrplus == 0.));
  }
  
  // check bin edges:
  bool sameEdges(const Bin& b) const {
    return ((fabs(xfocus - b.xfocus) < 1e-4) &&
            (fabs(xlow   - b.xlow)   < 1e-4) &&
            (fabs(xhigh  - b.xhigh)  < 1e-4));
  }
  
  // reset bin content
  void reset() {
    yval = 0;
    yerrminus = 0;
    yerrplus = 0;
  }
  
  double y() const {
    return yval;
  }

  double ylow(double fac=1) const {
    if (yval >= 0.) return max(yval/10., yval - fac*yerrminus);
    else return max(yval*10, yval - fac*yerrminus);
  }
  
  double yhigh(double fac=1) const {
    if (yval >= 0.) return min(pow(10.,fac)*yval,yval + fac*yerrplus);
    else return min(yval/pow(10.,fac),yval + fac*yerrplus);
  }
};

typedef map<string, string> PropMap;

struct Hist {
  Hist():
    MarkerStyle(1), MarkerSize(1),
    LineStyle(1), LineWidth(1),
    Reference(0),
    nevts(0),
    LogY(UNDEFINED), LogX(UNDEFINED)
  {}
  
  // section "HISTOGRAM" from .script file
  PropMap prop;
  string Filename;
  string PathToDataFile;
  int MarkerStyle;
  float MarkerSize;
  int LineStyle;
  int LineWidth;
  RGBColor Color;
  string Legend;
  int Reference;
  
  // section "HISTOGRAM" from .dat file
  string XLabel;
  string YLabel;
  string Title;
  
  // bins from .dat file
  vector<Bin> bins;
  
  // section "METADATA" from .dat file
  int nevts;
  string rivet;
  
  // section "PLOT" from .dat file
  int LogY;
  int LogX;
};

typedef vector<Hist> Histograms;

struct Plots
{
  Plots():
    LogY(UNDEFINED), LogX(UNDEFINED),
    XAxisMin(UNDEFINED), XAxisMax(UNDEFINED),
    YAxisMin(UNDEFINED), YAxisMax(UNDEFINED),
    DrawGrid(0),
    DrawErrorBarX(0),
    DrawRatioPlot(0)
  {}
  
  // data from "HISTOGRAM" sections of .script file
  // and data from corresponding .dat file
  Histograms histos;
  
  // data from "PLOT" section of .script file
  string XLabel;
  string YLabel;
  string Title;
  int LogY;
  int LogX;
  float XAxisMin;
  float XAxisMax;
  float YAxisMin;
  float YAxisMax;
  string UpperLeftLabel;
  string UpperRightLabel;
  string TextField1;
  string TextField2;
  string OutputFileName;
  int DrawGrid;
  int DrawErrorBarX;
  int DrawRatioPlot;
  
  
  // return index of a reference histogram
  int getRefIndex() const
  {
    for (size_t i = 0; i < histos.size(); ++i)
      if (histos[i].Reference == 1)
        return i;
    
    return 0;
  }
  
  
  // decide position of legend
  // return:
  //   false - left
  //   true  - right
  bool getLegendPosition() const
  {
    double yvalMaxPart1 = -100000000;
    double yvalMaxPart2 = -100000000;
    
    for (size_t i = 0; i < histos.size(); ++i) {
      const vector<Bin>& bins = histos[i].bins;
      const size_t mid = bins.size() / 2;
      
      for (size_t j = 0; j < bins.size(); ++j) {
        if (bins[j].isEmpty()) continue; // skip empty bin
        
        const double yval = bins[j].yval;
        if (j < mid && yval > yvalMaxPart1) yvalMaxPart1 = yval;
        if (j > mid && yval > yvalMaxPart2) yvalMaxPart2 = yval;
      }
    }
    
    if (debug)
      cout << "getLegendPosition()\n"
           << "  yvalMaxPart1 = " << yvalMaxPart1 << "\n"
           << "  yvalMaxPart2 = " << yvalMaxPart2 << endl;
    
    return (yvalMaxPart1 > yvalMaxPart2);
  }
  
  
  // function decides plots scale based on histograms content
  bool getScaleAutomatic() const
  {
    // extract min and max values of all plots
    double ymax = -10000000.;
    double ymin0 =  10000000.;    
    
    for (size_t i = 0; i < histos.size(); ++i) {
      // Skip MC histograms if last one was data
      //if (i >= 1 && histos[i-1].Reference == 1) break;
      const vector<Bin>& bins = histos[i].bins;
      
      for (size_t j = 0; j < bins.size(); ++j) {
        if (bins[j].isEmpty()) continue; // skip empty bins
        
        const double yval = bins[j].yval;
        if (yval > ymax) ymax = yval;
        if ((yval < ymin0) && (yval > 0.)) ymin0 = yval;
      }
    }
    
    if (debug)
      cout << "getScaleAutomatic()\n"
           << "  ymax = " << ymax << "\n"
           << "  ymin0 = " << ymin0 << endl;
    
    // advice to use logarithmic scale if difference between min and max values
    // is more than one order of magnitude (factor 20: log10 = 1.3)
    return (fabs(log10(ymax) - log10(ymin0)) > 1.3);
  }
  
  // decide plots Y scale
  // return:
  //   false - linear
  //   true  - logarithmic
  bool getScaleY() const
  {    
    if (debug)
      cout << "getScaleY()\n"
           << "  LogY = " << LogY << "\n";
    
    // scale specified in plotter steering file (.script):
    if (LogY != UNDEFINED)
      return LogY;
    
    // scale specified in one of histograms (.dat)
    for (size_t i = 0; i < histos.size(); ++i) {
      if (debug)
        cout << "  histos[" << i << "].LogY = " << histos[i].LogY << " .Reference = " << histos[i].Reference << "\n";
      
      // skip DATA histograms as they always have "LogY=1" (at least for Rivet 1.7.0)
      if (histos[i].Reference == 1)
        continue;
      
      if (histos[i].LogY != UNDEFINED)
        return histos[i].LogY;
    }
    
    // evaluate scale based on histograms content
    return getScaleAutomatic();
  }
  
  // decide plots X scale
  // return:
  //   false - linear
  //   true  - logarithmic
  bool getScaleX() const
  {
    if (debug)
      cout << "getScaleX()\n"
           << "  LogX = " << LogX << "\n";
    
    // scale specified in plotter steering file (.script):
    if (LogX != UNDEFINED)
      return LogX;
    
    // scale specified in one of histograms (.dat)
    for (size_t i = 0; i < histos.size(); ++i) {
      if (debug)
        cout << "  histos[" << i << "].LogX = " << histos[i].LogX << " .Reference = " << histos[i].Reference << "\n";
      
      // skip DATA histograms as information on "LogX" parameter is missing (at least for Rivet 1.8.1)
      if (histos[i].Reference == 1)
        continue;
      
      if (histos[i].LogX != UNDEFINED)
        return histos[i].LogX;
    }
    
    // by default use linear scale
    return false;
  }
  
  // extract histogram as TGraphAsymmErrors
  TGraphAsymmErrors* getGraph(const int num) const
  {
    const int MAX = 1000;
    
    int n = 0;
    float x[MAX];
    float y[MAX];
    float yem[MAX];
    float yep[MAX];
    float xem[MAX];
    float xep[MAX];
    
    const vector<Bin>& bins = histos[num].bins;
    
    if (debug)
      cout << "getGraph()\n"
           << "  Histo #" << num << ": nBins = " << bins.size() << endl;

    // Check if there is only one bin with content
    int nBinsFilled = 0;
    for (size_t i = 0; i < bins.size(); ++i) {
      const Bin& b = bins[i];
      
      if (b.isEmpty()) continue; // skip empty bins
      ++nBinsFilled;
    }

    // If only one bin is filled, no line will be drawn on histograms.
    // So draw X error bars instead.
    int DrawErrorBarXnow = DrawErrorBarX;
    if (nBinsFilled == 1) DrawErrorBarXnow = 1;
    
    for (size_t i = 0; i < bins.size(); ++i) {
      const Bin& b = bins[i];
      
      if (b.isEmpty()) continue; // skip empty bins
      
      x[n] = b.xfocus;
      y[n] = b.yval;
      yem[n] = fabs(b.yerrminus);
      yep[n] = fabs(b.yerrplus);

      if (DrawErrorBarXnow) {
        xem[n] = fabs(b.xfocus - b.xlow);
        xep[n] = fabs(b.xhigh - b.xfocus);
      }
      else {
        xem[n] = 0.;
        xep[n] = 0.;
      }
      
      n++;
    }
    
    TGraphAsymmErrors* gr = new TGraphAsymmErrors(n, x, y, xem, xep, yem, yep);
    return gr;
  }
  
  
  // extract histograms ratio as TGraphAsymmErrors
  TGraphAsymmErrors* getRatioGraph(const int num, const int ref) const
  {
    const int MAX = 1000;
    
    int n = 0;
    float x[MAX];
    float y[MAX];
    float yem[MAX];
    float yep[MAX];
    float xem[MAX];
    float xep[MAX];
    
    const vector<Bin>&  bins = histos[num].bins;
    const vector<Bin>& rbins = histos[ref].bins;
    
    if (bins.size() != rbins.size()) {
      // input histograms incompatible!
      cout << "ERROR: bins number do not match! Histograms " << num << " vs " << ref << endl;
      // return empty histo
      return new TGraphAsymmErrors; 
    }
    
    if (debug)
      cout << "getRatioGraph()\n"
           << "  Histo #" << num << ": nBins = " << bins.size() << endl;
    
    // Check if there is only one bin with content
    int nBinsFilled = 0;
    for (size_t i = 0; i < bins.size(); ++i) {
      const Bin& b = bins[i];
      
      if (b.isEmpty()) continue; // skip empty bins
      ++nBinsFilled;
    }

    // If only one bin is filled, no line will be drawn on histograms.
    // So draw X error bars instead.
    int DrawErrorBarXnow = DrawErrorBarX;
    if (nBinsFilled == 1) DrawErrorBarXnow = 1;

    for (size_t i = 0; i < bins.size(); ++i) {
      const Bin& b =  bins[i];
      const Bin& r = rbins[i];
      
      if (b.isEmpty() || r.isEmpty()) continue; // skip empty bins
      
      // check bin edges:
      if (! b.sameEdges(r)) {
        cout << "ERROR: bins edges do not match! Histograms " << num << " vs " << ref << ". Bin " << i << endl;
        continue; // skip different bins
      }
      
      x[n] = b.xfocus;
      y[n] = b.yval / r.yval;
      yem[n] = fabs(b.yerrminus / r.yval);
      yep[n] = fabs(b.yerrplus / r.yval);
      
      if (DrawErrorBarXnow || (num == ref)) {
        xem[n] = fabs(b.xfocus - b.xlow);
        xep[n] = fabs(b.xhigh - b.xfocus);
      }
      else {
        xem[n] = 0.;
        xep[n] = 0.;
      }
      
      n++;
    }
    
    TGraphAsymmErrors* gr = new TGraphAsymmErrors(n, x, y, xem, xep, yem, yep);
    return gr;
  }
  
  // calculate Chi2/Ndof for two histograms
  // [num] - index of theory histogram (MC)
  // [ref] - index of reference histogram (DATA)
  // [uncertainty] - value of "theory uncertainty"
  double calcNormChi2(const int num, const int ref, const double uncertainty) const
  {
    const vector<Bin>&  bins = histos[num].bins;
    const vector<Bin>& rbins = histos[ref].bins;
    
    if (debug) {
      cout << "Comparing histogram #" << num << " (mc, #bins = " << bins.size() << ") with "
           << "#" << ref << " (data, #bins = " << rbins.size() << ")" << endl;
    }
    
    if (bins.size() != rbins.size()) {
      // input histograms incompatible: different number of bins
      cout << "ERROR: bins number do not match in histograms." << endl;
      return -1; 
    }
    
    double Chi2 = 0;
    size_t N = 0;
    
    for (size_t i = 0; i < bins.size(); ++i) {
      const Bin& b =  bins[i];
      const Bin& r = rbins[i];
      
      // Require same bins filled. If data is filled and MC is not filled,
      // we do not know what the chi2 of that bin is. Return error.
      if (b.isEmpty() && !r.isEmpty()) {
        cout << "ERROR: empty bin #" << i << " detected in mc histogram" << endl;
        
        return -1;
      }
      
      // Skip empty bins (if data is empty but theory is filled, it's ok. We
      // are allowed to plot theory outside where there is data, we just 
      // cannot calculate a chi2 there).
      if (b.isEmpty() || r.isEmpty()) continue;
      
      // skip different bins (check bin edges)
      if (! b.sameEdges(r)) {
        cout << "ERROR: bins edges do not match for bin #" << i << endl;
        continue; 
      }
      
      // compute one element of test statistics:
      //                     (Theory - Data)^2
      // X = --------------------------------------------------------
      //      Sigma_data^2 + Sigma_theory^2 + (uncertainty*Theory)^2
      
      const double Theory = b.yval;
      const double Data = r.yval;
      
      const double Sigma_theory = (Theory > Data) ? b.yerrminus : b.yerrplus;
      const double Sigma_data   = (Theory > Data) ? r.yerrplus : r.yerrminus;
      
      const double nomin = (Theory - Data) * (Theory - Data);
      const double denom = Sigma_data*Sigma_data + 
                           Sigma_theory*Sigma_theory +
                           (uncertainty*Theory) * (uncertainty*Theory);
      
      // TODO: handle (denom == 0)
      const double X = nomin/denom;
      Chi2 += X;
      N++;
    }
    
    // TODO: calculate NDOF properly (decrease by 1) if histograms
    //       area was normalized to the constant
    const size_t Ndof = N;
    
    // there is no any correct bin
    if (Ndof == 0) {
      cout << "ERROR: no bins to compare " << num << " vs " << ref << endl;
      return -2;
    }
    
    return Chi2/Ndof;
  }
  
  string getRivetVersion() const
  {
    for (size_t i = 0; i < histos.size(); ++i) {
      const string& rivet = histos[i].rivet;
      if (rivet != "") return rivet;
    }
    
    return string();
  }
  
  // return string with the number of events in the plot
  // (number of events corresponds to the histogram with lowest statistics)
  string getNevents() const
  {
    if (debug)
      cout << "getNevents()" << endl;
    
    if (histos.size() == 0) return string();
    
    // find minimum number of events (but above zero)
    int min0 = 0;
    
    for (size_t i = 0; i < histos.size(); ++i) {
      const int nevts = histos[i].nevts;
      
      if (debug)
        cout << "  Hist #" << i << ": nEvents = " << nevts << endl;
      
      if ((min0 == 0) || ((nevts > 0) && (nevts < min0))) min0 = nevts;
    }
    
    if (min0 == 0) return string();
    
    // determine unit multiplier and prefix
    const bool less1e6 = (min0 < 1000000);
    const float  mult = less1e6 ? 1000 : 1000000;
    const string unit = less1e6 ? "k" : "M";
    
    // following is to have the default precision of 2 digits,
    // but to avoid the fallback of ostringstream to scietific format
    // if integer of val have in fact three digits
    const float val = min0/mult;
    const streamsize nprec = (val < 100) ? 2 : 3;
    
    // prepare the string with number of events
    ostringstream oss;
    oss.precision(nprec);
    oss << "#geq " << val << unit << " events";
    
    return oss.str();
  }
  
  // Sometimes there are gaps in the data histograms.
  // Rivet/AIDA can't deal with gaps currently, which
  // gives MC histograms with more number of bins
  // than in corresponding DATA histogram.
  // The Rivet solution for the issue is to post-process
  // MC histograms with `rivet-rmgaps' utility which
  // remove 'gap' bins to adjust MC and DATA histograms
  // to each other.
  // 
  // Following function has the same purpose, but implemented
  // by inserting of empty bins into the DATA (reference) histogram.
  void insertGaps()
  {
    if (histos.size() < 2) return;
    
    // get indices of DATA and MC histograms
    const int data_ind = getRefIndex();
    const int mc_ind = (data_ind == 0) ? 1 : 0;
    
          vector<Bin>& data = histos[data_ind].bins;
    const vector<Bin>& mc   = histos[mc_ind].bins;
    
    // continue only if DATA has less bins than MC
    if (data.size() >= mc.size()) return;
    
    if (debug)
      cout << "insertGaps()\n"
           << "  #data bins = " << data.size() << "\n"
           << "  #mc bins   = " << mc.size() << endl;
    
    for (size_t i = 0; (i + 1) < mc.size(); ++i) {
      // check match between current MC bin and current DATA bin
      if (mc[i].sameEdges(data[i])) continue;
      
      // check the next MC bin match current DATA bin to be sure
      // we find a gap
      if (! mc[i + 1].sameEdges(data[i])) break;
      
      Bin bin = mc[i];
      bin.reset();
      
      // shift content of data vector to have place for new bin:
      data.resize(data.size() + 1);
      for (size_t j = data.size(); j > i; --j) data[j] = data[j - 1];
      
      // put new bin
      data[i] = bin;
    }
    
    if (debug)
      cout << "insertGaps()\n"
           << "  #new data bins = " << data.size() << "\n"
           << "  #new mc bins   = " << mc.size() << endl;
  }
};


Hist readDataFile(const char* fname)
{
  if (debug)
    cout << "readDataFile()\n"
         << "  fname = " << fname << endl;
  
  ifstream f(fname);
  if (!f) {
    cerr << "ERROR: failed to open file " << fname << endl;
    return Hist();
  }
  
  Hist hist;
  string line, section;
  
  // read file line by line
  while (getline(f, line)) {
    // new section
    if (line.find("# BEGIN") == 0) {
      istringstream iss(line.substr(7));
      iss >> section;
      continue;
    }
    
    // end section
    if (line.find("# END") == 0) {
      section = "";
      continue;
    }
    
    // skip out-of-section space
    if (section == "") continue;
    
    // skip comments
    if (line.find("#") != string::npos) continue;
    
    // section properties
    const size_t eqpos = line.find("=");
    const bool isProperty = (eqpos != string::npos);
    
    if (isProperty) {
      // there is a '=' sign in the line, so this is a property string: key=value
      const string key = line.substr(0, eqpos);
      const string value = line.substr(eqpos + 1);
      
      if (debug)
        cout << "  " << section << " : " << key << " = " << value << endl;
      
      if (section == "METADATA") {
        if (key == "nevts") hist.nevts = cast<int>(value);
        if (key == "rivet") hist.rivet = value;
      }
      else if (section == "PLOT") {
        if (key == "Title")  hist.Title  = Latex2Root(value);
        if (key == "XLabel") hist.XLabel = Latex2Root(value);
        if (key == "YLabel") hist.YLabel = Latex2Root(value);
        if (key == "LogY") hist.LogY = cast<int>(value);
        if (key == "LogX") hist.LogX = cast<int>(value);
      }
      
      continue;
    }
    
    // starting from Rivet 2.4.0 data histograms have section name "HISTO1D"
    if (section == "HISTOGRAM" || section == "HISTO1D") {
      // this is bin content
      istringstream ss(line);
      Bin bin;
      ss >> bin.xlow >> bin.xfocus >> bin.xhigh >> bin.yval >> bin.yerrminus >> bin.yerrplus;
      hist.bins.push_back(bin);
      
      if (ss.bad()) {
        cerr << "ERROR: fail to read bin content\n"
             << "       " << line << "\n"
             << endl;
      }
    }
  }
  
  return hist;
}


Plots read(const char* fname)
{
    if (debug)
      cout << "read()\n"
           << "  fname = " << fname << endl;
    
    ifstream f(fname);
    if (!f) {
      cerr << "ERROR: failed to open file " << fname << endl;
      return Plots();
    }
    
    Plots plots;
    Histograms histograms;
    string line;
    
    // read file line by line
    while (getline(f, line)) {
        //header block
        if (line.find("# BEGIN PLOT") == 0) {
            while (getline(f, line)) {
                const size_t endpos = line.find("# END PLOT");
                if (endpos == 0) break;
                
                const size_t hashpos = line.find("#");
                if (hashpos == 0) continue;
                
                const size_t eqpos = line.find("=");
                if (eqpos != string::npos) {
                    // if where is '=' sign in line, this is a property string: name=value
                    const string name = line.substr(0, eqpos);
                    const string value = line.substr(eqpos + 1);
                    // hist.prop[name] = value;
                    //  cout << " value name: " << name << endl;
                    if (name == "XLabel")          plots.XLabel = Latex2Root(value);
                    if (name == "YLabel")          plots.YLabel = Latex2Root(value);
                    if (name == "Title")           plots.Title = Latex2Root(value);
                    if (name == "LogY")            plots.LogY = cast<int>(value);
                    if (name == "LogX")            plots.LogX = cast<int>(value);
                    if (name == "XAxisMin")        plots.XAxisMin = cast<float>(value);
                    if (name == "XAxisMax")        plots.XAxisMax = cast<float>(value);
                    if (name == "YAxisMin")        plots.YAxisMin = cast<float>(value);
                    if (name == "YAxisMax")        plots.YAxisMax = cast<float>(value);
                    if (name == "upperLeftLabel")  plots.UpperLeftLabel = value;
                    if (name == "upperRightLabel") plots.UpperRightLabel = value;
                    if (name == "textField1")      plots.TextField1 = value;
                    if (name == "textField2")      plots.TextField2 = value;
                    if (name == "outputFileName")  plots.OutputFileName = value;
                    if (name == "drawGrid")        plots.DrawGrid = cast<int>(value);
                    if (name == "drawErrorBarX")   plots.DrawErrorBarX = cast<int>(value);
                    if (name == "drawRatioPlot")   plots.DrawRatioPlot = cast<int>(value);
                }
            }
        }

        // histogram block
        if (line.find("# BEGIN HISTOGRAM") == 0) {
            //cout << " Begin found " << endl;
            Hist hist;
            // extract name
            //hist.name = line.substr(18); // index = length("# BEGIN HISTOGRAM") + 1

            // read histogram data
            while (getline(f, line)) {
                const size_t endpos = line.find("# END HISTOGRAM");
                if (endpos == 0) break;

                const size_t hashpos = line.find("#");
                if (hashpos != string::npos) continue;

                const size_t eqpos = line.find("=");
                if (eqpos != string::npos) {
                    // if where is '=' sign in line, this is a property string: name=value
                    const string name = line.substr(0, eqpos);
                    const string value = line.substr(eqpos + 1);
                    hist.prop[name] = value;
                    //  cout << " value name: " << name << endl;

                    if (name == "filename")       hist.Filename = value;
                    if (name == "pathtodatafile") hist.PathToDataFile = value;
                    if (name == "markerStyle")    hist.MarkerStyle = cast<int>(value);
                    if (name == "markerSize")     hist.MarkerSize = cast<float>(value);
                    if (name == "lineStyle")      hist.LineStyle = cast<int>(value);
                    if (name == "lineWidth")      hist.LineWidth = cast<int>(value);
                    if (name == "color")          hist.Color = getRGBfromString(value);
                    if (name == "legend")         hist.Legend = value;
                    if (name == "reference")      hist.Reference = cast<int>(value);
                }
            }
            
            // read corresponding .dat file:
            const Hist& datHist = readDataFile(hist.Filename.c_str());
            
            // check file read is ok
            if (datHist.bins.empty()) {
              cerr << "ERROR: fail to read file (no bins) " << hist.Filename << endl;
              return Plots();
            }
            
            // propagate .dat file info:
            hist.XLabel = datHist.XLabel;
            hist.YLabel = datHist.YLabel;
            hist.Title  = datHist.Title;
            hist.bins   = datHist.bins;
            hist.nevts  = datHist.nevts;
            hist.rivet  = datHist.rivet;
            hist.LogY   = datHist.LogY;
            hist.LogX   = datHist.LogX;
            
            if (plots.XLabel == "") plots.XLabel = hist.XLabel;
            if (plots.YLabel == "") plots.YLabel = hist.YLabel;
            
            histograms.push_back(hist);
        }
    }
    
    plots.histos = histograms;
    plots.insertGaps();
    
    if (debug)
      cout << "  total histograms = " << plots.histos.size() << endl;
    
    return plots;
}


void do_plot(const Plots& plots)
{
    gROOT->SetStyle("Plain");
    gStyle->SetLabelFont(42, "XY");
    gStyle->SetTitleFont(42, "XY");
    gStyle->SetLegendFont(42);
    gStyle->SetTextFont(42);
    
    // Definitions: left and right margin
    const double leftMargin = 0.116;
    const double rightMargin = 0.055;
    const size_t numHist = plots.histos.size();
        
    TLegend* legend = new TLegend;
    TMultiGraph* mg = new TMultiGraph;
    TMultiGraph* mgRatio = new TMultiGraph;
    
    const size_t ref = plots.getRefIndex();

    if (debug) cout << "Reference hist index = " << ref << endl;
    if (debug) cout << "Reference hist nBins = " << plots.histos[ref].bins.size() << endl;
        
    // prepare graphs
    for (size_t ihist = 0; ihist < numHist; ihist++) {
      if (debug)
	cout << "Preparing histogram #" << ihist << endl;
      
      const Hist& histo = plots.histos[ihist];
      
      TGraphAsymmErrors* gr = plots.getGraph(ihist);
      
      // create new color in palette
      const Int_t colorIndex = prepareColor(histo.Color);
      
      gr->SetMarkerStyle(histo.MarkerStyle);
      gr->SetMarkerColor(colorIndex);
      gr->SetMarkerSize(histo.MarkerSize);
            
      gr->SetLineColor(colorIndex);
      gr->SetLineWidth(histo.LineWidth);
      gr->SetLineStyle(histo.LineStyle);
      
      legend->AddEntry(gr, histo.Legend.c_str() , "LP");
      
      // Only allow symbol-only if there actually is a symbol
      if (histo.LineWidth == 0 && histo.MarkerStyle >= 2) {
	const char* grOptions = "P";
	mg->Add(gr, grOptions);
      }
      else {
	const char* grOptions = "LP";
	mg->Add(gr, grOptions);
      }
      
      // ratio graph
      TGraphAsymmErrors* grRatioPlot = plots.getRatioGraph(ihist, ref);
      
      grRatioPlot->SetMarkerStyle(histo.MarkerStyle);
      grRatioPlot->SetMarkerColor(colorIndex);
      grRatioPlot->SetMarkerSize(histo.MarkerSize);
      grRatioPlot->SetLineColor(colorIndex);
      grRatioPlot->SetLineWidth(histo.LineWidth);
      grRatioPlot->SetLineStyle(histo.LineStyle);
      
      grRatioPlot->GetXaxis()->SetTitle(plots.XLabel.c_str());
      grRatioPlot->GetYaxis()->SetTitle(plots.YLabel.c_str());
      
      grRatioPlot->GetXaxis()->SetNdivisions(20505);
      grRatioPlot->GetXaxis()->SetLabelSize(0.033);
      grRatioPlot->GetYaxis()->SetLabelSize(0.033);
      grRatioPlot->GetYaxis()->SetLabelOffset(0.025);
      
      if (ihist == ref) {
	grRatioPlot->SetFillStyle(1001);
	grRatioPlot->SetFillColor(kYellow);
      }
            
      const char* raOptions = (ihist == ref) ? "E2" : "LP";
      mgRatio->Add(grRatioPlot, raOptions);
    }
        
    // decide position of legend
    const bool kLegendPositionRight = plots.getLegendPosition();
    const double xLegWidth = 0.35;
    const double xLegLo = kLegendPositionRight ? 0.54 : leftMargin + 0.035;
    const double xLegHi = xLegLo + xLegWidth;
    const double yLegHi = 0.84;
    const double yLegLo = yLegHi - 0.0365*numHist;
    // legend = new TLegend(xLegLo,yLegLo,xLegHi,yLegHi);
    
    legend->SetX1NDC(xLegLo);
    legend->SetX2NDC(xLegHi);
    legend->SetY1NDC(yLegLo);
    legend->SetY2NDC(yLegHi);
    legend->SetTextSize(0.030);
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    
    // TODO: ??? plots.DrawRatioPlot=0;

    if (plots.DrawGrid)
      {
        gStyle->SetGridColor(kOrange - 9);
        gStyle->SetGridStyle(1);
        gStyle->SetGridWidth(1);
      }
    
    TCanvas * c1;
    TPad * pad1;
    TPad * pad2;
    
    if (!plots.DrawRatioPlot)
      {
        c1 = new TCanvas("canvas", "canvas", 750, 750);
        pad1 = new TPad("main", "main", 0., 0., 1., 1.);
        pad1->SetGrid(plots.DrawGrid, plots.DrawGrid);
        pad1->SetBorderMode(0);
        pad1->SetFillStyle(0);
        pad1->SetTopMargin(0.065);
        pad1->SetRightMargin(rightMargin);
        pad1->SetLeftMargin(leftMargin);
        pad1->SetTickx();
        pad1->SetTicky();
      }
    
    if (plots.DrawRatioPlot)
      {
        c1 = new TCanvas("canvas", "canvas", 750, 1000);
        pad1 = new TPad("main", "main", 0., 0.38, 1., 1.);
        pad1->SetGrid(plots.DrawGrid, plots.DrawGrid);
        pad1->SetBorderMode(0);
        pad1->SetFillStyle(0);
        pad1->SetTopMargin(0.055);
        pad1->SetRightMargin(rightMargin);
        pad1->SetLeftMargin(leftMargin);
        pad1->SetBottomMargin(0.);
        pad1->SetTickx();
        pad1->SetTicky();
        
        pad2 = new TPad("ratio", "ratio", 0., 0., 1., 0.38);
        pad2->SetGrid(plots.DrawGrid,plots.DrawGrid);
        pad2->SetTopMargin(0.0);
        pad2->SetRightMargin(rightMargin);
        pad2->SetLeftMargin(leftMargin);
        pad2->SetBottomMargin(0.175);
        pad2->SetBorderMode(0);
        pad2->SetFillStyle(0);
        pad2->SetTickx();
        pad2->SetTicky();
        pad2->SetLogy();
      }
    

    const bool useLogScaleY = plots.getScaleY();
    const bool useLogScaleX = plots.getScaleX();
    
    if (useLogScaleY) pad1->SetLogy();
    if (useLogScaleX) {
      pad1->SetLogx();
      pad2->SetLogx();
    }
    
    c1->cd();
    pad1->Draw();
    pad1->cd();
    
    mg->Draw("A");
    
    mg->GetXaxis()->SetTitle(plots.XLabel.c_str());
    mg->GetYaxis()->SetTitle(plots.YLabel.c_str());
    mg->GetXaxis()->SetNdivisions(20505);
    mg->GetXaxis()->SetLabelSize(0.038);
    mg->GetYaxis()->SetLabelSize(0.038);
    mg->GetYaxis()->SetLabelOffset(0.013);
    mg->GetYaxis()->SetTitleOffset(1.3);
    mg->GetXaxis()->SetTitleSize(0.04);
    mg->GetYaxis()->SetTitleSize(0.04);
    
    
    // calc data range
    double xmaxData = -100000;
    double ymaxData = -100000;
    double xminData = 1000000;
    double yminData = 1000000;
    double ymin0Data = 1000000;
    // Only use data (or first MC) histo to define y axis range
    for (size_t i = 0; i < plots.histos.size(); ++i) {
      // TODO: this code gives bad result if MC histograms are much higher/lower
      //       from DATA histogram - essentialy resulting plot shows only DATA
      //       distribution
      //       Disabled, to be re-thinked
      // Skip MC histograms if the last one was data
      // (I.e., if there is data, just use data to set ranges)
      // (Otherwise consider all MC distributions)
      //if (i >= 1 && plots.histos[i-1].Reference == 1) break;
      
      // Loop over bins
      const vector<Bin>& bins = plots.histos[i].bins;
      for (size_t ibin = 0; ibin < bins.size(); ibin++) {
        const Bin& b = bins[ibin];
        if (b.isEmpty()) continue;
        
        // X range
        if (b.xhigh > xmaxData) xmaxData = b.xhigh;
        if (b.xlow < xminData ) xminData = b.xlow;
        
        // Y range : for first histogram, make sure we are inside
        // error bars. For subsequent histograms, reduce range
        if (i == 0) {
          if (b.yhigh() > ymaxData) ymaxData = b.yhigh();
          if (b.ylow() < yminData) yminData = b.ylow();
          // min Y value but above zero (for log plots)
          if ((b.ylow() > 0) && (b.ylow() < ymin0Data)) ymin0Data = b.ylow();
        }
        else {              
          if (b.yhigh(0.2) > ymaxData) ymaxData = b.yhigh(0.2);
          if (b.ylow(0.2) < yminData) yminData = b.ylow(0.2);
          // min Y value but above zero (for log plots)
        }
        if ((b.yval > 0) && (b.yval < ymin0Data)) ymin0Data = b.yval;
        
        
      }
    }
    
    // temporary work-around for empty histograms
    if (yminData > ymaxData)
      yminData = ymin0Data = ymaxData = 0.;
    
    if (debug)
      cout << "Data X min / max = " << xminData << " / " << xmaxData << "\n"
           << "Data Y min / min0 / max = " << yminData << " / " << ymin0Data << " / " << ymaxData << endl;
    
    // calc Y axis range
    float maxYAxis=-100;
    float minYAxis=-100;
    if (useLogScaleY)
      {
        yminData = ymin0Data;
        maxYAxis = pow(10, (log10(ymaxData)+  (log10(ymaxData)- log10(yminData))*0.5) );
        minYAxis = pow(10, (log10(yminData)-  (log10(ymaxData)- log10(yminData))*0.15) );
        
        const float diff = log10(maxYAxis)-log10(minYAxis);
        if (diff < 2.)
          {
            maxYAxis = pow(10, ceil(log10(ymaxData)) +  0.15* ( ceil(log10(ymaxData)) - floor(log10(yminData)) )    );
            minYAxis = pow(10, floor(log10(yminData)) );
          }
        double test = int(10*log10(minYAxis))%10;
        if (abs(test) <= 1) minYAxis /= 1.1;
        
      }
    else
      {
        const double diff = fabs(ymaxData-yminData);
        maxYAxis = ymaxData + diff*0.5;
        minYAxis = yminData - diff*0.15;
        
        // TODO: re-write this crap:
        while (minYAxis>0. && maxYAxis / minYAxis < 4. )
          {
            double step = 0.05*fabs(maxYAxis-minYAxis);
            
            // take into account the case maxYAxis == minYAxis
            if (step < 1e-10) step = maxYAxis/100;
            
            maxYAxis+= step;
            minYAxis-= step;
          }
      }
    
    if (debug)
      cout << "Y axis range (automatic): "
           << "min = " << minYAxis << ", max = " << maxYAxis << endl;
    
    if (plots.YAxisMin != UNDEFINED) minYAxis = plots.YAxisMin;
    if (plots.YAxisMax != UNDEFINED) maxYAxis = plots.YAxisMax;
    
    if (debug)
      cout << "Y axis range (actual): "
           << "min = " << minYAxis << ", max = " << maxYAxis << endl;
    
    // Option to force Y range symmetric about 0 
    // (used for top asymmetry paper)
    //bool symmetricYrange = false;
    //if (symmetricYrange) {
    //  float yMax = max(abs(minYAxis),abs(maxYAxis));
    //  minYAxis = -yMax;
    //  maxYAxis = yMax;
    //}
    mg->GetYaxis()->SetRangeUser(minYAxis , maxYAxis);
    
    // calc X axis range
    const double gap = (xminData == xmaxData) ? 0.5 : 0.02*(xmaxData - xminData);
    double maxXAxis = xmaxData + gap;
    double minXAxis = xminData - (useLogScaleX ? gap/30 : gap);
    // check if minXAxis consistent with zero
    if (!useLogScaleX && xminData > 0. &&
      xminData/(xmaxData-xminData) < 0.10) minXAxis = 0.0;
    // For log x scale, use lowest point / 2 if min <= 0
    if (useLogScaleX && minXAxis*maxXAxis <= 0.0) 
      minXAxis = plots.histos[0].bins[0].xfocus/2.;
    
    if (plots.XAxisMin != UNDEFINED) minXAxis = plots.XAxisMin;
    if (plots.XAxisMax != UNDEFINED) maxXAxis = plots.XAxisMax;
    
    if (debug)
      cout << "X axis range (actual): "
           << "min = " << minXAxis << ", max = " << maxXAxis << endl;
    
    mg->GetXaxis()->SetLimits(minXAxis, maxXAxis);
    
    // Hide x axis labels and title from main plot if plotting ratio
    if (plots.DrawRatioPlot) {
      mg->GetXaxis()->SetLabelOffset(999);
      mg->GetXaxis()->SetTitleOffset(999);
    }
    
    mg->Draw();
    
    legend->Draw();
    
    // Top title box
    //TPave* Box = new TPave(0.124, 0.94, 0.9665, 0.94 + 0.05, 0, "NDC");
    //Box->SetFillColor(kWhite);
    //Box->Draw();
    
    // energy, beam
    double upperLeftX=leftMargin+0.015;
    TText* Text1 = new TText(upperLeftX, 0.955, plots.UpperLeftLabel.c_str());
    Text1->SetNDC(kTRUE);
    Text1->SetTextAlign(11);
    Text1->SetTextColor(kBlack);
    Text1->SetTextSize(0.04);
    Text1->Draw();
    
    // process
    double upperRightX=1.-rightMargin-0.015;
    TText* Text2 = new TLatex(upperRightX, 0.955, plots.UpperRightLabel.c_str());
    Text2->SetNDC(kTRUE);
    Text2->SetTextAlign(31);
    Text2->SetTextColor(kBlack);
    Text2->SetTextSize(0.04);
    Text2->Draw();
    
    // logo
    double mcplotsRightX=1.-rightMargin+0.004;    
    TText* Text3 = new TText(mcplotsRightX, 0.01, "mcplots.cern.ch (arXiv:1306.3436)");
    Text3->SetNDC(kTRUE);
    Text3->SetTextAngle(90);
    Text3->SetTextAlign(13);
    Text3->SetTextColor(kGray + 2);
    Text3->SetTextSize(0.032);
    Text3->Draw();       
    
    // reference
    /*
      double mcplotsRightX2=1.-0.01;
      TText* Text3b = new TText(mcplotsRightX2, 0.01, "Eur Phys J C74 (2014) 1 (arXiv:1306.3436)");
      Text3b->SetNDC(kTRUE);
      Text3b->SetTextAngle(90);
      Text3b->SetTextAlign(11);
      Text3b->SetTextColor(kGray + 3);
      Text3b->SetTextSize(0.033);
      Text3b->Draw();       
    */

    // Rivet version, number of events
    const string rivet = plots.getRivetVersion();
    const string nevts = plots.getNevents();
    
    string etcnote;
    if (rivet != "") etcnote += ("Rivet " + rivet);
    if (nevts != "") etcnote += (",  " + nevts);
    
    double rivetRightX=1.-rightMargin+0.004;    
    TLatex* Text4 = new TLatex(rivetRightX, 0.94, etcnote.c_str());
    Text4->SetNDC(kTRUE);
    Text4->SetTextAngle(90);
    Text4->SetTextAlign(33);
    Text4->SetTextColor(kGray + 2);
    Text4->SetTextSize(0.032);
    Text4->Draw();
    
    
    //
    const double xC_NDC = 0.5 * (leftMargin + (1.0 - pad1->GetRightMargin()));
    
    // Monte Carlo Event Generator Version Numbers
    TText* textbox1 = new TText(xC_NDC, 0.06, plots.TextField1.c_str());
    textbox1->SetNDC(kTRUE);
    textbox1->SetTextAlign(22);
    textbox1->SetTextSize(0.027);
    textbox1->SetTextColor(kGray + 2);
    textbox1->Draw();
    
    // Data Reference
    TText* textbox2 = new TText(xC_NDC, 0.104, plots.TextField2.c_str());
    textbox2->SetNDC(kTRUE);
    textbox2->SetTextAlign(22);
    textbox2->SetTextSize(0.027);
    textbox2->SetTextColor(kGray + 2);
    textbox2->Draw();
    
    // Plot title (LaTeX)
    TLatex* MainTitle = new TLatex(xC_NDC, 0.875, plots.Title.c_str());
    MainTitle->SetNDC(kTRUE);
    MainTitle->SetTextAlign(22);
    MainTitle->SetTextSize(0.033);
    MainTitle->SetTextColor(kBlack);
    MainTitle->Draw();

        
    // ratio plot
    if (plots.DrawRatioPlot)
      {
        c1->cd();
        pad2->SetFillStyle(0);
        pad2->Draw();
        pad2->cd();
        
        mgRatio->Draw("A");
        
        mgRatio->GetXaxis()->SetLabelSize(0.065);
        mgRatio->GetYaxis()->SetLabelSize(0.065);
        mgRatio->GetYaxis()->SetLabelOffset(0.015);
        mgRatio->GetXaxis()->SetTitle(plots.XLabel.c_str());
        mgRatio->GetXaxis()->SetTitleSize(0.07);            
        const TString nameRatio = "Ratio to " + plots.histos[ref].Legend;
        mgRatio->GetYaxis()->SetTitle(nameRatio);
        mgRatio->GetYaxis()->SetTitleSize(0.05);
        mgRatio->GetYaxis()->CenterTitle();
        mgRatio->GetYaxis()->SetRangeUser(0.4,2.5);
        mgRatio->GetYaxis()->SetNdivisions(20505,-1);
        mgRatio->GetXaxis()->SetNdivisions(20505);
        
        mgRatio->GetXaxis()->SetLimits(minXAxis, maxXAxis);
        
        mgRatio->Draw();
        
        // to redraw axis hidden by the fill area
        pad2->RedrawAxis();
        
        if (false) {
          TPaveText* PaveRatio = new TPaveText(0.081, 0.899672, 1 - 0.0335, 0.899672 + 0.1, "NDC");
          PaveRatio->SetBorderSize(0);
          PaveRatio->SetFillStyle(0);
          PaveRatio->SetTextAlign(12);
          PaveRatio->SetTextSize(0.062);
          PaveRatio->SetTextColor(kBlack);
          PaveRatio->AddText(nameRatio);
          PaveRatio->Draw();
        }
        
        // Extract final plot ranges
        const double xPlotLo = minXAxis;
        const double xPlotHi = mgRatio->GetXaxis()->GetXmax();
        
        // Draw line at 1
        TLine* Line = new TLine(xPlotLo, 1.0, xPlotHi, 1.0);
        Line->SetLineColorAlpha(kBlack,0.7);
        Line->SetLineWidth(1);
        Line->SetLineStyle(1);
        Line->Draw();
        
        // Optionally Draw weaker lines at 10% intervals
        if (false) {
          int lineStyle = 1;
          
          TLine * LineLo = new TLine(xPlotLo,0.9,xPlotHi,0.9);
          LineLo->SetLineColorAlpha(kGray+2,0.3);
          LineLo->SetLineWidth(1);
          LineLo->SetLineStyle(lineStyle);
          LineLo->Draw();
          TLine * LineHi = new TLine(xPlotLo,1.1,xPlotHi,1.1);
          LineHi->SetLineColorAlpha(kGray+2,0.3);
          LineHi->SetLineWidth(1);
          LineHi->SetLineStyle(lineStyle);
          LineHi->Draw();
          TLine * LineLo2 = new TLine(xPlotLo,0.8,xPlotHi,0.8);
          LineLo2->SetLineColorAlpha(kGray+2,0.3);
          LineLo2->SetLineWidth(1);
          LineLo2->SetLineStyle(lineStyle);
          LineLo2->Draw();
          TLine * LineHi2 = new TLine(xPlotLo,1.2,xPlotHi,1.2);
          LineHi2->SetLineColorAlpha(kGray+2,0.3);
          LineHi2->SetLineWidth(1);
          LineHi2->SetLineStyle(lineStyle);
          LineHi2->Draw();
          TLine * LineLo3 = new TLine(xPlotLo,0.5,xPlotHi,0.5);
          LineLo3->SetLineColorAlpha(kGray+2,0.3);
          LineLo3->SetLineWidth(1);
          LineLo3->SetLineStyle(lineStyle);
          LineLo3->Draw();
          TLine * LineHi3 = new TLine(xPlotLo,2.0,xPlotHi,2.0);
          LineHi3->SetLineColorAlpha(kGray+2,0.3);
          LineHi3->SetLineWidth(1);
          LineHi3->SetLineStyle(lineStyle);
          LineHi3->Draw();
          /*
            TLine * LineHi5 = new TLine(xPlotLo,1.5,xPlotHi,1.5);
            LineHi5->SetLineColorAlpha(kGray+2,0.5);
            LineHi5->SetLineWidth(1);
            LineHi5->SetLineStyle(1);
            LineHi5->Draw();
          */
          // Lines at 5%
          if (false) {
            TLine * LineLo4 = new TLine(xPlotLo,1.05,xPlotHi,1.05);
            LineLo4->SetLineColorAlpha(kGray+2,0.3);
            LineLo4->SetLineWidth(1);
            LineLo4->SetLineStyle(lineStyle);
            LineLo4->Draw();
            TLine * LineHi4 = new TLine(xPlotLo,0.95,xPlotHi,0.95);
            LineHi4->SetLineColorAlpha(kGray+2,0.3);
            LineHi4->SetLineWidth(1);
            LineHi4->SetLineStyle(lineStyle);
            LineHi4->Draw();
          }
        }
        
        // Make extra tick marks between 1 and 2
        if (true) {
          vector<TLine*> Ticks;
          
          double tickLen = 0.015;
          double tickLenMaj = 0.022;
          double xLo2      = xPlotLo + tickLen*(xPlotHi-xPlotLo);
          double xLo2maj   = xPlotLo + tickLenMaj*(xPlotHi-xPlotLo);
          double xHi2      = xPlotHi - tickLen*(xPlotHi-xPlotLo);
          double xHi2maj   = xPlotHi - tickLenMaj*(xPlotHi-xPlotLo);
          double xLabLeft  = xPlotLo - tickLenMaj*(xPlotHi-xPlotLo);
          double xLabRight = xPlotHi + 0.009*(xPlotHi-xPlotLo);
          double xLabR2    = xPlotHi + 0.009*(xPlotHi-xPlotLo);
          double xLabR3    = xPlotHi + tickLen*(xPlotHi-xPlotLo);
          if (useLogScaleX) {
            cout<<" xPlotHi = "<<xPlotHi<<" xPlotLo = "<<xPlotLo<<endl;
            double lnxDist     = log(xPlotHi/xPlotLo);
            double lnXlo2      = log(xPlotLo) + tickLen * lnxDist;
            double lnXlo2maj   = log(xPlotLo) + tickLenMaj  * lnxDist;
            double lnXhi2      = log(xPlotHi) - tickLen * lnxDist;
            double lnXhi2maj   = log(xPlotHi) - tickLenMaj  * lnxDist;
            double lnXlabLeft  = log(xPlotLo) - tickLenMaj  * lnxDist;
            double lnXlabRight = log(xPlotHi) + 0.0065 * lnxDist;
            double lnXlabR2    = log(xPlotHi) + 0.009 * lnxDist;
            double lnXlabR3    = log(xPlotHi) + tickLen * lnxDist;
            xLo2      = exp(lnXlo2);
            cout<<" xLo 2 ="<<xLo2<<" lnxDist = "<<lnxDist<<endl;
            xLo2maj   = exp(lnXlo2maj);
            xHi2      = exp(lnXhi2);
            xHi2maj   = exp(lnXhi2maj);
            xLabLeft  = exp(lnXlabLeft);
            xLabRight = exp(lnXlabRight);
            xLabR2    = exp(lnXlabR2);
            xLabR3    = exp(lnXlabR3);
          }
          // Minor ticks
          for (int i=1;i<=14;++i) {
            Ticks.push_back(new TLine(xPlotLo,1+i*0.1,xLo2,1+i*0.1));
            Ticks.push_back(new TLine(xPlotHi,1+i*0.1,xHi2,1+i*0.1));
          }
          // Major ticks
          for (int i=-5;i<=10; i+= 5) {
            Ticks.push_back(new TLine(xPlotLo,1+i*0.1,xLo2maj,1+i*0.1));
            Ticks.push_back(new TLine(xPlotHi,1+i*0.1,xHi2maj,1+i*0.1));
          }
          for (int i=0; i<(int)Ticks.size(); ++i) {
            Ticks[i]->SetLineColor(kBlack);
            Ticks[i]->SetLineWidth(1);
            Ticks[i]->SetLineStyle(1);
            Ticks[i]->Draw();
          }
          // Labels: 0.5 and 2.0            
          TText* lab2 = new TText(xLabLeft, 2.01, "2");            
          lab2->SetTextAlign(32);
          lab2->SetTextSize(0.065);
          lab2->SetTextColor(kBlack);
          lab2->Draw();
          TText* lab05 = new TText(xLabLeft, 0.505, "0.5");            
          lab05->SetTextAlign(32);
          lab05->SetTextSize(0.065);
          lab05->SetTextColor(kBlack);
          lab05->Draw();
          
          // Labels on right side denoting percentages, or same as left side
          bool showPercentages=false;
          if (!showPercentages) {
            // Duplicate left-hand side labels
            TText* lab2right = new TText(xLabRight, 2.01, "2");            
            lab2right->SetTextAlign(12);
            lab2right->SetTextSize(0.065);
            lab2right->SetTextColor(kBlack);
            lab2right->Draw();
            TText* lab05right = new TText(xLabRight, 0.505, "0.5");            
            lab05right->SetTextAlign(12);
            lab05right->SetTextSize(0.065);
            lab05right->SetTextColor(kBlack);
            lab05right->Draw();
            TText* lab1right = new TText(xLabRight, 1.0, "1");            
            lab1right->SetTextAlign(12);
            lab1right->SetTextSize(0.065);
            lab1right->SetTextColor(kBlack);
            lab1right->Draw();
	    
          } else {
            TText* lab12 = new TText(xLabRight, 1.218, "+20%");            
            lab12->SetTextAlign(12);
            lab12->SetTextSize(0.03);
            lab12->SetTextColor(kGray+1);
            lab12->Draw();
            TText* lab08 = new TText(xLabRight, 0.81, "-20%");            
            lab08->SetTextAlign(12);
            lab08->SetTextSize(0.03);
            lab08->SetTextColor(kGray+1);
            lab08->Draw();
            TText* lab11 = new TText(xLabRight, 1.115, "+10%");            
            lab11->SetTextAlign(12);
            lab11->SetTextSize(0.03);
            lab11->SetTextColor(kGray+1);
            lab11->Draw();
            TText* lab09 = new TText(xLabRight, 0.907, "-10%");            
            lab09->SetTextAlign(12);
            lab09->SetTextSize(0.03);
            lab09->SetTextColor(kGray+1);
            lab09->Draw();
            if (false) {
              TLine* r5pc = new TLine(xLabRight,0.95,xLabRight,1.05);
              r5pc->SetLineColor(kGray+1);
              r5pc->SetLineWidth(1);
              r5pc->SetLineStyle(1);
              r5pc->Draw();
              TLatex* labc = new TLatex(xLabR3, 1.01, "#pm5%");            
              labc->SetTextAlign(12);
              labc->SetTextSize(0.03);
              labc->SetTextColor(kGray+1);
              labc->Draw();
            }
            TLatex* lab2r = new TLatex(xLabR2, 2.02, "#times2");            
            lab2r->SetTextAlign(12);
            lab2r->SetTextSize(0.03);
            lab2r->SetTextColor(kGray+1);
            lab2r->Draw();
            TLatex* lab05r = new TLatex(xLabRight, 0.502, "-50%");            
            lab05r->SetTextAlign(12);
            lab05r->SetTextSize(0.03);
            lab05r->SetTextColor(kGray+1);
            lab05r->Draw();
            TLatex* lab15r = new TLatex(xLabRight, 1.507, "+50%");            
            lab15r->SetTextAlign(12);
            lab15r->SetTextSize(0.03);
            lab15r->SetTextColor(kGray+1);
            lab15r->Draw();
          }
        }
        
      }
    
    c1->SaveAs((plots.OutputFileName + ".pdf").c_str());
    c1->SaveAs((plots.OutputFileName + ".eps").c_str());
    c1->SaveAs((plots.OutputFileName + ".png").c_str());
    
    delete c1;
}

void do_chi2_calc(const Plots& plots, double uncertainty)
{
  const size_t ref = plots.getRefIndex();
  
  // loop through data files
  for (size_t i = 0; i < plots.histos.size(); i++) {
    // skip self-comparison
    if (i == ref) continue;
    
    // calc Chi2/Ndof
    const double normChi2 = plots.calcNormChi2(i, ref, uncertainty);
    
    if (debug) {
      cout << "Chi2/Ndof =" << normChi2 << endl;
    }
    
    // TODO: currently the PHP code from the validation page calls this function
    //       with two input histograms (DATA and MC), the more efficient way is
    //       DATA and MC_1 and MC_2 ... MC_n but require change of PHP code
    
    // write Chi2/NDOF to file
    const string fname = plots.OutputFileName + ".txt";
    ofstream f(fname.c_str());
    f << normChi2 << "; ";
    cout << "File " << fname << " has been created" << endl;
  }
}


struct CmdArgs {
  CmdArgs():
    debug(false),
    chi2only(false),
    uncertainty(0)
  {}
  
  bool debug;
  bool chi2only;
  double uncertainty;
  vector<string> fnames;
};

// parse command-line arguments
CmdArgs parseArgs(const int argc, char* argv[])
{
  CmdArgs cmd;
  
  for (int i = 1; i < argc; ++i) {
    const string arg = argv[i];
    
    // check the argument in 'key=value' format and extract value
    const size_t eqpos = arg.find("=");
    const string key = (eqpos != string::npos) ? arg.substr(0, eqpos) : "";
    const string val = (eqpos != string::npos) ? arg.substr(eqpos + 1) : "";
    
    // check for 'chi2=level' parameter
    if (key == "chi2") {
      cmd.chi2only = true;
      cmd.uncertainty = cast<double>(val) / 100.; // extract uncertainty level
      continue;
    }
    
    // check for 'debug=level' parameter
    if (key == "debug") {
      cmd.debug = cast<int>(val) != 0;
      continue;
    }
    
    // argument is steering file name
    cmd.fnames.push_back(arg);
  }
  
  if (cmd.debug)
    cout << "Command-line params:\n"
         << "  debug = "       << cmd.debug << "\n"
         << "  chi2only = "    << cmd.chi2only << "\n"
         << "  uncertainty = " << cmd.uncertainty << "\n"
         << "  #fnames = "     << cmd.fnames.size() << endl;
  
  return cmd;
}


int main (int argc, char* argv[])
{
    const CmdArgs& cmd = parseArgs(argc, argv);
    debug = cmd.debug;
    
    // silence ROOT info messages in non-debug mode
    if (!debug)
      gErrorIgnoreLevel = kWarning;
    
    for (size_t n = 0; n < cmd.fnames.size(); ++n) {
        const Plots& plots = read(cmd.fnames[n].c_str());
        
        // exit in case of error reading input files
        if (plots.histos.empty())
          return 1;
        
        if (cmd.chi2only)
          do_chi2_calc(plots, cmd.uncertainty);
        else
          do_plot(plots);
    }

    return 0;
}
