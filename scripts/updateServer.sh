#!/bin/bash -e

# print usage instruction
function print_usage () {
  echo "Script for update of production server"
  echo ""
  echo "Usage:"
  echo "    updateServer.sh [-r -d -h]"
  echo ""
  echo "Options:"
  echo "    -r revision of SVN to update to. Default is HEAD revision"
  echo "    -d path to dat/ directory with histograms"
  echo "    -h usage information"
  echo ""
  echo "Example:"
  echo "    updateServer.sh"
  echo "    updateServer.sh -r 308"
  echo "    updateServer.sh -d /path/to/directory/dat"
  echo ""
}

#  helper function to show progress during tarball creation
function print_progress () {
  local fname="$1"
  
  while read ; do
    local size=$(du -k $fname | cut -f 1)
    echo -ne "\r            $size Kb"
  done
  
  echo ""
}


# process args
while getopts "r:d:h" Option ; do
  case $Option in
    r ) rev=$OPTARG ;;
    d ) datdir=$OPTARG ;;
    h ) print_usage ; exit 0 ;;
    ? ) exit 1 ;;
  esac
done


prodhost=mcplots
svnrepo="http://svn.cern.ch/guest/mcplots/trunk"
revision=${rev+-r $rev}
stamp="$(date +%F-%H.%M.%S)"
dattgz="dat.$stamp.tgz"
mcpath="/home/mcplots.$stamp"

echo "Checking passwordless access to the $prodhost ..."
ssh -o "BatchMode=yes" $prodhost exit

# copy tarball with histograms to server
if [[ "$datdir" != "" ]] ; then
  tmptgz="$(pwd)/$dattgz"
  
  if test -e $tmptgz ; then
    echo "ERROR: temporary file exists already, tmptgz = $tmptgz"
    exit 1
  fi
  
  echo "Preparing tarball with histograms..."
  echo "  source  = $datdir"
  echo "  tarball = $tmptgz"
  
  tar --checkpoint -b 256 -zcf $tmptgz -C $datdir . 2>&1 | print_progress $tmptgz
  
  echo "Copying tarball to $prodhost..."
  echo "  dest    = $prodhost:/home/$dattgz"
  
  scp $tmptgz $prodhost:/home/$dattgz
  
  rm $tmptgz
fi

# connect to server and setup site
ssh -T $prodhost bash -e <<EOT
# download fresh version
echo "Exporting $svnrepo to $mcpath ..."
svn export -q $revision $svnrepo $mcpath

# build plotter
echo "Building plotter ..."
make -C $mcpath/plotter

#
echo "Creating symlinks and directories ..."
cd $mcpath/www
ln -sf ../plotter/plotter.exe
ln -sf ../scripts/updatedb.sh

mkdir -p dat cache/plots
chmod a+w -R cache/

# prepare histograms
if [[ "$datdir" != "" ]] ; then
  # unpack tarball with histograms
  echo "Unpacking histograms /home/$dattgz to $mcpath/www/dat ..."
  tar -zxf /home/$dattgz -C dat/
  rm /home/$dattgz
else
  # copy histograms from latest publication
  echo "Copying histograms from latest publication /home/mcplots/www/dat to $mcpath/www/dat ..."
  cp -al /home/mcplots/www/dat/* dat/
fi

# create database
echo "Creating database ..."
./updatedb.sh

# remove cache of the old site
#echo "Run old site release cache cleanup ..."
#rm -rf /home/mcplots/www/cache &

# re-point 'mcplots' symlink to new site root
echo "Switching site root to $mcpath ..."
ln -sf -T $mcpath /home/mcplots
EOT

echo "Site updated successfully!"
