#!/bin/bash

# this function iterates over histograms directory structure
# and prints SQL commands to build the table which reflect
# the directory structure
function prepare_histograms() {
  local src=$1
  
  # records counter
  local i=0
  
  # clean table
  echo "DROP TABLE IF EXISTS histograms;"
  echo "CREATE TABLE histograms"
  echo "("
  echo "  id         MEDIUMINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,"
  echo "  fname      CHAR(200) NOT NULL,"
  echo "  "
  echo "  type       CHAR(4)  NOT NULL,"
  echo "  process    CHAR(20) NOT NULL,"
  echo "  observable CHAR(40) NOT NULL,"
  echo "  tune       CHAR(20) NOT NULL,"
  echo "  experiment CHAR(20) NOT NULL,"
  echo "  reference  CHAR(40) NOT NULL,"
  echo "  beam       CHAR(20) NOT NULL,"
  echo "  energy     SMALLINT UNSIGNED NOT NULL,"
  echo "  cuts       CHAR(40) NOT NULL,"
  echo "  generator  CHAR(20) NOT NULL,"
  echo "  version    CHAR(20) NOT NULL,"
  echo "  INDEX (process, observable)"
  echo ");"
  echo "LOCK TABLES histograms WRITE;"
  echo ""
  
  # iterate over all *.dat files to add new records to the table
  find $src -type f -name '*.dat' | while read fname ; do
    # extract all metadata fields from file path/name
    vals=( ${fname//// } )         # replace all '/' by ' ' in $fname and prepare array
    
    local nvals="${#vals[*]}"
    
    # do check on file path structure:
    if [[ "$nvals" != "9" && "$nvals" != "7" ]] ; then
      if [[ "$nvals" == "10" && ("${fname}" =~ "alpgen") ]] ; then
        # Alpgen NpX histogram, skip
        continue
      fi
      
      { echo "ERROR: incorrect path to histogram:"
        echo "         $fname"
        echo ""
        echo "       should be in form:"
        echo "         [data] dat/beam/process/observable/cuts/energy/reference.dat"
        echo "         [mc]   dat/beam/process/observable/cuts/energy/generator/version/tune.dat"
        echo "         [mc]   dat/beam/process/observable/cuts/energy/alpgen*/version/specific/tune.dat"
      } 1>&2
      exit 1
    fi
    
    beam="${vals[1]}"
    process="${vals[2]}"
    observable="${vals[3]}"
    cuts="${vals[4]}"
    energy="${vals[5]}"
    
    if [[ "$nvals" == "9" ]] ; then
      # this is MC .dat file
      type="mc"
      reference=""
      experiment=""
      generator="${vals[6]}"
      version="${vals[7]}"
      tune="${vals[8]%.dat}"         # remove '.dat' from the back of the string
    else
      # this is DATA .dat file
      type="data"
      reference="${vals[6]%.dat}"
      experiment="${reference%%_*}"  # remove first '_' character and everything after
      generator=""
      version=""
      tune=""
    fi
    
    echo "INSERT INTO histograms VALUES " \
         "( NULL, '$fname', '$type', '$process', '$observable', '$tune', '$experiment'," \
         " '$reference', '$beam', $energy, '$cuts', '$generator', '$version' );"
    
    # print progress and increase counter
    let i++
    if (( i % 10000 == 0 )) ; then
      echo "[$(date)] ${i%000}k: $fname" >&2
    fi
  done || return 1
  
  echo "UNLOCK TABLES;"
}

# optimize `histograms` table structure by
# parsing output of MySQL ANALYSE() procedure to
# convert most of fields from CHAR() to ENUM() type
# and reduce disk usage/improve access speed
function optimize_db () {
  echo "SELECT * FROM histograms PROCEDURE ANALYSE()" | \
  mysql --skip-column-names -u mcplots mcplots | \
  while read line ; do
    # field name
    local f=$(echo "$line" | cut -f 1)
    f=$(echo $f | cut -d '.' -f 3)
    
    # optimised type
    local t=$(echo "$line" | cut -f 10)
    
    # skip `id` and `energy` columns
    if [[ "$f" == "id" || "$f" == "energy" ]] ; then
      continue
    fi
    
    # keep `fname` column as CHAR type
    if [[ "$f" == "fname" ]] ; then
      t=${t/VARCHAR/CHAR}
    fi
    
    echo "MODIFY $f $t"
  done | \
  (
    echo -n "ALTER TABLE histograms "
    paste -s -d ,
  ) | \
  mysql -u mcplots mcplots
}


# path to histograms root directory
histdir="dat/"

echo "Input path: $histdir"

if ! test -d $histdir ; then
  echo "ERROR: input path directory $histdir does not exist"
  exit 1
fi

# path to SQL script:
sql=$(mktemp)

# generate SQL script:
echo "[$(date)] Generating SQL script ($sql)..."
prepare_histograms $histdir > $sql

# check success:
if [[ "$?" != "0" ]] ; then
  echo "ERROR: fail to prepare SQL script" >&2
  exit 1
fi

# update database:
echo "[$(date)] Updating database..."
mysql -u mcplots mcplots < $sql

# and check the update is successful:
if [[ "$?" != "0" ]] ; then
  echo "ERROR: fail to update database" >&2
  exit 1
fi

# database updated, remove SQL script
rm -f $sql

# optimize database
echo "[$(date)] Optimizing database..."
optimize_db

if [[ "$?" != "0" ]] ; then
  echo "ERROR: fail to optimize database" >&2
  exit 1
fi

# clear cache:
echo "[$(date)] Clearing cache..."
# ensure cache/ could be written
mkdir -p cache
chmod a+w cache
# rename plots cache directory and run remove in background
cache0="cache/plots.obsolete.$RANDOM"
mkdir -p cache/plots
mv cache/plots $cache0
rm -rf $cache0 &
# create new empty plots cache
mkdir -p cache/plots
chmod -R a+w cache/plots

echo "[$(date)] Update finished successfully"
