#!/bin/bash -e

# Script `update-production.sh` is intended to be run as cron job.
# Manual edditing of script at the time of execution can lead to
# dangerous results.
# The purpose of this wrapper is to protect agains this case by
# picking up and executing the latest version of script from svn.

# path to script in svn:
svnscript="http://svn.cern.ch/guest/mcplots/trunk/scripts/update-production.sh"

# temporal local copy of script:
script=$(mktemp -t update-production.XXXXXXXX)

echo "$(date) Exporting production script..."

svn export -q $svnscript $script

chmod +x $script

$script

rm -f $script
