#!/bin/bash -ev
# -e: stop script execution on command error exit code
# -v: print commands before execution to see the progress

pool="/home/mcplots/pool"
release="/home/mcplots/release"

echo "[$(date)] Building release ..."

# ===== prepare dev release
rel1="$release/dev.2017-12-05"
mkdir $rel1
cd $rel1

# copy "base" dataset (BOINC production)
cp -al --remove-destination $pool/2016/dat .

# add runs which are missig in BOINC production:
#  - Alpgen runs (segfault on 32 bits nodes)
# merge.sh $release/2016.alpgen/dat $release/2016.alpgen.combined/dat
cp -al --remove-destination $release/2016.alpgen.combined/dat .

# NOTE: 24 epos runs failed and missing (crash in Rivet/YODA 2.4.0)
# ppbar mb-inelastic (63|200) - - epos 1.99.crmc.1.* (default|lhc)

#exit

# ===== public release
rel2="$release/pub.2017-12-05"
mkdir $rel2
cd $rel2

# the public release is based on rel1:
cp -al --remove-destination $rel1/dat .

# remove several preliminary/broken processes and observables:

# broken
rm -rf dat/ee/zhad/rate-*jet
rm -rf dat/ee/zhad/Y2/aleph1/133

# MC_TTBAR_FBA (remove debug plot with various cuts)
rm -rf dat/{pp,ppbar}/top-mc/xsec/mc-cut

# CMS_EWK_10_012 (still unfinished)
rm -rf */*/*/*/cms2010-{el,mu}

# Alpgen+Herwig/AGILe has an issue with XS
rm -rf dat/*/*/*/*/*/alpgenherwigjimmy

# the highest jets multiplicity subsample ("4,0") is failed and absent in some of "jets" runs
# which leads to histograms with incomplete data
# remove such histograms
find dat/*/jets/*/*/*/alpgen*/*/*.dat | while read f ; do
  dn=$(dirname $f)
  bn=$(basename $f)
  f40="$dn/4,0/$bn"
  if [[ ! -e "$f40" ]] ; then
    rm $f
  fi
done

# bottom production steering cards are unfinished
rm -rf dat/*/b
