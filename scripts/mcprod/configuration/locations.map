# Correspondence between generator version and location in one of LCG software trees at:
#
#   AFS:
#     /afs/cern.ch/sw/lcg/external/MCGenerators_*       (old trees, LCG < 68)
#     /afs/cern.ch/sw/lcg/releases/LCG_*/MCGenerators   (new trees, 68 <= LCG <= 87)
#
#   CVMFS:
#     /cvmfs/sft.cern.ch/lcg/external/MCGenerators_*       (LCG < 68)
#     /cvmfs/sft.cern.ch/lcg/releases/LCG_*/MCGenerators   (LCG >= 71)
#
# Note:
#
# 1. CVMFS installation is neccessary for runs on BOINC cluster.
#
# 2. The CVMFS installations are supported (and identical to AFS) starting on LCG 87,
#    previous LCG versions are targeted for AFS and have only partial copy on CVMFS.
#
# 3. Starting from LCG 88 the only installation provided is CVMFS (AFS is deprecated).
#
# LCG software reference:
#
#   http://lcginfo.cern.ch/
#   http://lcgsoft.web.cern.ch/lcgsoft/


# [generator]    [version]         [tree]

alpgenpythia6    2.1.3e_6.426      hepmc2.06.05
alpgenpythia6    2.1.4_6.426       hepmc2.06.05

alpgenherwigjimmy 2.1.3e_6.520     hepmc2.06.05
alpgenherwigjimmy 2.1.4_6.520      hepmc2.06.05

epos             1.99.crmc.0.v3400 lcgcmt65
epos             1.99.crmc.1.0     lcgcmt65
epos             1.99.crmc.1.2     lcgcmt65
epos             1.99.crmc.1.3     lcgcmt65
epos             1.99.crmc.1.4     lcgcmt67c
epos             1.99.crmc.1.5.3   lcgcmt67c
epos             1.99.crmc.1.5.4   lcgcmt67c
epos             1.99.crmc.1.5.4-hi higenerators

herwig++         2.4.2             hepmc2.06.05
herwig++         2.5.0             hepmc2.06.05
herwig++         2.5.1             hepmc2.06.05
herwig++         2.5.2             hepmc2.06.05
herwig++         2.6.0             hepmc2.06.05
herwig++         2.6.1a            hepmc2.06.05
herwig++         2.6.2             hepmc2.06.05
herwig++         2.6.3             hepmc2.06.05
herwig++         2.7.0             lcgcmt65
herwig++         2.7.1             lcgcmt61c

herwig++powheg   2.5.0             hepmc2.06.05
herwig++powheg   2.5.1             hepmc2.06.05
herwig++powheg   2.5.2             hepmc2.06.05
herwig++powheg   2.6.0             hepmc2.06.05
herwig++powheg   2.6.1a            hepmc2.06.05
herwig++powheg   2.6.2             hepmc2.06.05
herwig++powheg   2.6.3             hepmc2.06.05
herwig++powheg   2.7.0             lcgcmt65
herwig++powheg   2.7.1             lcgcmt61c

herwig7         7.0.0             lcgcmt67c
herwig7         7.0.1             lcgcmt67c
herwig7         7.0.2             LCG_87
herwig7         7.0.3             LCG_87
herwig7         7.0.4             LCG_87
herwig7         7.1.0             LCG_87
herwig7         7.1.1             LCG_87

phojet           1.12a             hepmc2.06.05

pythia6          6.423             hepmc2.06.05
pythia6          6.424             hepmc2.06.05
pythia6          6.425             hepmc2.06.05
pythia6          6.426             hepmc2.06.05
pythia6          6.427             lcgcmt65
pythia6          6.428             lcgcmt65

pythia8          8.108.p1          hepmc2.06.05
pythia8          8.130.p1          hepmc2.06.05
pythia8          8.135             hepmc2.06.05
pythia8          8.142             hepmc2.06.05
pythia8          8.145             hepmc2.06.05
pythia8          8.150             hepmc2.06.05
pythia8          8.153             hepmc2.06.05
pythia8          8.160             hepmc2.06.05
pythia8          8.162             hepmc2.06.05
pythia8          8.163             hepmc2.06.05
pythia8          8.165             hepmc2.06.05
pythia8          8.170             hepmc2.06.05
pythia8          8.175             hepmc2.06.05
pythia8          8.176             hepmc2.06.05
pythia8          8.180             lcgcmt65
pythia8          8.183             lcgcmt65
pythia8          8.185             lcgcmt65
pythia8          8.186             lcgcmt61c
pythia8          8.201             lcgcmt61c
pythia8          8.205             lcgcmt61c
pythia8          8.209             lcgcmt61c
pythia8          8.210             lcgcmt61c
pythia8          8.212             lcgcmt65
pythia8          8.219             LCG_87
pythia8          8.223             LCG_87
pythia8          8.226             LCG_87
pythia8          8.230             LCG_87
pythia8          8.230.custom      LCG_87

sherpa           1.2.2p            hepmc2.06.05
sherpa           1.2.3             hepmc2.06.05
sherpa           1.3.0             hepmc2.06.05
sherpa           1.3.1             hepmc2.06.05
sherpa           1.4.0             hepmc2.06.05
sherpa           1.4.1             hepmc2.06.05
sherpa           1.4.2             hepmc2.06.05
sherpa           1.4.3             hepmc2.06.05
sherpa           1.4.5             lcgcmt65
sherpa           2.0.0             lcgcmt65
sherpa           2.1.0             lcgcmt65
sherpa           2.1.1             LCG_87
sherpa           2.2.0             LCG_87
sherpa           2.2.1             LCG_87
sherpa           2.2.2             LCG_87
sherpa           2.2.4             LCG_87

vincia           1.0.24_8.142      hepmc2.06.05
vincia           1.0.25_8.150      hepmc2.06.05
vincia           1.0.26_8.150      hepmc2.06.05
vincia           1.0.28_8.165      hepmc2.06.05
vincia           1.0.28_8.170      hepmc2.06.05
vincia           1.1.00_8.176      hepmc2.06.05
vincia           1.1.01_8.180      lcgcmt61c
vincia           1.1.02_8.185      lcgcmt61c
vincia           1.1.03_8.185      lcgcmt61c
vincia           1.2.00_8.205      lcgcmt61c
vincia           1.2.01_8.205      lcgcmt61c
vincia           1.2.02_8.205      lcgcmt61c
vincia           1.2.02_8.210      lcgcmt61c
vincia           2.0.01_8.226      LCG_92
