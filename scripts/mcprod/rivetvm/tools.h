#ifndef TOOLS_H
#define TOOLS_H

#include "Rivet/Rivet.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/RivetYODA.hh"

using namespace std;

void printState(const Rivet::AnalysisHandler& rivet)
{
  cout << "Current histograms list:" << endl;
  
  const vector<Rivet::AnalysisObjectPtr>& data = rivet.getData();
  
  for (size_t i = 0; i < data.size(); ++i) {
    const Rivet::AnalysisObjectPtr ao = data[i];
    cout << ao->type() << "\t" << ao->path() << endl;
  }
}

bool checkAnalysis(const string a)
{
  static const set<string> analyses = Rivet::AnalysisLoader::getAllAnalysisNames();
  return (analyses.count(a) != 0);
}

void listAvailableAnalyses()
{
  static const set<string> analyses = Rivet::AnalysisLoader::getAllAnalysisNames();
  cout << "Available analyses: " << endl;
  foreach (const string& a, analyses) {
    cout << a << endl;
  }
}

// create the output filename based on a histogram name
string getOutputFilename(string path)
{
  // replace '/' -> '_'
  while (true) {
    const size_t p = path.find("/");
    if (p == string::npos) break;
    path.replace(p, 1, "_");
  }
  
  return path.substr(1) + ".dat";
}

string getAnalysisName(const string path)
{
  const size_t p1 = path.find("/");
  if (p1 != 0) return "";
  
  const size_t p2 = path.find("/", p1 + 1);
  if (p2 == string::npos) return "";
  
  return path.substr(p1 + 1, p2 - (p1 + 1));
}

// Write some header meta data
void writeHeader(ofstream &file, const int numEvents, const double crosssection, const string status)
{
  // increase precision for floats:
  const streamsize prec0 = file.precision(15);
  
  file << "# BEGIN METADATA\n"
       << "beam=%beam%\n"
       << "process=%process%\n"
       << "energy=%energy%\n"
       << "params=%params%\n"
       << "specific=%specific%\n"
       << "generator=%generator%\n"
       << "version=%version%\n"
       << "tune=%tune%\n"
       << "nevts=" << numEvents << "\n"
       << "seed=%seed%\n"
       << "crosssection=" << crosssection << "\n"
       << "rivet=" << Rivet::version() << "\n"
       << "status=" << status << "\n"
       << "revision=%revision%\n"
       << "observable=%observable%\n"
       << "cuts=%cuts%\n"
       << "# END METADATA\n\n";
  
  // restore precision
  file.precision(prec0);
}

// Write Histogram1D to file
void writeHisto(ofstream& file, const Rivet::AnalysisObjectPtr obj)
{
  const string hpath = obj->path();
  const Rivet::Histo1D& histo = dynamic_cast<const Rivet::Histo1D&>(*obj);
  const size_t nBins = histo.numBins();
  
  file << "# BEGIN HISTOGRAM " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (size_t i = 0; i < nBins; ++i) {
    const YODA::HistoBin1D& bin = histo.bin(i);
    
    file << bin.xMin()   << "\t"
         << bin.xMid()  << "\t"
         << bin.xMax()  << "\t"
         << bin.height()    << "\t"
         << bin.heightErr() << "\t"
         << bin.heightErr() << "\n";
  }
  file << "# END HISTOGRAM\n\n";
  
  // increase precision for floats:
  const streamsize prec0 = file.precision(15);
  
  // Also write the Histogram1D distributions so that we can add weigthed things afterwards
  file << "# BEGIN HISTOSTATS " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (size_t i = 0; i < nBins; ++i) {
    const YODA::HistoBin1D& bin = histo.bin(i);
    
    file << bin.xMin()    << "\t"
         << bin.xMid()   << "\t"
         << bin.xMax()   << "\t"
         << bin.numEntries() << "\t"
         << bin.sumW()    << "\t"
         << bin.sumW2()   << "\t"
         << bin.sumWX()   << "\t"
         << bin.sumWX2()  << "\n";
  }
  file << "# END HISTOSTATS\n\n";
  
  // restore precision
  file.precision(prec0);
}

double getBinMean(const YODA::ProfileBin1D& bin)
{
  double y;
  try {
    y = bin.mean();
  } catch (const YODA::LowStatsError& lse) {
    y = 0.0;
  }
  return y;
}

double getBinError(const YODA::ProfileBin1D& bin)
{
  double e;
  try {
    e = bin.stdErr();
  } catch (const YODA::LowStatsError& lse) {
    e = 0.0;
  }
  return e;
}

// Write a Profile1D object to file
void writeProfile(ofstream &file, const Rivet::AnalysisObjectPtr obj)
{
  const string hpath = obj->path();
  const Rivet::Profile1D& prof = dynamic_cast<const Rivet::Profile1D&>(*obj);
  const size_t nBins = prof.numBins();
  
  file << "# BEGIN HISTOGRAM " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (int i = 0; i < nBins; ++i) {
    const YODA::ProfileBin1D& bin = prof.bin(i);
    
    file << bin.xMin()    << "\t"
         << bin.xMid()   << "\t"
         << bin.xMax()   << "\t"
         << getBinMean(bin)  << "\t"
         << getBinError(bin) << "\t"
         << getBinError(bin) << "\n";
  }
  file << "# END HISTOGRAM\n\n";
  
  // increase precision for floats:
  const streamsize prec0 = file.precision(15);
  
  file << "# BEGIN PROFILESTATS " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (int i = 0; i < nBins; ++i) {
    const YODA::ProfileBin1D& bin = prof.bin(i);
    
    file << bin.xMin()    << "\t"
         << bin.xMid()   << "\t"
         << bin.xMax()   << "\t"
         << bin.numEntries() << "\t"
         << bin.sumW()    << "\t"
         << bin.sumW2()   << "\t"
         << bin.sumWX()   << "\t"
         << bin.sumWX2()  << "\t"
         << bin.sumWY()   << "\t"
         << bin.sumWY2()  << "\t"
         << 0 << "\n";
    
    // Note: the last output value was LWH::Profile1D.getSumY2W2(i) (Rivet1/AIDA)
    // which is not available in Rivet2/YODA (which provide different quantity
    // YODA::Profile1D.bin(i).sumWXY() instead).
    // The Y2W2 or WXY are not necessary for our applications: calculations of error or mean and
    // merging of histograms and could be ommited.
    // The field is set to ZERO to keep compatibility with parser from merge code (mcprod/merge),
    // this also allows to use same merge program for both Rivet1 and Rivet2 output.
  }
  file << "# END PROFILESTATS\n\n";
  
  // restore precision
  file.precision(prec0);
}

// Write DataPointSet to file
void writeDPS(ofstream& file, const Rivet::AnalysisObjectPtr obj)
{
  const string hpath = obj->path();
  const Rivet::Scatter2D& dps = dynamic_cast<const Rivet::Scatter2D&>(*obj);
  
  file << "# BEGIN HISTOGRAM " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (size_t i = 0; i < dps.numPoints(); ++i) {
    const YODA::Point2D& p = dps.point(i);
    
    file << p.xMin() << "\t"
         << p.x()    << "\t"
         << p.xMax() << "\t"
         << p.y()    << "\t"
         << p.yErrMinus() << "\t"
         << p.yErrPlus()  << "\n";
  }
  file << "# END HISTOGRAM\n\n";
}

// A small function that calls either writeHisto or writeProfile, depending on the observable
bool writeFinal(ofstream& file, const Rivet::AnalysisObjectPtr obj)
{
  const string type = obj->type();
  const string path = obj->path();
  
  if (type == "Profile1D") {
    writeProfile(file, obj);
    return true;
  }
  
  if (type == "Histo1D") {
    writeHisto(file, obj);
    return true;
  }
  
  if (type == "Scatter2D") {
    writeDPS(file, obj);
    return true;
  }
  
  cout << "ERROR: unsupported output type " << type << " (" << path << ")" << endl;
  return false;
}

bool dumpState(const string& root, const Rivet::AnalysisHandler& rivet)
{
  const int numEvents = rivet.numEvents();
  const double crossSection = rivet.crossSection();
  const vector<Rivet::AnalysisObjectPtr>& data = rivet.getData();
  
  // query analyses statuses (VALIDATED, UNVALIDATED, etc)
  map<string, string> name_to_status;
  typedef set<Rivet::AnaHandle, Rivet::CmpAnaHandle> AnalysesSet;
  const AnalysesSet& as = rivet.analyses();
  for (AnalysesSet::const_iterator i = as.begin(); i != as.end(); ++i)
    name_to_status[(*i)->name()] = (*i)->status();
  
  for (size_t i = 0; i < data.size(); ++i) {
    const Rivet::AnalysisObjectPtr obj = data[i];
    const string path = obj->path();
    const string name = getAnalysisName(path);
    const string status = name_to_status[name];
    const string fname = root + "/" + getOutputFilename(path);
    //cout << "name = " << name << " status = " << status << " path = " << path << " fname = " << fname << endl;
    
    // skip several auxially counter histograms introduced in rivet 2.4.0
    // (path starts with "/_" for such histograms, so far /_EVTCOUNT and /_XSEC exist)
    if (path.substr(0, 2) == "/_") continue;
    
    ofstream file(fname.c_str());
    
    if (!file) {
      cout << "ERROR: failed to open output file " << fname << endl;
      return false;
    }
    
    writeHeader(file, numEvents, crossSection, status);
    writeFinal(file, obj);
  }
}

#endif
