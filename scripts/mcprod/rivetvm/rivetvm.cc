#include "Rivet/AnalysisHandler.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/RivetYODA.hh"

#include "HepMC/GenEvent.h"
#include <fstream>
#include "tools.h"

using namespace std;

int main(int argc, char* argv[])
{
  // Vector with Rivet analyses
  vector<string> analyses;
  
  // HepMC input file
  string input_file = "input.hepmc";
  
  // .dat histograms output directory
  string output_dat_dir = ".";
  
  // YODA output file
  string output_yoda = "output.yoda";
  
  // dump directory for screensaver
  string output_dump_dir = "";
  
  // YODA calibration input files
  vector<string> calibration_files;
  
  // Iterate over command-line arguments
  for (int i = 1; i < argc; i++) {
    const string par = argv[i];
    const string val = (i < argc - 1) ? argv[i+1] : "";
    
    if (par == "--list-analyses") {
      listAvailableAnalyses();
      return 0;
    }
    else if (par == "-a") {
      analyses.push_back(val);
    }
    else if (par == "-i") {
      input_file = val;
    }
    else if (par == "-o") {
      output_dat_dir = val;
    }
    else if (par == "-H") {
      output_yoda = val;
    }
    else if (par == "-d") {
      output_dump_dir = val;
    }
    else if (par == "-I") {
      calibration_files.push_back(val);
    }
  }
  
  // Check if analyses have been supplied
  if (analyses.size() == 0) {
    cerr << "ERROR: no analysis specified" << endl;
    return 1;
  }
  
  // Check analyses names
  foreach (const string& i, analyses) {
    if (!checkAnalysis(i)) {
      cerr << "ERROR: requested analysis " << i << " is not available" << endl;
      return 1;
    }
  }
  
  // Rivet
  Rivet::AnalysisHandler rivet;
  rivet.addAnalyses(analyses);
  
  // open input file
  ifstream is(input_file.c_str());
  
  if (!is) {
    cerr << "ERROR: failed to open input file " << input_file << endl;
    return 1;
  }
  
  // create an empty event
  HepMC::GenEvent evt;

#if HIMODE == 1
  // read calibration files
  for (auto calibration_file : calibration_files) {
    rivet.readData(calibration_file);
  }
#else
  if (!calibration_files.empty()) {
    cerr << "ERROR: calibration files are not supported in non-HI mode!" << endl;
    return 1;
  }
#endif
  
  // loop over the input stream
  while (is) {
    // read the event
    evt.read(is);
    
    if (is.bad()) {
      cerr << "ERROR: failed to read events from input file" << endl;
      return 1;
    }
    
    // skip invalid event (no particles or vertices)
    if (! evt.is_valid()) continue;
    
    // analyze the event
    rivet.analyze(evt);
    
    // print progress
    if (rivet.numEvents() % 100 == 0) {
      cout << rivet.numEvents() << " events processed" << endl;
    }
    
    // dump histograms for display every 100 up to 1000 and then every 1000
    if ((output_dump_dir != "") && 
        ( (rivet.numEvents() % 1000 == 0) ||
          ((rivet.numEvents() < 1000) && (rivet.numEvents() % 100 == 0)) )
       ) {
      cout << "dumping histograms..." << endl;
      //printState(rivet);
      dumpState(output_dump_dir, rivet);
    }
  }
  
  // finalize a run
  rivet.finalize();
  
  // write out histograms
  //printState(rivet);
  dumpState(output_dat_dir, rivet);
  
  // dump all histograms to .yoda file
  rivet.writeData(output_yoda);
  
  return 0;
}
