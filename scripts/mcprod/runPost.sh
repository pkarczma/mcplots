#!/bin/bash

# setup paths and environment to run Rivet in postprocessing mode
set_post_environment () {
  echo "Entering heavy-ion mode"
  export HIMODE=1
  source ./runRivet.sh load
  echo "Environment loaded"
  echo "------------------"
}

weed () {
  sed 's,^#.*,,' | sed '/^$/ d' | sed 's,  *, ,g'
}

list_post_analyses () {
  cat configuration/rivet-histograms.map | weed | grep "\- 1$" \
      | cut -d ' ' -f 5 | sed 's,_[^_]*$,,' | uniq -d
}

list_post_histograms () {
  cat configuration/rivet-histograms.map | weed | sort | grep "\- 1$"
}

list_nonpost_analysis_histograms () {
  cat configuration/rivet-histograms.map | weed | sort | grep -v "\- 1$" | grep $1 | cut -d ' ' -f 5 | sed 's,.*_,,'
}

run_post () {
  echo "Running postprocessing..."
  
  local workd=$(pwd)
  
  echo "Building rivetvm ..."
  make -B -C rivetvm rivetpost.exe HEPMC=$HEPMC RIVET=$RIVET CGAL=$CGAL HIMODE=$HIMODE
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to compile rivetvm"
    exit 1
  fi
  echo ""
  
  local mode=$1
  if [[ $mode != "all" && $mode != "filter" ]]; then
    echo "ERROR: unknown mode option"
    exit 1
  fi
  
  # find all histograms in rivet-histograms.map which require postprocessing
  echo "Histograms which require postprocessing:"
  list_post_histograms
  
  # find analyses in rivet-histograms.map which require postprocessing
  echo "Analyses which require postprocessing:"
  list_post_analyses
  
  # loop over all available analysis-generator-version-tunes
  find $workd/YODAIO/ -mindepth 4 -maxdepth 4 -type d | while read tunepath ; do
    tunepath=${tunepath#"$workd/YODAIO/"}
    analysis=$(echo $tunepath | cut -d'/' -f 1)
    generator=$(echo $tunepath | cut -d'/' -f 2)
    version=$(echo $tunepath | cut -d'/' -f 3)
    tune=$(echo $tunepath | cut -d'/' -f 4)
    local nonpostH=$(list_nonpost_analysis_histograms $analysis)
    local analysisdir="$workd/YODAIO/$analysis"
    echo "+++++++++++++++++++++++++++++"
    echo "$analysis $generator $version $tune... "
    local skip=0
    # find all available YODA files
    yodafiles=$(find $analysisdir/$generator/$version/$tune -name '*.yoda')
    echo $yodafiles
    # check if analysis requires postprocessing
    local postanalyses=$(list_post_analyses)
    if [[ $postanalyses != *"$analysis"* ]]; then 
      echo "Analysis $analysis does not require postprocessing!"
      skip=1
    fi
    # check if all analysis histograms are available
    for histogram in $nonpostH; do
      if [[ `grep -R "^Path=/$analysis/$histogram$" $yodafiles | wc -l` != 1 ]]; then
	echo "Histogram $histogram not found... "
	skip=1
	break
      fi
    done
    if [[ $skip == 1 ]]; then
      echo "Skipping postprocessing"
      break
    fi
    # prepare list of YODA input files
    local yodainput="$(echo "$yodafiles" | sed 's,^,-Y ,' | xargs)"
    # run rivetpost.exe in postprocessing mode
    local rivetExecString="$workd/rivetvm/rivetpost.exe -a $analysis $yodainput"      
    $rivetExecString
  done
  
  echo "Done"
}

# print usage info
if [[ "$#" < "1" ]] ; then
  echo "runPost.sh: script for running postprocessing"
  echo "Usage:"
  echo "  ./runPost.sh [mode] {filter}"
  echo "        [mode]   - all, filter"
  echo "        {filter} - optional parameter for filtering analyses, applied in filter mode"
  echo ""
  echo "Examples:"
  echo "  ./runPost.sh all"
  echo "  ./runPost.sh filter \"PbPb 2760 heavyion-mb\""
  exit 1
fi

set_post_environment

run_post $1 $2