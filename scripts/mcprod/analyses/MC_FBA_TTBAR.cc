// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/RivetYODA.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
/// @todo Include more projections as required, e.g. ChargedFinalState, FastJets, ZFinder...

namespace Rivet {


  namespace {
    const long TOP=6;
    const long ATOP=-TOP;
  }


  class MC_FBA_TTBAR : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    /// Constructor
    MC_FBA_TTBAR() : Analysis("MC_FBA_TTBAR") {
      setNeedsCrossSection(true);
    }

    //@}


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      /// @todo Initialise and register projections here

      IdentifiedFinalState ifs(Cuts::abseta < 10);
      ifs.acceptId(TOP);
      ifs.acceptId(ATOP);
      addProjection(ifs,"IFS");

      /// @todo Book histograms here, e.g.:
      // _h_XXXX = bookProfile1D(1, 1, 1);
      // _h_YYYY = bookHisto1D(2, 1, 1);

      _h_ttdy=bookHisto1D("dyttbar.all",50,-5.0,5.0);
      _h_ttdy_fw=bookHisto1D("dyttbar.fw",50,-5.0,5.0);
      _h_ttdy_bw=bookHisto1D("dyttbar.bw",50,-5.0,5.0);

      _h_ttody=bookHisto1D("moddyttbar.all",20,0.0,4.0);
      _h_ttody_fw=bookHisto1D("moddyttbar.fw",20,0.0,4.0);
      _h_ttody_bw=bookHisto1D("moddyttbar.bw",20,0.0,4.0);

      _h_ttdphi=bookHisto1D("dphittbar.all",40,0.0,M_PI);
      _h_ttdphi_fw=bookHisto1D("dphittbar.fw",40,0.0,M_PI);
      _h_ttdphi_bw=bookHisto1D("dphittbar.bw",40,0.0,M_PI);

      _h_ttpt=bookHisto1D("pTttbar.all",90,0.0,360.0);
      _h_ttpt_fw=bookHisto1D("pTttbar.fw",90,0.0,360.0);
      _h_ttpt_bw=bookHisto1D("pTttbar.bw",90,0.0,360.0);

      _h_topt=bookHisto1D("pTtop.all",80,0.0,480.0);
      _h_topt_fw=bookHisto1D("pTtop.fw",80,0.0,480.0);
      _h_topt_bw=bookHisto1D("pTtop.bw",80,0.0,480.0);
      _h_atpt=bookHisto1D("pTatop.all",80,0.0,480.0);
      _h_atpt_fw=bookHisto1D("pTatop.fw",80,0.0,480.0);
      _h_atpt_bw=bookHisto1D("pTatop.bw",80,0.0,480.0);

      _h_tty=bookHisto1D("yttbar.all",60,-3.0,3.0);
      _h_tty_fw=bookHisto1D("yttbar.fw",60,-3.0,3.0);
      _h_tty_bw=bookHisto1D("yttbar.bw",60,-3.0,3.0);

      _h_toy=bookHisto1D("ytop.all",100,-5.0,5.0);
      _h_toy_fw=bookHisto1D("ytop.fw",100,-5.0,5.0);
      _h_toy_bw=bookHisto1D("ytop.bw",100,-5.0,5.0);
      _h_aty=bookHisto1D("yatop.all",100,-5.0,5.0);
      _h_aty_fw=bookHisto1D("yatop.fw",100,-5.0,5.0);
      _h_aty_bw=bookHisto1D("yatop.bw",100,-5.0,5.0);

      _h_ttm=bookHisto1D("mttbar.all",100,0.0,1000.0);
      _h_ttm_fw=bookHisto1D("mttbar.fw",100,0.0,1000.0);
      _h_ttm_bw=bookHisto1D("mttbar.bw",100,0.0,1000.0);

      _h_xsectot=bookHisto1D("xsectot",1,-0.5,0.5);
      _h_xseccut=bookHisto1D("xseccut",17,-0.5,16.5);


      //ttbar mass cut
      _h_ttdy_mttc=bookHisto1D("dyttbar.mttc.lower",50,-5.0,5.0);
      _h_ttdphi_mttc=bookHisto1D("lowMtt.dphittbar.all",40,0.0,M_PI);
      _h_ttpt_mttc=bookHisto1D("lowMtt.pTttbar.all",90,0.0,360.0);
      _h_topt_mttc=bookHisto1D("lowMtt.pTtop.all",80,0.0,480.0);
      _h_tty_mttc=bookHisto1D("lowMtt.yttbar.all",60,-3.0,3.0);
      _h_ttdphi_mttc_fw=bookHisto1D("lowMtt.dphittbar.fw",40,0.0,M_PI);
      _h_ttpt_mttc_fw=bookHisto1D("lowMtt.pTttbar.fw",90,0.0,360.0);
      _h_topt_mttc_fw=bookHisto1D("lowMtt.pTtop.fw",80,0.0,480.0);
      _h_tty_mttc_fw=bookHisto1D("lowMtt.yttbar.fw",60,-3.0,3.0);
      _h_ttdphi_mttc_bw=bookHisto1D("lowMtt.dphittbar.bw",40,0.0,M_PI);
      _h_ttpt_mttc_bw=bookHisto1D("lowMtt.pTttbar.bw",90,0.0,360.0);
      _h_topt_mttc_bw=bookHisto1D("lowMtt.pTtop.bw",80,0.0,480.0);
      _h_tty_mttc_bw=bookHisto1D("lowMtt.yttbar.bw",60,-3.0,3.0);

      _h_ttdy_Mttc=bookHisto1D("dyttbar.mttc.upper",50,-5.0,5.0);
      _h_ttdphi_Mttc=bookHisto1D("highMtt.dphittbar.all",40,0.0,M_PI);
      _h_ttpt_Mttc=bookHisto1D("highMtt.pTttbar.all",90,0.0,360.0);
      _h_topt_Mttc=bookHisto1D("highMtt.pTtop.all",80,0.0,480.0);
      _h_tty_Mttc=bookHisto1D("highMtt.yttbar.all",60,-3.0,3.0);
      _h_ttdphi_Mttc_fw=bookHisto1D("highMtt.dphittbar.fw",40,0.0,M_PI);
      _h_ttpt_Mttc_fw=bookHisto1D("highMtt.pTttbar.fw",90,0.0,360.0);
      _h_topt_Mttc_fw=bookHisto1D("highMtt.pTtop.fw",80,0.0,480.0);
      _h_tty_Mttc_fw=bookHisto1D("highMtt.yttbar.fw",60,-3.0,3.0);
      _h_ttdphi_Mttc_bw=bookHisto1D("highMtt.dphittbar.bw",40,0.0,M_PI);
      _h_ttpt_Mttc_bw=bookHisto1D("highMtt.pTttbar.bw",90,0.0,360.0);
      _h_topt_Mttc_bw=bookHisto1D("highMtt.pTtop.bw",80,0.0,480.0);
      _h_tty_Mttc_bw=bookHisto1D("highMtt.yttbar.bw",60,-3.0,3.0);


      //ttbar pt cut to separate Sudakov from hard-PT region
      _h_ttdy_ptc=bookHisto1D("dyttbar.ptc.lower",50,-5.0,5.0);
      _h_ttdphi_ptc=bookHisto1D("z.lowPT.dphittbar.all",40,0.0,M_PI);
      _h_ttm_ptc=bookHisto1D("z.lowPT.mttbar.all",100,0.0,1000.0);
      _h_topt_ptc=bookHisto1D("z.lowPT.pTtop.all",80,0.0,480.0);
      _h_tty_ptc=bookHisto1D("z.lowPT.yttbar.all",60,-3.0,3.0);
      _h_ttdphi_ptc_fw=bookHisto1D("z.lowPT.dphittbar.fw",40,0.0,M_PI);
      _h_ttm_ptc_fw=bookHisto1D("z.lowPT.mttbar.fw",100,0.0,1000.0);
      _h_topt_ptc_fw=bookHisto1D("z.lowPT.pTtop.fw",80,0.0,480.0);
      _h_tty_ptc_fw=bookHisto1D("z.lowPT.yttbar.fw",60,-3.0,3.0);
      _h_ttdphi_ptc_bw=bookHisto1D("z.lowPT.dphittbar.bw",40,0.0,M_PI);
      _h_ttm_ptc_bw=bookHisto1D("z.lowPT.mttbar.bw",100,0.0,1000.0);
      _h_topt_ptc_bw=bookHisto1D("z.lowPT.pTtop.bw",80,0.0,480.0);
      _h_tty_ptc_bw=bookHisto1D("z.lowPT.yttbar.bw",60,-3.0,3.0);

      _h_ttdy_Ptc=bookHisto1D("dyttbar.ptc.upper",50,-5.0,5.0);
      _h_ttdphi_Ptc=bookHisto1D("z.highPT.dphittbar.all",40,0.0,M_PI);
      _h_ttm_Ptc=bookHisto1D("z.highPT.mttbar.all",100,0.0,1000.0);
      _h_topt_Ptc=bookHisto1D("z.highPT.pTtop.all",80,0.0,480.0);
      _h_tty_Ptc=bookHisto1D("z.highPT.yttbar.all",60,-3.0,3.0);
      _h_ttdphi_Ptc_fw=bookHisto1D("z.highPT.dphittbar.fw",40,0.0,M_PI);
      _h_ttm_Ptc_fw=bookHisto1D("z.highPT.mttbar.fw",100,0.0,1000.0);
      _h_topt_Ptc_fw=bookHisto1D("z.highPT.pTtop.fw",80,0.0,480.0);
      _h_tty_Ptc_fw=bookHisto1D("z.highPT.yttbar.fw",60,-3.0,3.0);
      _h_ttdphi_Ptc_bw=bookHisto1D("z.highPT.dphittbar.bw",40,0.0,M_PI);
      _h_ttm_Ptc_bw=bookHisto1D("z.highPT.mttbar.bw",100,0.0,1000.0);
      _h_topt_Ptc_bw=bookHisto1D("z.highPT.pTtop.bw",80,0.0,480.0);
      _h_tty_Ptc_bw=bookHisto1D("z.highPT.yttbar.bw",60,-3.0,3.0);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      const double weight = event.weight();
      _h_xsectot->fill(0,weight);
      _h_xseccut->fill(0,weight);

      /// @todo Do the event by event analysis here

      const IdentifiedFinalState& ifs
	=applyProjection<IdentifiedFinalState>(event,"IFS");
      ParticleVector allp=ifs.particlesByPt();
      if(allp.size()<2) vetoEvent;
      _h_xseccut->fill(1,weight);

      bool ftop=false, ftbar=false;
      FourMomentum momtop, momtbar;
      for(size_t i=0; i<allp.size(); ++i) {
	if(!ftop && allp[i].pdgId()==TOP) {
	  ftop=true; momtop=allp[i].momentum(); if(ftbar) break;
	}
	else if(!ftbar && allp[i].pdgId()==ATOP) {
	  ftbar=true; momtbar=allp[i].momentum(); if(ftop) break;
	}
      }
      if(ftop && ftbar); else vetoEvent;
      _h_xseccut->fill(2,weight);

      const double dy=momtop.rapidity()-momtbar.rapidity();
      const double dphi=deltaPhi(momtop.phi(), momtbar.phi());
      const double ptt=momtop.pT(), pta=momtbar.pT();
      const double yt=momtop.rapidity(), ya=momtbar.rapidity();
      FourMomentum momtt=momtop+momtbar;
      const double pt=momtt.pT();
      const double rap=momtt.rapidity();
      const double mss=momtt.mass();

      //All.
      _h_ttdy->fill(dy,weight); _h_ttody->fill(fabs(dy),weight);
      _h_ttdphi->fill(dphi,weight); _h_ttpt->fill(pt,weight);
      _h_topt->fill(ptt,weight); _h_atpt->fill(pta,weight);
      _h_tty->fill(rap,weight);
      _h_toy->fill(yt,weight); _h_aty->fill(ya,weight);
      _h_ttm->fill(mss,weight);
      if(dy>=0.0) {
	_h_xseccut->fill(3,weight);
	_h_ttdy_fw->fill(dy,weight); _h_ttody_fw->fill(fabs(dy),weight);
	_h_ttdphi_fw->fill(dphi,weight); _h_ttpt_fw->fill(pt,weight);
	_h_topt_fw->fill(ptt,weight); _h_atpt_fw->fill(pta,weight);
	_h_tty_fw->fill(rap,weight);
	_h_toy_fw->fill(yt,weight); _h_aty_fw->fill(ya,weight);
	_h_ttm_fw->fill(mss,weight);
      } else {
	_h_xseccut->fill(4,weight);
	_h_ttdy_bw->fill(dy,weight); _h_ttody_bw->fill(fabs(dy),weight);
	_h_ttdphi_bw->fill(dphi,weight); _h_ttpt_bw->fill(pt,weight);
	_h_topt_bw->fill(ptt,weight); _h_atpt_bw->fill(pta,weight);
	_h_tty_bw->fill(rap,weight);
	_h_toy_bw->fill(yt,weight); _h_aty_bw->fill(ya,weight);
	_h_ttm_bw->fill(mss,weight);
      }

      //Mass cut.
      if(mss<=450.0) {
	_h_xseccut->fill(5,weight);
	_h_ttdy_mttc->fill(dy,weight); _h_ttdphi_mttc->fill(dphi,weight);
	_h_ttpt_mttc->fill(pt,weight); _h_topt_mttc->fill(ptt,weight);
	_h_tty_mttc->fill(rap,weight);
	if(dy>=0.0) {
	  _h_xseccut->fill(6,weight);
	  _h_ttdphi_mttc_fw->fill(dphi,weight);
	  _h_ttpt_mttc_fw->fill(pt,weight); _h_topt_mttc_fw->fill(ptt,weight);
	  _h_tty_mttc_fw->fill(rap,weight);
	} else {
	  _h_xseccut->fill(7,weight);
	  _h_ttdphi_mttc_bw->fill(dphi,weight);
	  _h_ttpt_mttc_bw->fill(pt,weight); _h_topt_mttc_bw->fill(ptt,weight);
	  _h_tty_mttc_bw->fill(rap,weight);
	}
      } else {
	_h_xseccut->fill(8,weight);
	_h_ttdy_Mttc->fill(dy,weight); _h_ttdphi_Mttc->fill(dphi,weight);
	_h_ttpt_Mttc->fill(pt,weight); _h_topt_Mttc->fill(ptt,weight);
	_h_tty_Mttc->fill(rap,weight);
	if(dy>=0.0) {
	  _h_xseccut->fill(9,weight);
	  _h_ttdphi_Mttc_fw->fill(dphi,weight);
	  _h_ttpt_Mttc_fw->fill(pt,weight); _h_topt_Mttc_fw->fill(ptt,weight);
	  _h_tty_Mttc_fw->fill(rap,weight);
	} else {
	  _h_xseccut->fill(10,weight);
	  _h_ttdphi_Mttc_bw->fill(dphi,weight);
	  _h_ttpt_Mttc_bw->fill(pt,weight); _h_topt_Mttc_bw->fill(ptt,weight);
	  _h_tty_Mttc_bw->fill(rap,weight);
	}
      }

      //PT cut.
      if(pt<=50.0) {    //Sudakov region.
	_h_xseccut->fill(11,weight);
	_h_ttdy_ptc->fill(dy,weight); _h_ttdphi_ptc->fill(dphi,weight);
	_h_ttm_ptc->fill(mss,weight); _h_topt_ptc->fill(ptt,weight);
	_h_tty_ptc->fill(rap,weight);
	if(dy>=0.0) {
	  _h_xseccut->fill(12,weight);
	  _h_ttdphi_ptc_fw->fill(dphi,weight);
	  _h_ttm_ptc_fw->fill(mss,weight); _h_topt_ptc_fw->fill(ptt,weight);
	  _h_tty_ptc_fw->fill(rap,weight);
	} else {
	  _h_xseccut->fill(13,weight);
	  _h_ttdphi_ptc_bw->fill(dphi,weight);
	  _h_ttm_ptc_bw->fill(mss,weight); _h_topt_ptc_bw->fill(ptt,weight);
	  _h_tty_ptc_bw->fill(rap,weight);
	}
      } else {
	_h_xseccut->fill(14,weight);
	_h_ttdy_Ptc->fill(dy,weight); _h_ttdphi_Ptc->fill(dphi,weight);
	_h_ttm_Ptc->fill(mss,weight); _h_topt_Ptc->fill(ptt,weight);
	_h_tty_Ptc->fill(rap,weight);
	if(dy>=0.0) {
	  _h_xseccut->fill(15,weight);
	  _h_ttdphi_Ptc_fw->fill(dphi,weight);
	  _h_ttm_Ptc_fw->fill(mss,weight); _h_topt_Ptc_fw->fill(ptt,weight);
	  _h_tty_Ptc_fw->fill(rap,weight);
	} else {
	  _h_xseccut->fill(16,weight);
	  _h_ttdphi_Ptc_bw->fill(dphi,weight);
	  _h_ttm_Ptc_bw->fill(mss,weight); _h_topt_Ptc_bw->fill(ptt,weight);
	  _h_tty_Ptc_bw->fill(rap,weight);
	}
      }

    }
    
    void calc_asymm(const string& hname, const Histo1D& a, const Histo1D& b) {
      Scatter2DPtr hist(YODA::asymm(a, b).newclone());
      hist->setPath(histoPath(hname));
      addAnalysisObject(hist);
      MSG_TRACE("Made histogram " << hname <<  " for " << name());
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      /// @todo Normalise, scale and otherwise manipulate histograms here

      // scale(_h_YYYY, crossSection()/sumOfWeights()); # norm to cross section
      // normalize(_h_YYYY); # normalize to unity

      calc_asymm("pTttbar.asym", *_h_ttpt_fw, *_h_ttpt_bw);
      calc_asymm("mttbar.asym", *_h_ttm_fw, *_h_ttm_bw);
      calc_asymm("yttbar.asym", *_h_tty_fw, *_h_tty_bw);
      calc_asymm("dyttbar.asym", *_h_ttdy_fw, *_h_ttdy_bw);
      calc_asymm("moddyttbar.asym", *_h_ttody_fw, *_h_ttody_bw);
      calc_asymm("dphittbar.asym", *_h_ttdphi_fw, *_h_ttdphi_bw);
      calc_asymm("pTtop.asym", *_h_topt_fw, *_h_topt_bw);
      calc_asymm("pTatop.asym", *_h_atpt_fw, *_h_atpt_bw);
      calc_asymm("ytop.asym", *_h_toy_fw, *_h_toy_bw);
      calc_asymm("yatop.asym", *_h_aty_fw, *_h_aty_bw);

      //Mass cut.
      calc_asymm("lowMtt.dphittbar.asym", *_h_ttdphi_mttc_fw, *_h_ttdphi_mttc_bw);
      calc_asymm("highMtt.dphittbar.asym", *_h_ttdphi_Mttc_fw, *_h_ttdphi_Mttc_bw);
      calc_asymm("lowMtt.pTttbar.asym", *_h_ttpt_mttc_fw, *_h_ttpt_mttc_bw);
      calc_asymm("highMtt.pTttbar.asym", *_h_ttpt_Mttc_fw, *_h_ttpt_Mttc_bw);
      calc_asymm("lowMtt.pTtop.asym", *_h_topt_mttc_fw, *_h_topt_mttc_bw);
      calc_asymm("highMtt.pTtop.asym", *_h_topt_Mttc_fw, *_h_topt_Mttc_bw);
      calc_asymm("lowMtt.yttbar.asym", *_h_tty_mttc_fw, *_h_tty_mttc_bw);
      calc_asymm("highMtt.yttbar.asym", *_h_tty_Mttc_fw, *_h_tty_Mttc_bw);

      //PT cut.
      calc_asymm("z.lowPT.dphittbar.asym", *_h_ttdphi_ptc_fw, *_h_ttdphi_ptc_bw);
      calc_asymm("z.highPT.dphittbar.asym", *_h_ttdphi_Ptc_fw, *_h_ttdphi_Ptc_bw);
      calc_asymm("z.lowPT.mttbar.asym", *_h_ttm_ptc_fw, *_h_ttm_ptc_bw);
      calc_asymm("z.highPT.mttbar.asym", *_h_ttm_Ptc_fw, *_h_ttm_Ptc_bw);
      calc_asymm("z.lowPT.pTtop.asym", *_h_topt_ptc_fw, *_h_topt_ptc_bw);
      calc_asymm("z.highPT.pTtop.asym", *_h_topt_Ptc_fw, *_h_topt_Ptc_bw);
      calc_asymm("z.lowPT.yttbar.asym", *_h_tty_ptc_fw, *_h_tty_ptc_bw);
      calc_asymm("z.highPT.yttbar.asym", *_h_tty_Ptc_fw, *_h_tty_Ptc_bw);

      scale(_h_ttdy, crossSection()/sumOfWeights());
      scale(_h_ttdy_fw, crossSection()/sumOfWeights());
      scale(_h_ttdy_bw, crossSection()/sumOfWeights());
      scale(_h_ttody, crossSection()/sumOfWeights());
      scale(_h_ttody_fw, crossSection()/sumOfWeights());
      scale(_h_ttody_bw, crossSection()/sumOfWeights());
      scale(_h_ttdphi, crossSection()/sumOfWeights());
      scale(_h_ttdphi_fw, crossSection()/sumOfWeights());
      scale(_h_ttdphi_bw, crossSection()/sumOfWeights());
      scale(_h_ttpt, crossSection()/sumOfWeights());
      scale(_h_ttpt_fw, crossSection()/sumOfWeights());
      scale(_h_ttpt_bw, crossSection()/sumOfWeights());
      scale(_h_topt, crossSection()/sumOfWeights());
      scale(_h_topt_fw, crossSection()/sumOfWeights());
      scale(_h_topt_bw, crossSection()/sumOfWeights());
      scale(_h_atpt, crossSection()/sumOfWeights());
      scale(_h_atpt_fw, crossSection()/sumOfWeights());
      scale(_h_atpt_bw, crossSection()/sumOfWeights());
      scale(_h_tty, crossSection()/sumOfWeights());
      scale(_h_tty_fw, crossSection()/sumOfWeights());
      scale(_h_tty_bw, crossSection()/sumOfWeights());
      scale(_h_toy, crossSection()/sumOfWeights());
      scale(_h_toy_fw, crossSection()/sumOfWeights());
      scale(_h_toy_bw, crossSection()/sumOfWeights());
      scale(_h_aty, crossSection()/sumOfWeights());
      scale(_h_aty_fw, crossSection()/sumOfWeights());
      scale(_h_aty_bw, crossSection()/sumOfWeights());
      scale(_h_ttm, crossSection()/sumOfWeights());
      scale(_h_ttm_fw, crossSection()/sumOfWeights());
      scale(_h_ttm_bw, crossSection()/sumOfWeights());

      scale(_h_ttdy_mttc, crossSection()/sumOfWeights());
      scale(_h_ttdphi_mttc, crossSection()/sumOfWeights());
      scale(_h_ttpt_mttc, crossSection()/sumOfWeights());
      scale(_h_topt_mttc, crossSection()/sumOfWeights());
      scale(_h_tty_mttc, crossSection()/sumOfWeights());
      scale(_h_ttdphi_mttc_fw, crossSection()/sumOfWeights());
      scale(_h_ttpt_mttc_fw, crossSection()/sumOfWeights());
      scale(_h_topt_mttc_fw, crossSection()/sumOfWeights());
      scale(_h_tty_mttc_fw, crossSection()/sumOfWeights());
      scale(_h_ttdphi_mttc_bw, crossSection()/sumOfWeights());
      scale(_h_ttpt_mttc_bw, crossSection()/sumOfWeights());
      scale(_h_topt_mttc_bw, crossSection()/sumOfWeights());
      scale(_h_tty_mttc_bw, crossSection()/sumOfWeights());

      scale(_h_ttdy_Mttc, crossSection()/sumOfWeights());
      scale(_h_ttdphi_Mttc, crossSection()/sumOfWeights());
      scale(_h_ttpt_Mttc, crossSection()/sumOfWeights());
      scale(_h_topt_Mttc, crossSection()/sumOfWeights());
      scale(_h_tty_Mttc, crossSection()/sumOfWeights());
      scale(_h_ttdphi_Mttc_fw, crossSection()/sumOfWeights());
      scale(_h_ttpt_Mttc_fw, crossSection()/sumOfWeights());
      scale(_h_topt_Mttc_fw, crossSection()/sumOfWeights());
      scale(_h_tty_Mttc_fw, crossSection()/sumOfWeights());
      scale(_h_ttdphi_Mttc_bw, crossSection()/sumOfWeights());
      scale(_h_ttpt_Mttc_bw, crossSection()/sumOfWeights());
      scale(_h_topt_Mttc_bw, crossSection()/sumOfWeights());
      scale(_h_tty_Mttc_bw, crossSection()/sumOfWeights());

      scale(_h_ttdy_ptc, crossSection()/sumOfWeights());
      scale(_h_ttdphi_ptc, crossSection()/sumOfWeights());
      scale(_h_ttm_ptc, crossSection()/sumOfWeights());
      scale(_h_topt_ptc, crossSection()/sumOfWeights());
      scale(_h_tty_ptc, crossSection()/sumOfWeights());
      scale(_h_ttdphi_ptc_fw, crossSection()/sumOfWeights());
      scale(_h_ttm_ptc_fw, crossSection()/sumOfWeights());
      scale(_h_topt_ptc_fw, crossSection()/sumOfWeights());
      scale(_h_tty_ptc_fw, crossSection()/sumOfWeights());
      scale(_h_ttdphi_ptc_bw, crossSection()/sumOfWeights());
      scale(_h_ttm_ptc_bw, crossSection()/sumOfWeights());
      scale(_h_topt_ptc_bw, crossSection()/sumOfWeights());
      scale(_h_tty_ptc_bw, crossSection()/sumOfWeights());

      scale(_h_ttdy_Ptc, crossSection()/sumOfWeights());
      scale(_h_ttdphi_Ptc, crossSection()/sumOfWeights());
      scale(_h_ttm_Ptc, crossSection()/sumOfWeights());
      scale(_h_topt_Ptc, crossSection()/sumOfWeights());
      scale(_h_tty_Ptc, crossSection()/sumOfWeights());
      scale(_h_ttdphi_Ptc_fw, crossSection()/sumOfWeights());
      scale(_h_ttm_Ptc_fw, crossSection()/sumOfWeights());
      scale(_h_topt_Ptc_fw, crossSection()/sumOfWeights());
      scale(_h_tty_Ptc_fw, crossSection()/sumOfWeights());
      scale(_h_ttdphi_Ptc_bw, crossSection()/sumOfWeights());
      scale(_h_ttm_Ptc_bw, crossSection()/sumOfWeights());
      scale(_h_topt_Ptc_bw, crossSection()/sumOfWeights());
      scale(_h_tty_Ptc_bw, crossSection()/sumOfWeights());

      scale(_h_xsectot, crossSection()/sumOfWeights());
      scale(_h_xseccut, crossSection()/sumOfWeights());


    }

    //@}


  private:

    // Data members like post-cuts event weight counters go here


  private:

    /// @name Histograms
    //@{
    //AIDA::IProfile1D *_h_XXXX;
    //AIDA::IHistogram1D *_h_YYYY;
    Histo1DPtr  _h_ttdy;
    Histo1DPtr  _h_ttdy_fw;
    Histo1DPtr  _h_ttdy_bw;
    Histo1DPtr  _h_ttody;
    Histo1DPtr  _h_ttody_fw;
    Histo1DPtr  _h_ttody_bw;
    Histo1DPtr  _h_ttdphi;
    Histo1DPtr  _h_ttdphi_fw;
    Histo1DPtr  _h_ttdphi_bw;
    Histo1DPtr  _h_ttpt;
    Histo1DPtr  _h_ttpt_fw;
    Histo1DPtr  _h_ttpt_bw;
    Histo1DPtr  _h_topt;
    Histo1DPtr  _h_topt_fw;
    Histo1DPtr  _h_topt_bw;
    Histo1DPtr  _h_atpt;
    Histo1DPtr  _h_atpt_fw;
    Histo1DPtr  _h_atpt_bw;
    Histo1DPtr  _h_tty;
    Histo1DPtr  _h_tty_fw;
    Histo1DPtr  _h_tty_bw;
    Histo1DPtr  _h_toy;
    Histo1DPtr  _h_toy_fw;
    Histo1DPtr  _h_toy_bw;
    Histo1DPtr  _h_aty;
    Histo1DPtr  _h_aty_fw;
    Histo1DPtr  _h_aty_bw;
    Histo1DPtr  _h_ttm;
    Histo1DPtr  _h_ttm_fw;
    Histo1DPtr  _h_ttm_bw;
    Histo1DPtr  _h_xsectot;
    Histo1DPtr  _h_xseccut;
    Histo1DPtr  _h_ttdy_mttc;
    Histo1DPtr  _h_ttdphi_mttc;
    Histo1DPtr  _h_ttpt_mttc;
    Histo1DPtr  _h_topt_mttc;
    Histo1DPtr  _h_tty_mttc;
    Histo1DPtr  _h_ttdphi_mttc_fw;
    Histo1DPtr  _h_ttpt_mttc_fw;
    Histo1DPtr  _h_topt_mttc_fw;
    Histo1DPtr  _h_tty_mttc_fw;
    Histo1DPtr  _h_ttdphi_mttc_bw;
    Histo1DPtr  _h_ttpt_mttc_bw;
    Histo1DPtr  _h_topt_mttc_bw;
    Histo1DPtr  _h_tty_mttc_bw;
    Histo1DPtr  _h_ttdy_Mttc;
    Histo1DPtr  _h_ttdphi_Mttc;
    Histo1DPtr  _h_ttpt_Mttc;
    Histo1DPtr  _h_topt_Mttc;
    Histo1DPtr  _h_tty_Mttc;
    Histo1DPtr  _h_ttdphi_Mttc_fw;
    Histo1DPtr  _h_ttpt_Mttc_fw;
    Histo1DPtr  _h_topt_Mttc_fw;
    Histo1DPtr  _h_tty_Mttc_fw;
    Histo1DPtr  _h_ttdphi_Mttc_bw;
    Histo1DPtr  _h_ttpt_Mttc_bw;
    Histo1DPtr  _h_topt_Mttc_bw;
    Histo1DPtr  _h_tty_Mttc_bw;

    Histo1DPtr  _h_ttdy_ptc;
    Histo1DPtr  _h_ttdphi_ptc;
    Histo1DPtr  _h_ttm_ptc;
    Histo1DPtr  _h_topt_ptc;
    Histo1DPtr  _h_tty_ptc;
    Histo1DPtr  _h_ttdphi_ptc_fw;
    Histo1DPtr  _h_ttm_ptc_fw;
    Histo1DPtr  _h_topt_ptc_fw;
    Histo1DPtr  _h_tty_ptc_fw;
    Histo1DPtr  _h_ttdphi_ptc_bw;
    Histo1DPtr  _h_ttm_ptc_bw;
    Histo1DPtr  _h_topt_ptc_bw;
    Histo1DPtr  _h_tty_ptc_bw;
    Histo1DPtr  _h_ttdy_Ptc;
    Histo1DPtr  _h_ttdphi_Ptc;
    Histo1DPtr  _h_ttm_Ptc;
    Histo1DPtr  _h_topt_Ptc;
    Histo1DPtr  _h_tty_Ptc;
    Histo1DPtr  _h_ttdphi_Ptc_fw;
    Histo1DPtr  _h_ttm_Ptc_fw;
    Histo1DPtr  _h_topt_Ptc_fw;
    Histo1DPtr  _h_tty_Ptc_fw;
    Histo1DPtr  _h_ttdphi_Ptc_bw;
    Histo1DPtr  _h_ttm_Ptc_bw;
    Histo1DPtr  _h_topt_Ptc_bw;
    Histo1DPtr  _h_tty_Ptc_bw;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_FBA_TTBAR);

}
