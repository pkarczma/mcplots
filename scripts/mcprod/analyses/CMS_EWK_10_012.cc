// -*- C++ -*-
// AUTHOR:  Anil Singh (anil@cern.ch), Lovedeep Saini (lovedeep@cern.ch)

#include "Rivet/Analysis.hh"
#include "Rivet/Tools/RivetYODA.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/InvMassFinalState.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"


namespace Rivet {
  
  
  class CMS_EWK_10_012 : public Analysis {
  public:
    
    CMS_EWK_10_012()
      : Analysis("CMS_EWK_10_012")
    {
      setNeedsCrossSection(true);
    }
    
    
    /// Book histograms and initialise projections before the run
    void init() {
      
      const FinalState fs(-5.0,5.0);
      addProjection(fs, "FS");
      
      // Zee
      LeadingParticlesFinalState ZeeFS(FinalState(-2.5,2.5, 0.)); 
      ZeeFS.addParticleIdPair(PID::ELECTRON);
      addProjection(ZeeFS, "ZeeFS");
      // Zmm
      LeadingParticlesFinalState ZmmFS(FinalState(-2.4,2.4, 0.)); 
      ZmmFS.addParticleIdPair(PID::MUON);
      addProjection(ZmmFS, "ZmmFS");
      
      // We-nu_e~
      LeadingParticlesFinalState WminusenuFS(FinalState(-2.5,2.5, 0.)); 
      WminusenuFS.addParticleId(PID::ELECTRON).addParticleId(PID::NU_EBAR);
      addProjection(WminusenuFS, "WminusenuFS");
      
      // We+nu_e
      LeadingParticlesFinalState WplusenuFS(FinalState(-2.5,2.5, 0.));
      WplusenuFS.addParticleId(PID::POSITRON).addParticleId(PID::NU_E);
      addProjection(WplusenuFS, "WplusenuFS");
      
      // Wm+nu_mu~
      LeadingParticlesFinalState WplusmunuFS(FinalState(-2.4,2.4, 0.));
      WplusmunuFS.addParticleId(PID::ANTIMUON).addParticleId(PID::NU_MU);
      addProjection(WplusmunuFS, "WplusmunuFS");
      
      // Wm-nu_mu
      LeadingParticlesFinalState WminusmunuFS(FinalState(-2.4,2.4, 0.));
      WminusmunuFS.addParticleId(PID::MUON).addParticleId(PID::NU_MUBAR);
      addProjection(WminusmunuFS, "WminusmunuFS");

// Remove neutrinos for isolation of final state particles      
      VetoedFinalState vfs(fs);
      vfs.vetoNeutrinos();
      vfs.addVetoOnThisFinalState(ZeeFS);
      vfs.addVetoOnThisFinalState(ZmmFS);
      vfs.addVetoOnThisFinalState(WminusenuFS);
      vfs.addVetoOnThisFinalState(WminusmunuFS);
      vfs.addVetoOnThisFinalState(WplusenuFS);
      vfs.addVetoOnThisFinalState(WplusmunuFS);
      addProjection(vfs, "VFS");


      addProjection(FastJets(vfs, FastJets::ANTIKT, 0.5), "Jets");
      
      _histNoverN0Welec = bookScatter2D(1,1,1, true);   
      _histNoverNm1Welec = bookScatter2D(2,1,1, true);   
      _histNoverN0Wmu = bookScatter2D(3,1,1, true);
      _histNoverNm1Wmu = bookScatter2D(4,1,1, true);   
      _histNoverN0Zelec = bookScatter2D(5,1,1, true);   
      _histNoverNm1Zelec = bookScatter2D(6,1,1, true);   
      _histNoverN0Zmu = bookScatter2D(7,1,1, true);   
      _histNoverNm1Zmu = bookScatter2D(8,1,1, true);   
      _histJetMultWelec  = bookHisto1D("njetWenu", 5, -0.5, 4.5);
      _histJetMultWmu    = bookHisto1D("njetWmunu", 5, -0.5, 4.5);
      _histJetMultZelec  = bookHisto1D("njetZee", 5, -0.5, 4.5);
      _histJetMultZmu    = bookHisto1D("njetZmumu", 5, -0.5, 4.5);
      
      _histJetMultWmuPlus = bookHisto1D("njetWmuPlus", 5, -0.5, 4.5);
      _histJetMultWmuMinus = bookHisto1D("njetWmuMinus", 5, -0.5, 4.5);
      _histJetMultWelPlus = bookHisto1D("njetWePlus", 5, -0.5, 4.5);
      _histJetMultWelMinus = bookHisto1D("njetWeMinus", 5, -0.5, 4.5);
      _histJetMultRatioWmuPlusMinus = bookScatter2D(10, 1, 1, true);
      _histJetMultRatioWelPlusMinus = bookScatter2D(9, 1, 1, true);
//      _histWZRatioelec = bookScatter2D(11, 1, 1);
  //    _histWZRatiomu = bookScatter2D(12, 1, 1);
      _histWZnormRatioelec = bookScatter2D(11, 1, 1, true);
      _histWZnormRatiomu = bookScatter2D(12, 1, 1, true);

    } 
      void GetPtEtaPhi(Particle p1, double& pt, double& eta,double& phi){
	pt = p1.momentum().pT();
	eta = p1.momentum().eta();
        phi = p1.momentum().phi();
	return;
      }

    
      bool ApplyZAcceptance(const LeadingParticlesFinalState& zFS, std::string lepton){
	const ParticleVector& Zdaughters = zFS.particlesByPt();
	double phi1 = -9999., phi2 = -9999.;
	double pt1 = -9999., pt2 = -9999.;
	double eta1 = -9999., eta2 = -9999.;
	GetPtEtaPhi(Zdaughters[0],pt1,eta1,phi1);
	GetPtEtaPhi(Zdaughters[1],pt2,eta2,phi2);
	bool isFid1 = false;
	bool isFid2 = false;
	if(lepton=="electron"){
	isFid1 = ((fabs(eta1)<1.4442)||((fabs(eta1)>1.566)&&(fabs(eta1)<2.5)));
	isFid2 = ((fabs(eta2)<1.4442)||((fabs(eta2)>1.566)&&(fabs(eta2)<2.5)));
	}
	if(lepton=="muon"){
	  isFid1 = ((fabs(eta1)<2.1));
	  isFid2 = ((fabs(eta2)<2.4));
  	}

	if( isFid1 && isFid2 && pt1>20 && pt2 >10){
	  const FourMomentum pmom = Zdaughters[0].momentum() + Zdaughters[1].momentum();
	  double mass = sqrt(pmom.invariant());
	  if (inRange(mass/GeV, 60.0, 120.0))
	    return true;
	  else return false;
	}
	else return false;
      }
      
      bool ApplyWAcceptance(const LeadingParticlesFinalState& wFS,std::string lepton){
        const ParticleVector& Wdaughters = wFS.particles();
        double phi1 = -9999.;
        double pt1 = -9999.;
        double eta1 = -9999.;

	Particle lep;
	Particle neut;
	int lepIndex = GetLeptonIndex(wFS);
	lep = Wdaughters[lepIndex];
	if(lepIndex==0) neut = Wdaughters[lepIndex+1];
	else if(lepIndex==1) neut = Wdaughters[lepIndex-1];
        GetPtEtaPhi(Wdaughters[lepIndex],pt1,eta1,phi1);
        bool isFid = false;
        if(lepton=="electron")isFid = (((fabs(eta1)<1.4442)||((fabs(eta1)>1.566)&&(fabs(eta1)<2.5))) && pt1>20);
        if(lepton=="muon") isFid = ((fabs(eta1)<2.1) && pt1>20);
        if(!isFid)return false;
//	double mt=sqrt(2.0*lep.momentum().pT()*neut.momentum().Et()*(1.0-cos(lep.momentum().phi()-neut.momentum().phi())));
	//if (mt<20)return false;
	return true;
  
}

//pdgid
// e- 11, nue 12, mu- 13, numu 14
//why this GetLeptonIndex function?
//Wdaughters[0] has -ve pdgid and Wdaughters[1] has +ve one
//in order to apply acceptance cut on lepton, we want to know its id 
//if Wdaughters[0] is lepton, we say order1 is true and return 0 as lepton index
//if Wdaughters[1] is lepton, we say order2 is true and return 1 
//now, if lepIndex is 0, means neutrino is at position 1, else if lepIndex is 1, neut. is at 0-position 
//so, lepIndex is giving the lepton - Index and we use GetPtEtaPhi() to acess pt,eta,phi of lepton and apply acceptance cuts 
//an example of Wdaughter's prop. [pdgid1: -12 (pt1)16.0846 , pdgid2: 11 (pt2)11.181]
      const int GetLeptonIndex(const LeadingParticlesFinalState& wFS){
	const ParticleVector& Wdaughters = wFS.particles();
	double pdgId1 = fabs(Wdaughters[0].pdgId());
	double pdgId2 = fabs(Wdaughters[1].pdgId());
        //cout<<"pdgid1: "<<Wdaughters[0].pdgId()<<" (pt1)"<<Wdaughters[0].momentum().pT()
	//<<" , pdgid2: "<<Wdaughters[1].pdgId()<<" (pt2)"<<Wdaughters[1].momentum().pT()<<endl;
	bool order1 = ((pdgId1==11&&pdgId2==12)||(pdgId1==13&&pdgId2==14));
	bool order2 = ((pdgId1==12&&pdgId2==11)||(pdgId1==14&&pdgId2==13));
	if(order1 && !order2) return 0;
	else if(order2 && !order1)return 1;
	else return -99999;
      }

    
    void Fill(Histo1DPtr _histJetMult, const double& weight, std::vector<FourMomentum>& finaljet_list){
      _histJetMult->fill(0, weight);
      for (size_t i=0 ; i<finaljet_list.size() ; ++i) {
        if (i==6) break;
        _histJetMult->fill(i+1, weight);  // inclusive
      }
    }  
    
    // returns a/b
    YODA::Point2D calc_bins_ratio(const YODA::HistoBin1D& a, const YODA::HistoBin1D& b)
    {
      YODA::Point2D p(0, 0, 0, 0);
      
      if (!(isZero(a.height()) || isZero(b.height()))) {
        const double val = a.height() / b.height();
        const double err = val * sqrt(pow(a.relErr(), 2) + pow(b.relErr(), 2));
        p.setY(val, err);
      }
      
      return p;
    }
    
    void FillNoverNm1(Histo1DPtr _histJetMult, Scatter2DPtr _histNoverNm1)
    {
      for (size_t i = 1; i < _histJetMult->numBins(); i++) {
        const YODA::Point2D p = calc_bins_ratio(_histJetMult->bin(i), _histJetMult->bin(i-1));
        _histNoverNm1->point(i-1).setY(p.y(), p.yErrMinus(), p.yErrPlus());
      }
    }
    
    void FillNoverN0(Histo1DPtr _histJetMult, Scatter2DPtr _histNoverN0)
    {
      for (size_t i = 1; i < _histJetMult->numBins(); i++) {
        const YODA::Point2D p = calc_bins_ratio(_histJetMult->bin(i), _histJetMult->bin(0));
        _histNoverN0->point(i-1).setY(p.y(), p.yErrMinus(), p.yErrPlus());
      }
    }
    

    void FillWZRatioHistogramSet(Histo1DPtr _histJetMult1, Histo1DPtr _histJetMult2, Scatter2DPtr _histJetMultRatio12)
    {
      for (size_t i = 0; i < 4; ++i) {
        const YODA::Point2D p = calc_bins_ratio(_histJetMult1->bin(i), _histJetMult2->bin(i));
        _histJetMultRatio12->point(i).setY(p.y(), p.yErrMinus(), p.yErrPlus());
      }
    }


    void FillWZnormRatioHistogramSet(Histo1DPtr _histJetMult1, Histo1DPtr _histJetMult2, Scatter2DPtr _histJetMultRatio12)
    {
      std::vector<YODA::Point2D> Wy;
      for (size_t i = 1; i < _histJetMult1->numBins(); i++) {
        const YODA::Point2D p = calc_bins_ratio(_histJetMult1->bin(i), _histJetMult1->bin(0));
        Wy.push_back(p);
      }
      
      std::vector<YODA::Point2D> Zy;
      for (size_t i = 1; i < _histJetMult2->numBins(); i++) {
        const YODA::Point2D p = calc_bins_ratio(_histJetMult2->bin(i), _histJetMult2->bin(0));
        Zy.push_back(p);
      }
      
      for (size_t i = 0; i < 4; ++i) {
        double ratioWZ = 0;
        double errWZ = 0.;
        
        if (!(isZero(Wy[i].y()) || isZero(Zy[i].y()))) {
          ratioWZ = Wy[i].y() / Zy[i].y();
          double errZ = Zy[i].yErrPlus() / Zy[i].y();
          double errW = Wy[i].yErrPlus() / Wy[i].y();
          errWZ = std::sqrt(std::pow(errW,2)+std::pow(errZ,2));
        }
        
        _histJetMultRatio12->point(i).setY(ratioWZ, ratioWZ*errWZ);
      }
    }


 
    void FillChargeAssymHistogramSet(Histo1DPtr _histJetMult1, Histo1DPtr _histJetMult2, Scatter2DPtr _histJetMultRatio12)
    {
      const YODA::Scatter2D asymm = YODA::asymm(*_histJetMult1, *_histJetMult2);
      for (size_t i = 0; i < 4; ++i) {
        const YODA::Point2D& p = asymm.point(i);
        _histJetMultRatio12->point(i).setY(p.y(), p.yErrMinus(), p.yErrPlus());
      }
    }
    
  
   
    void analyze(const Event& event) {
      //some flag definitions.
      bool isZmm =false;
      bool isZee =false;
      bool isWmnMinus =false;
      bool isWmnPlus  =false;
      bool isWenMinus =false;
      bool isWenPlus  =false;
      //bool isWmn =false;
      //bool isWen =false;
      
      const double weight = event.weight();
      const LeadingParticlesFinalState& ZeeFS = applyProjection<LeadingParticlesFinalState>(event, "ZeeFS");
      const LeadingParticlesFinalState& ZmmFS = applyProjection<LeadingParticlesFinalState>(event, "ZmmFS");
      const LeadingParticlesFinalState& WminusenuFS = applyProjection<LeadingParticlesFinalState>(event, "WminusenuFS");
      const LeadingParticlesFinalState& WplusenuFS = applyProjection<LeadingParticlesFinalState>(event, "WplusenuFS");
      const LeadingParticlesFinalState& WminusmunuFS = applyProjection<LeadingParticlesFinalState>(event, "WminusmunuFS");
      const LeadingParticlesFinalState& WplusmunuFS = applyProjection<LeadingParticlesFinalState>(event, "WplusmunuFS");
      
      bool boolZ= (ZeeFS.particles().size()>1 && ZmmFS.empty()) || (ZmmFS.particles().size()>1 && ZeeFS.empty()); 
      bool boolW=(!WminusenuFS.empty() || !WplusenuFS.empty() || !WminusmunuFS.empty() || !WplusmunuFS.empty());
      double phi1 = -9999., phi2 = -9999.;
      double pt1 = -9999., pt2 = -9999.;
      double eta1 = -9999., eta2 = -9999.;

  const ParticleVector& ZeeDaus  = ZeeFS.particlesByPt();
  const ParticleVector& ZmmDaus = ZmmFS.particlesByPt();

  if(boolZ){
        //cout<<"Z"<<endl;
    if(ZeeDaus.size()==2 && ZmmDaus.size()<2){
      isZee = ApplyZAcceptance(ZeeFS,"electron");
	GetPtEtaPhi(ZeeDaus[0],pt1,eta1,phi1);
	GetPtEtaPhi(ZeeDaus[1],pt2,eta2,phi2);
    }
    if(ZmmDaus.size()==2 && ZeeDaus.size()<2){
      isZmm = ApplyZAcceptance(ZmmFS,"muon");
    }
  }
  else if(boolW)
    {
      //cout<<"W"<<endl;
      bool boolWenMinus=WplusenuFS.empty() && WplusmunuFS.empty() && WminusmunuFS.empty() ;
      bool boolWenPlus=WminusenuFS.empty() && WplusmunuFS.empty() && WminusmunuFS.empty() ;
      bool boolWmnMinus=WplusenuFS.empty() && WplusmunuFS.empty() && WminusenuFS.empty() ;
      bool boolWmnPlus=WplusenuFS.empty() && WminusenuFS.empty() && WminusmunuFS.empty() ;
      
      if (WminusenuFS.particles().size() == 2 && boolWenMinus ){
	isWenMinus = ApplyWAcceptance(WminusenuFS,"electron");
	int lep = GetLeptonIndex(WminusenuFS);
	const ParticleVector& Wdaughters = WminusenuFS.particles();
        GetPtEtaPhi(Wdaughters[lep],pt1,eta1,phi1);
      }
      
      else if (WplusenuFS.particles().size() == 2 && boolWenPlus) {
	isWenPlus=ApplyWAcceptance(WplusenuFS,"electron");   
	int lep = GetLeptonIndex(WplusenuFS);
	const ParticleVector& Wdaughters = WplusenuFS.particles();
        GetPtEtaPhi(Wdaughters[lep],pt1,eta1,phi1);
      }
      
      else if (WminusmunuFS.particles().size() == 2 && boolWmnMinus) {
	isWmnMinus=ApplyWAcceptance(WminusmunuFS,"muon");
	int lep = GetLeptonIndex(WminusmunuFS);
	const ParticleVector& Wdaughters = WminusmunuFS.particles();
	GetPtEtaPhi(Wdaughters[lep],pt1,eta1,phi1);
      }
      
      else if (WplusmunuFS.particles().size() == 2 && boolWmnPlus) {
	isWmnPlus=ApplyWAcceptance(WplusmunuFS,"muon");
	int lep = GetLeptonIndex(WplusmunuFS);
	const ParticleVector& Wdaughters = WplusmunuFS.particles();
	GetPtEtaPhi(Wdaughters[lep],pt1,eta1,phi1);
      }
      
      //if(isWenMinus||isWenPlus)isWen = true;
      //if(isWmnMinus||isWmnPlus)isWmn = true;
      
    }
     
      if(!(isZmm||isZee||isWmnPlus || isWenPlus||isWenMinus||isWmnMinus))vetoEvent;
      
      
      //Obtain the jets.
      vector<FourMomentum> finaljet_list;
      foreach (const Jet& j, applyProjection<FastJets>(event, "Jets").jetsByPt(30.0*GeV)) {
	const double jeta = j.momentum().eta();
	const double jphi = j.momentum().phi();
	const double jpt = j.momentum().pT();
	if (fabs(jeta) < 2.4) 
	  if(jpt>30){
	    if(isZee){
	      if (deltaR(eta1, phi1, jeta, jphi) > 0.3 && deltaR(eta2, phi2, jeta, jphi) > 0.3)
		finaljet_list.push_back(j.momentum());
	      continue;
	    }
	    else if( isWenPlus||isWenMinus ){
	      if (deltaR(eta1, phi1, jeta, jphi) > 0.3)
		finaljet_list.push_back(j.momentum());
	      continue;
	    }
	    
	    else  finaljet_list.push_back(j.momentum());
	  }
      }
      
      //Multiplicity plots.	
      //      if(isWen)Fill(_histJetMultWelec, weight, finaljet_list);
      if(isWenPlus || isWenMinus)Fill(_histJetMultWelec, weight, finaljet_list);
      //      if(isWmn)Fill(_histJetMultWmu, weight, finaljet_list);
      if(isWmnPlus || isWmnMinus)Fill(_histJetMultWmu, weight, finaljet_list);
      if(isWmnPlus)Fill(_histJetMultWmuPlus, weight, finaljet_list);
      if(isWmnMinus)Fill(_histJetMultWmuMinus, weight, finaljet_list);
      if(isWenPlus)Fill(_histJetMultWelPlus, weight, finaljet_list);
      if(isWenMinus)Fill(_histJetMultWelMinus, weight, finaljet_list);
      if(isZee)Fill(_histJetMultZelec, weight, finaljet_list);
      if(isZmm)Fill(_histJetMultZmu, weight, finaljet_list);
      
    }   
    
    /// Normalise histograms etc., after the run
    void finalize() {
      FillNoverNm1(_histJetMultWelec,_histNoverNm1Welec);
      FillNoverN0(_histJetMultWelec,_histNoverN0Welec);
      FillNoverNm1(_histJetMultWmu,_histNoverNm1Wmu);
      FillNoverN0(_histJetMultWmu,_histNoverN0Wmu);
      FillNoverNm1(_histJetMultZelec,_histNoverNm1Zelec);
      FillNoverN0(_histJetMultZelec,_histNoverN0Zelec);
      FillNoverNm1(_histJetMultZmu,_histNoverNm1Zmu);
      FillNoverN0(_histJetMultZmu,_histNoverN0Zmu);
      FillChargeAssymHistogramSet(_histJetMultWmuPlus,_histJetMultWmuMinus, _histJetMultRatioWmuPlusMinus);
      FillChargeAssymHistogramSet(_histJetMultWelPlus,_histJetMultWelMinus, _histJetMultRatioWelPlusMinus);
//      FillWZRatioHistogramSet(_histJetMultWelec, _histJetMultZelec,_histWZRatioelec);
  //    FillWZRatioHistogramSet(_histJetMultWmu, _histJetMultZmu,_histWZRatiomu);
      FillWZnormRatioHistogramSet(_histJetMultWelec, _histJetMultZelec,_histWZnormRatioelec);
      FillWZnormRatioHistogramSet(_histJetMultWmu, _histJetMultZmu,_histWZnormRatiomu);

      removeAnalysisObject(_histJetMultWelec);
      removeAnalysisObject(_histJetMultWmu);
      removeAnalysisObject(_histJetMultWelMinus);
      removeAnalysisObject(_histJetMultWmuMinus);
      removeAnalysisObject(_histJetMultWelPlus);
      removeAnalysisObject(_histJetMultWmuPlus);
      removeAnalysisObject(_histJetMultZelec);
      removeAnalysisObject(_histJetMultZmu);

    }

  private:
    
    Histo1DPtr  _histJetMultWelec;
    Scatter2DPtr _histNoverNm1Welec;          // n/(n-1)
    Scatter2DPtr _histNoverN0Welec;          // n/n(0)
    
    Histo1DPtr  _histJetMultWmu;
    Scatter2DPtr _histNoverNm1Wmu;          // n/(n-1)
    Scatter2DPtr _histNoverN0Wmu;          // n/n(0)

    Histo1DPtr  _histJetMultWelMinus;
    Histo1DPtr  _histJetMultWelPlus;
    Scatter2DPtr _histJetMultRatioWelPlusMinus;
    
    Histo1DPtr  _histJetMultWmuMinus;
    Histo1DPtr  _histJetMultWmuPlus;
    Scatter2DPtr _histJetMultRatioWmuPlusMinus;
   
    Histo1DPtr  _histJetMultZelec;
    Scatter2DPtr _histNoverNm1Zelec;          // n/(n-1)
    Scatter2DPtr _histNoverN0Zelec;          // n/n(0)

    Histo1DPtr  _histJetMultZmu;
    Scatter2DPtr _histNoverNm1Zmu;          // n/(n-1)
    Scatter2DPtr _histNoverN0Zmu;          // n/n(0)

//    Scatter2DPtr _histWZRatioelec;
//    Scatter2DPtr _histWZRatiomu;
    Scatter2DPtr _histWZnormRatioelec;
    Scatter2DPtr _histWZnormRatiomu;

  };
  
  AnalysisBuilder<CMS_EWK_10_012> plugin_CMS_EWK_10_012;
  
 
}

