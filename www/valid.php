<?php
  $t->start();

  echo "<h2>Generator Validation</h2>\n";
  echo $validationNote;
  
  // get list of available versions
  $query = mysql_query("SELECT DISTINCT version
                        FROM histograms
                        WHERE generator = '$q_generator' AND tune = '$q_tune' AND type = 'mc'
                        ORDER BY version DESC");
  $vers_avail = array();
  while ($row = mysql_fetch_assoc($query)) {
    $vers_avail[] = $row["version"];
  }
  
  // sanitize user input for versions display
  $vers_sel = $_GET["vers"];
  $vers_sel = is_array($vers_sel) ? array_intersect($vers_sel, $vers_avail) : array();
  
  // by default display 3 first versions
  if (count($vers_sel) == 0) {
    $vers_sel = array_slice($vers_avail, 0, 3);
  }
  
  // print list of available versions
  echo "  <form method=\"get\">\n";
  echo "  <input type=hidden name=query value=\"" . $_GET["query"] . "\">\n";
  echo "    <table>\n";
  echo "      <tr>\n";
  echo "        <th>Versions:</th>\n";
  echo "        <td>\n";
  
  foreach ($vers_avail as $version) {
    $chk = in_array($version, $vers_sel) ? "checked" : "";
    
    echo "  <label><input type=checkbox name=vers[] value=\"$version\" $chk><span>$version</span></label>\n";
  }
  
  echo "        </td>\n";
  echo "      </tr>\n";
  echo "      <tr>";
  echo "        <td></td>";
  echo "        <td><input type=\"submit\" value=\"Display\"></td>\n";
  echo "      </tr>\n";
  echo "    </table>\n";
  echo "  </form>\n";
  
  //get data from database
  $query = mysql_query("SELECT *
                        FROM histograms
                        WHERE type='data'
                        ORDER BY beam, process, observable, energy, cuts, version DESC");

  $mydata = array();
  //process sql result into array
  while ($row = mysql_fetch_assoc($query)) {
    $params  = $row["observable"].$safeDelimiter.$row["energy"].$safeDelimiter.$row["cuts"];
    // TODO: add beam -> beamgroup map function in config.php
    $beam = ($row["beam"] == "ee" ? "ee" : "ppppbar");
    $process = $row["process"];


    $mydata[$beam][$process][$params][] = $row;
  }

  $query = mysql_query("SELECT *
                        FROM histograms
                        WHERE generator = '$q_generator' AND tune = '$q_tune' AND version IN ('" . implode("','", $vers_sel) . "')
                        ORDER BY beam, process, observable, energy, cuts, version DESC");
  
  $mytable = array();
  
  //process sql result into array
  while ($row = mysql_fetch_assoc($query)) {
    $beam = ($row["beam"] == "ee" ? "ee" : "ppppbar");
    $process = $row["process"];
    $version = $row["version"];
    $params  = $row["observable"].$safeDelimiter.$row["energy"].$safeDelimiter.$row["cuts"];
    
    $mytable[$beam][$process][$version][$params][] = $row;
  }

  echo "<h3>Process Summary</h3>\n";
  echo "<p>(click on numbers to see individual observables)</p>\n";
  
  $t->stamp("init");
  
  echo "<table class=\"validation\">\n";

  //table Header begin
  $nvers = 0;
  $minrow="";
  $versrow="";
  $maxrow="";
  foreach ($vers_sel as $version) {
    if ($nvers > 0) {
      $minrow  .="    <th class=\"mcup\">best</th>\n";
      $versrow .="    <th class=\"mcmid\">&lt;&Delta;&gt;</th>\n";
      $maxrow  .="    <th class=\"mcdwn\">worst</th>\n";
    }
    
    $minrow  .="    <th class=\"dup\">min</th>\n";
    $versrow .="    <th class=\"dmid\">$version</th>\n";
    $maxrow  .="    <th class=\"ddwn\">max</th>\n";
    $nvers++;
  }
  echo "  <tr>\n";
  echo "    <th class=\"mn\" rowspan=\"3\">&lt;&chi;<sup>2</sup>&gt;<br>\n";
  echo "      <span class=\"smallText\">incl. 5% \"theory uncertainty\" on all points</span></th>\n";
  echo "$minrow";
  echo "  </tr>\n";
  echo "  <tr>\n$versrow  </tr>\n  <tr>\n$maxrow  </tr>\n";
  echo "\n";
  //table header end

  //table body begin >>>>>
  //loop through beam and processes (creating rows)
  foreach (array_keys($mytable) as $beam ){
    foreach (array_keys($mytable[$beam]) as $process){
      //validation row begin >>>>

      //initialization of loop
      $minrow   = "";
      $versrow  = "";
      $maxrow   = "";
      
      $prev = "";
      $lastavg  = "";
      $allchi2old = array();
      
      //loop through versions to create columns of a row begin >>>
      foreach ($vers_sel as $version) {
        // skip unselected versions
        if (!in_array($version, array_keys($mytable[$beam][$process]))) continue;
        
        $t->start();
        //comparison of version and DATA
        $curr=$version;
        $ref="data";
        $fsteertotal = array();
        $ftxttotal = array();
        //loop through observable cuts energy and count all chi2
        foreach (array_keys($mytable[$beam][$process][$version]) as $params){
          // skip records which don't have corresponding reference (DATA) entry
          if (!in_array($params,array_keys($mydata[$beam][$process]))) continue;
            
            list($observable,$energy,$cut)=explode($safeDelimiter,$params);
            //create steer file name
            $fname = str_replace(" ", "_", "$q_generator-$q_tune-$beam-$process-$observable-$energy-$cut-".$curr."-".$ref);
            $fname = str_replace("/", "_", $fname);
            $fname = str_replace("*", "x", $fname);
            $fsteer = "cache/plots/$fname.script";
            $txtf = "cache/plots/$fname";
            $ftxttotal[] = $txtf . ".txt";

            if (! file_exists("$txtf.txt")){
              //create steer file content
              $myrow0 = $mydata[$beam][$process][$params][0];
              $myrow1 = $mytable[$beam][$process][$curr][$params][0];

              prepare_plotter_steer(array($myrow0, $myrow1), $fsteer, $txtf, "valid");
              $fsteertotal[] = $fsteer;
            }
            //prepare steerfile for comparing mc1 mc2 to data
            //in first iteration do nothing, in the next and next etc.
            if ($prev!=""){
              $fname_t = str_replace(" ", "_", "$q_generator-$q_tune-$beam-$process-$observable-$energy-$cut-".$prev."-".$curr);
              $fname_t = str_replace("/", "_", $fname_t);
              $fname_t = str_replace("*", "x", $fname_t);
              $fsteer_t = "cache/plots/$fname_t.script";
              $txtf_t = "cache/plots/$fname_t";
              if (! file_exists("$txtf_t.txt")){
                //create steer file content
                $myrow0 = $mydata[$beam][$process][$params][0];
                $myrow1 = $mytable[$beam][$process][$prev][$params][0];
                $myrow2 = $mytable[$beam][$process][$curr][$params][0];

                prepare_plotter_steer(array($myrow0, $myrow1, $myrow2), $fsteer_t, $txtf_t, "valid");
              }
            }
        }
        
        $t->stamp("steer file write");
        
        if (count($fsteertotal) > 0) {
          //echo "Steer<br />$fsteertotal <br />";
          exec("./plotter.exe chi2=5 " . implode(" ", array_map("escapeshellarg", $fsteertotal)) . " >> cache/plotter.log 2>&1");
        }
        
        $t->stamp("chi2 calc");
        
        //get avg
        $allchi2=array();
        $dtchi2=array();
        //loop through all the txt file consisting of chi2 values
        foreach ($ftxttotal as $chi2data){
            $lines = file_get_contents($chi2data);
            //echo "file: $chi2data <br />";
            //echo "Content $lines";
            
            //only one value is expected, but it is possible to compare more than 2 histograms
            list($chi2, ) = explode(";", $lines);
            //echo "chi2: $chi2<br />";
            //echo "chi2o: $allchi2old[$arrind]<br />";
            
            //we don't count <0 neither to average nor to min/max
            if (($chi2 < 0) || (!is_numeric($chi2))) {
              continue;
            }
            
            $arrind = str_ireplace($version, "", $chi2data);
            $allchi2[$arrind] = $chi2;
            
            if ((!is_null($chi2)) && (!is_null($allchi2old[$arrind]))){
              //variable allchi2old is from previous step that's why it is called old
              //but it stores values of the newer generator!!!
              $dtchi2[$arrind]=($allchi2old[$arrind] - $chi2);
            }
        }
        $allchi2old=$allchi2;
        
        $t->stamp("chi2 file read");
        
        $maxchi=max($allchi2);
        $minchi=min($allchi2);
        $avgchi=array_sum($allchi2)/count($allchi2);
        
        // insert 'delta' column
        if ($prev != "") {
          $dmaxchi=max($dtchi2); //($maxchi-$lastmax)/$lastmax;
          $dminchi=min($dtchi2); //($minchi-$lastmin)/$lastmin;
          $dif = $lastavg - $avgchi;
          $davgchi=((abs($dif) < 1000) ? ((spec_format($lastavg))-(spec_format($avgchi))) : (spec_format($dif))); //lastavg is avg from previous

          $minrow  .= "    <td class=\"mcup ".get_deltColor($dminchi,true)." \">"
                           .spec_format($dminchi,true)."</td>\n";
          $param = $q_generator.$safeDelimiter.$q_tune.$safeDelimiter.$prev.$safeDelimiter.$curr;
          $versrow .= "    <td class=\"mcmid\" >" .
                      sprintf("<a class=\"%s\" href=\"%s\">%s</a>",
                        get_deltColor($davgchi),
                        prepare_link(array("validdetail",$beam,$process,"","","",$param)),
                        spec_format($davgchi,true)) .
                      "</td>\n";
          $maxrow  .= "    <td class=\"mcdwn ".get_deltColor($dmaxchi,true)."\" >"
                           .spec_format($dmaxchi,true)."</td>\n";
        }
        
        // insert main column
        $minrow  .= "    <td class=\"dup ".get_color($minchi)."\" >".spec_format($minchi)."</td>\n";
        $param = $q_generator.$safeDelimiter.$q_tune.$safeDelimiter.$curr.$safeDelimiter.$ref;
        $versrow .= "    <td class=\"dmid ".get_color($avgchi)." \">" .
                    sprintf("<a class=\"clblack\" href=\"%s\">%s</a>",
                      prepare_link(array("validdetail",$beam,$process,"","","",$param)),
                      spec_format($avgchi)) .
                    "</td>\n";
        $maxrow  .= "    <td class=\"ddwn ".get_color($maxchi)." \">".spec_format($maxchi)."</td>\n";
        
        $prev = $curr;
        $lastavg=$avgchi;
        
        $t->stamp("display");
      }
      echo "  <tr>\n";
      echo "    <td class=\"mn right-bord-thick\" rowspan=\"3\" >";
      echo $c->name($beam) . " &rarr; " . $c->name($process);
      echo "</td>\n";
      echo "$minrow";
      echo "  </tr>\n";
      echo "  <tr>\n$versrow</tr>\n  <tr>\n$maxrow</tr>\n";
      echo "\n";
      //columns end <<<
      //physics row end <<<<
    }
  }
  echo "</table>\n";
  
  echo "<p>Legend: ";
  echo "<span class=\"" . get_color(0.5) . "\">[ &chi;<sup>2</sup> &lt; 1 ]</span> / ";
  echo "<span class=\"" . get_color(2) . "\"> [ 1 &le; &chi;<sup>2</sup> &lt; 4 ]</span> / ";
  echo "<span class=\"" . get_color(5) . "\"> [ 4 &le; &chi;<sup>2</sup> ]</span><br>\n";
?>
