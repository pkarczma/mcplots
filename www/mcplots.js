// following two function are intended to show/hide elements on index.php pages
// inspared by http://www.codeisart.ru/js-core-fast-dom-element-traversing/
function toggle(item)
{
  while (item = item.nextSibling)
    if (item.className == "submenu-items" || item.className == "submenu-items-hidden")
      break;

  if (item.className == "submenu-items")
    item.className = "submenu-items-hidden";
  else
    item.className = "submenu-items";
}

function togglespan(item)
{
  var mainspan = item;
  
  while (item = item.nextSibling)
    if (item.className == "caption-items" || item.className == "caption-items-hidden")
      break;
  
  if (item.className == "caption-items") {
    mainspan.innerHTML = "show details &rarr;";
    item.className = "caption-items-hidden";
  }
  else {
    mainspan.innerHTML = "hide details &larr;";
    item.className = "caption-items";
  }
}


// navigate to a new URL
function navigate(url)
{
  window.location.href = url;
}

// Remove an element and provide a function that inserts it into its original position
// https://developers.google.com/speed/articles/javascript-dom
// section "Out-of-the-flow DOM Manipulation"
function removeToInsertLater(element) {
  var parentNode = element.parentNode;
  var nextSibling = element.nextSibling;
  parentNode.removeChild(element);
  return function() {
    if (nextSibling) {
      parentNode.insertBefore(element, nextSibling);
    } else {
      parentNode.appendChild(element);
    }
  };
}


// for production.php
function applyFilter()
{
  var term = document.getElementById("filterEdit").value;
  var invert = document.getElementById("invertFlag").checked;
  
  var runs = document.getElementById("runs");
  var insertFunction = removeToInsertLater(runs);
  var rows = runs.getElementsByTagName('tr');
  
  var matches = 0;
  var len = rows.length;
  
  for (var i = 1; i < len; i++) {
    var row = rows[i];
    var run = row.children[0].textContent;
    var match = (run.indexOf(term) !== -1);
    
    if (match != invert) { // logical XOR
      // TODO: highline 'term'
      row.style.display = "table-row";
      matches++;
    }
    else {
      row.style.display = "none";
    }
  }
  
  insertFunction();
  
  var state = document.getElementById("filterState");
  state.textContent = (term !== "") ? "Keyword: " + term + " (matched " + matches + " of " + (len-1) + " rows)" : "";
}
