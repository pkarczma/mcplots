<html>

<head>
  <title>MC Production</title>
  <link rel="shortcut icon" href="img/logo.png" />
  <link rel="stylesheet" type="text/css" href="style.css"/>
  <script async src="mcplots.js"></script>
</head>

<body>

<h1>MC Production</h1>

<?php
  // start profiler
  include "profiler.php";
  $t = new Profiler();
  
  function accept_commands() {
    // TODO: fix case when all elements in column unselected by user
    
    $up = array();
    $up[] = "LOCK TABLES production WRITE";
    
    foreach (array_keys($_POST) as $key) {
      $val = $_POST[$key];
      
      switch ($key) {
        case "lxbatch":
        case "boinc":
          $up[] = "UPDATE production SET $key=0";
          foreach ($val as $i)
            if (is_numeric($i))
              $up[] = "UPDATE production SET $key=1 WHERE revision=$i";
          break;
      }
    }
    
    $up[] = "UNLOCK TABLES";
    foreach ($up as $i) mysql_query($i);
  }
  
  // this function runs $query and save result to file $fname
  function dump_query($query, $fname) {
    $query = mysql_query($query);       // do SQL query
    $ncols = mysql_num_fields($query);  // get number of fields in result
    
    // prepare list of fields names
    $names = array();
    for ($i = 0; $i < $ncols; $i++)
      $names[] = mysql_field_name($query, $i);
    
    // prepare header from fields names
    $dump = "#" . implode(" \t", $names) . "\n";
    
    // get content of query
    while ($row = mysql_fetch_row($query))
      $dump .= implode("\t", $row) . "\n";
    
    // save result
    file_put_contents($fname, $dump);
  }
  
  function calc_stats() {
      // various statistics on jobs
      $stat = array();
      
      foreach (array(0, 1, 7, 30, 365) as $i) {
        // get total number of submitted and received (good and bad) jobs over last $i days
        $query = "SELECT
                    SUM(IF(param = 'submit-to-boinc', value, 0)) AS sub,
                    SUM(IF(param = 'good-jobs', value, 0)) AS succ,
                    SUM(IF(param = 'bad-jobs', value, 0)) AS fail
                  FROM
                    telemetry
                  WHERE
                    date > (NOW() - interval $i day)";
        
        $query = mysql_query($query);
        $stat[$i] = mysql_fetch_assoc($query);
        
        // get jobs queue total length $i days ago
        $query = "SELECT value AS len FROM telemetry WHERE param='copilot-jobs-queue-total' AND date < (NOW() - interval $i day) ORDER BY id DESC LIMIT 1";
        $query = mysql_query($query);
        $len = mysql_fetch_assoc($query);
        $stat[$i]["len"] = $len["len"];
      }
      
      $query = mysql_query("SELECT SUM(events) AS events, SUM(attempts) AS sub, SUM(success) AS succ, SUM(failure) AS fail FROM runs");
      $stat["x"] = mysql_fetch_assoc($query);
      $stat["x"]["len"] = 0;
      
      foreach (array(1, 7, 30, 365, "x") as $i) {
        $stat[$i]["rcv"] = $stat[$i]["succ"] + $stat[$i]["fail"];
        $stat[$i]["drain"] = $stat[$i]["sub"] + $stat[$i]["len"] - $stat[0]["len"];
        $stat[$i]["lost"] = $stat[$i]["drain"] - $stat[$i]["rcv"];
      }
      
      $stat["name"]["sub"]   = "Submitted:";
      $stat["name"]["drain"] = "Queue drain:";
      $stat["name"]["rcv"]   = "Received:";
      $stat["name"]["succ"]  = "Succeeded:";
      $stat["name"]["fail"]  = "Failed:";
      $stat["name"]["lost"]  = "Lost:";
      
      return $stat;
  }
  
  function calc_jobsratios_data($frates, $fdrain, $fname) {
    $data = array();
    
    // read jobs rates data
    foreach (file($frates) as $line) {
      // skip header line
      if ($line[0] == "#") continue;
      
      $line = str_replace("\n", "", $line);
      $vals = explode("\t", $line);
      $data[$vals[0]][] = $vals[1];
      $data[$vals[0]][] = $vals[2];
      $data[$vals[0]][] = $vals[3];
    }
    
    // read queue drain data
    foreach (file($fdrain) as $line) {
      // skip header line
      if ($line[0] == "#") continue;
      
      $line = str_replace("\n", "", $line);
      $vals = explode("\t", $line);
      $data[$vals[0]][] = $vals[1];
    }
    
    // output data
    $dump = "#" . implode(" \t", array("date", "n_good", "n_bad", "total", "drain")) . "\n";
    
    foreach ($data as $k => $v) {
      // skip incomplete entries (no drain data)
      if (count($v) < 4) continue;
      
      $dump .= $k . "\t" . implode("\t", $v) . "\n";
    }
    
    file_put_contents($fname, $dump);
  }
  
  function calc_plots_data() {
      // jobs rates
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d.%H') AS date_h,
                  SUM(IF(param = 'good-jobs', value, 0)) AS n_good,
                  SUM(IF(param = 'bad-jobs', value, 0)) AS n_bad,
                  SUM(value) AS total,
                  SUM(IF(param = 'dup-jobs', value, 0)) AS n_dup,
                  SUM(IF(param = 'corrupt-jobs', value, 0)) AS n_corrupt
                FROM
                  telemetry
                WHERE
                  param IN ('good-jobs', 'bad-jobs', 'dup-jobs', 'corrupt-jobs') AND
                  date > DATE_FORMAT(now() - INTERVAL 4 DAY, '%Y-%m-%d %H:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-jobs-hourly.txt");
      
      // daily
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d') AS date_d,
                  SUM(IF(param = 'good-jobs', value, 0)) AS n_good,
                  SUM(IF(param = 'bad-jobs', value, 0)) AS n_bad,
                  SUM(value) AS total
                FROM
                  telemetry
                WHERE
                  param IN ('good-jobs', 'bad-jobs') AND
                  date > DATE_FORMAT(now() - INTERVAL 6 WEEK, '%Y-%m-%d 00:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-jobs-daily.txt");
      
      // weekly
      $query = "SELECT
                  DATE_FORMAT(DATE_ADD(date, INTERVAL (6 - WEEKDAY(date)) DAY), '%Y-%m-%d') AS date_d,
                  SUM(IF(param = 'good-jobs', value, 0)) AS n_good,
                  SUM(IF(param = 'bad-jobs', value, 0)) AS n_bad,
                  SUM(value) AS total
                FROM
                  telemetry
                WHERE
                  param IN ('good-jobs', 'bad-jobs') AND
                  date > DATE_FORMAT(now() - INTERVAL 1 YEAR, '%Y-%m-%d 00:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-jobs-weekly.txt");
      
      // monthly
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-01') AS date_d,
                  SUM(IF(param = 'good-jobs', value, 0)) AS n_good,
                  SUM(IF(param = 'bad-jobs', value, 0)) AS n_bad,
                  SUM(value) AS total
                FROM
                  telemetry
                WHERE
                  param IN ('good-jobs', 'bad-jobs')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-jobs-monthly.txt");
      
      // total queue drain over last 4 days / hourly
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d.%H') AS date,
                  SUM(drain) AS drain
                FROM (
                  SELECT
                    date,
                    len,
                    nsub,
                    nsubtot,
                    @prevlentot - lentot AS drain,
                    @prevlentot AS prevlentot,
                    @prevlentot:=lentot AS lentot
                  FROM
                    (SELECT @prevlentot:=0) tmp1,
                    (SELECT
                      date,
                      @len:=IF(param = 'copilot-jobs-queue-total', value, 0) AS len,
                      @nsub:=IF(param = 'submit-to-boinc', value, 0) AS nsub,
                      @nsubtot:=@nsubtot + @nsub AS nsubtot,
                      @len - @nsubtot AS lentot
                     FROM
                      (SELECT @nsubtot:=0) tmp2,
                      telemetry
                     WHERE
                      param IN ('copilot-jobs-queue-total', 'submit-to-boinc') AND
                      date > (now() - INTERVAL 4 DAY)
                    ) tmp3
                  WHERE
                    nsub = 0
                ) tmp4
                GROUP BY 1
                LIMIT 1, 1000";
      dump_query($query, "cache/stats/stats-jobsdrain-hourly.txt");
      
      // queue drain daily
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d') AS date,
                  SUM(drain) AS drain
                FROM (
                  SELECT
                    date,
                    len,
                    nsub,
                    nsubtot,
                    @prevlentot - lentot AS drain,
                    @prevlentot AS prevlentot,
                    @prevlentot:=lentot AS lentot
                  FROM
                    (SELECT @prevlentot:=0) tmp1,
                    (SELECT
                      date,
                      @len:=IF(param = 'copilot-jobs-queue-total', value, 0) AS len,
                      @nsub:=IF(param = 'submit-to-boinc', value, 0) AS nsub,
                      @nsubtot:=@nsubtot + @nsub AS nsubtot,
                      @len - @nsubtot AS lentot
                     FROM
                      (SELECT @nsubtot:=0) tmp2,
                      telemetry
                     WHERE
                      param IN ('copilot-jobs-queue-total', 'submit-to-boinc') AND
                      date > (now() - INTERVAL 6 WEEK)
                    ) tmp3
                  WHERE
                    nsub = 0
                ) tmp4
                GROUP BY 1
                LIMIT 1, 1000000";
      dump_query($query, "cache/stats/stats-jobsdrain-daily.txt");
      
      // queue drain weekly
      $query = "SELECT
                  DATE_FORMAT(DATE_ADD(date, INTERVAL (6 - WEEKDAY(date)) DAY), '%Y-%m-%d') AS date,
                  SUM(drain) AS drain
                FROM (
                  SELECT
                    date,
                    len,
                    nsub,
                    nsubtot,
                    @prevlentot - lentot AS drain,
                    @prevlentot AS prevlentot,
                    @prevlentot:=lentot AS lentot
                  FROM
                    (SELECT @prevlentot:=0) tmp1,
                    (SELECT
                      date,
                      @len:=IF(param = 'copilot-jobs-queue-total', value, 0) AS len,
                      @nsub:=IF(param = 'submit-to-boinc', value, 0) AS nsub,
                      @nsubtot:=@nsubtot + @nsub AS nsubtot,
                      @len - @nsubtot AS lentot
                     FROM
                      (SELECT @nsubtot:=0) tmp2,
                      telemetry
                     WHERE
                      param IN ('copilot-jobs-queue-total', 'submit-to-boinc') AND
                      date > (now() - INTERVAL 1 YEAR)
                    ) tmp3
                  WHERE
                    nsub = 0
                ) tmp4
                GROUP BY 1
                LIMIT 1, 1000000";
      dump_query($query, "cache/stats/stats-jobsdrain-weekly.txt");
      
      // queue drain monthly
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-01') AS date,
                  SUM(drain) AS drain
                FROM (
                  SELECT
                    date,
                    len,
                    nsub,
                    nsubtot,
                    @prevlentot - lentot AS drain,
                    @prevlentot AS prevlentot,
                    @prevlentot:=lentot AS lentot
                  FROM
                    (SELECT @prevlentot:=0) tmp1,
                    (SELECT
                      date,
                      @len:=IF(param = 'copilot-jobs-queue-total', value, 0) AS len,
                      @nsub:=IF(param = 'submit-to-boinc', value, 0) AS nsub,
                      @nsubtot:=@nsubtot + @nsub AS nsubtot,
                      @len - @nsubtot AS lentot
                     FROM
                      (SELECT @nsubtot:=0) tmp2,
                      telemetry
                     WHERE
                      param IN ('copilot-jobs-queue-total', 'submit-to-boinc')
                    ) tmp3
                  WHERE
                    nsub = 0
                ) tmp4
                GROUP BY 1
                LIMIT 1, 10000000";
      dump_query($query, "cache/stats/stats-jobsdrain-monthly.txt");
      
      // jobs rates ratios
      foreach (array("hourly", "daily", "weekly", "monthly") as $i) {
        calc_jobsratios_data("cache/stats/stats-jobs-$i.txt", "cache/stats/stats-jobsdrain-$i.txt", "cache/stats/stats-jobsratios-$i.txt");
      }
      
      // contributed CPU time
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d.%H') AS date_h,
                  SUM(IF(param = 'good-cpuusage', value, 0)) AS cpu_good,
                  SUM(IF(param = 'bad-cpuusage', value, 0)) AS cpu_bad,
                  SUM(value) AS total
                FROM
                  telemetry
                WHERE
                  param IN ('good-cpuusage', 'bad-cpuusage') AND
                  date > DATE_FORMAT(now() - INTERVAL 4 DAY, '%Y-%m-%d %H:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-cpu-hourly.txt");
      
      // daily
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d') AS date_d,
                  SUM(IF(param = 'good-cpuusage', value, 0)) AS cpu_good,
                  SUM(IF(param = 'bad-cpuusage', value, 0)) AS cpu_bad,
                  SUM(value) AS total
                FROM
                  telemetry
                WHERE
                  param IN ('good-cpuusage', 'bad-cpuusage') AND
                  date > DATE_FORMAT(now() - INTERVAL 6 WEEK, '%Y-%m-%d 00:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-cpu-daily.txt");
      
      // weekly
      $query = "SELECT
                  DATE_FORMAT(DATE_ADD(date, INTERVAL (6 - WEEKDAY(date)) DAY), '%Y-%m-%d') AS date_d,
                  SUM(IF(param = 'good-cpuusage', value, 0)) AS cpu_good,
                  SUM(IF(param = 'bad-cpuusage', value, 0)) AS cpu_bad,
                  SUM(value) AS total
                FROM
                  telemetry
                WHERE
                  param IN ('good-cpuusage', 'bad-cpuusage') AND
                  date > DATE_FORMAT(now() - INTERVAL 1 YEAR, '%Y-%m-%d 00:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-cpu-weekly.txt");
      
      // monthly
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-01') AS date_d,
                  SUM(IF(param = 'good-cpuusage', value, 0)) AS cpu_good,
                  SUM(IF(param = 'bad-cpuusage', value, 0)) AS cpu_bad,
                  SUM(value) AS total
                FROM
                  telemetry
                WHERE
                  param IN ('good-cpuusage', 'bad-cpuusage')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-cpu-monthly.txt");
      
      // copilot total input queue size (internal + public)
      $query = "SELECT
                  date,
                  value AS queue_size
                FROM
                  telemetry
                WHERE
                  param = 'copilot-jobs-queue-total' AND
                  date > (now() - INTERVAL 4 DAY)";
      dump_query($query, "cache/stats/stats-queue-in-total.txt");
      
      // copilot input queue size (public)
      $query = "SELECT
                  date,
                  value AS queue_size
                FROM
                  telemetry
                WHERE
                  param = 'copilot-jobs-queue-len' AND
                  date > (now() - INTERVAL 4 DAY)";
      dump_query($query, "cache/stats/stats-queue-in.txt");
      
      // copilot output queue size
      $query = "SELECT
                  date,
                  value AS queue_size
                FROM
                  telemetry
                WHERE
                  param = 'copilot-done-queue-len' AND
                  date > (now() - INTERVAL 4 DAY)";
      dump_query($query, "cache/stats/stats-queue-out.txt");
      
      // number of machines connected to copilot host
      $query = "SELECT
                  date,
                  value AS machines
                FROM
                  telemetry
                WHERE
                  param = 'copilot-connected-machines' AND
                  date > (now() - INTERVAL 4 DAY)";
      dump_query($query, "cache/stats/stats-machines-hourly.txt");
      
      // daily
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d') AS date_d,
                  AVG(value) AS machines
                FROM
                  telemetry
                WHERE
                  param = 'copilot-connected-machines' AND
                  date > DATE_FORMAT(now() - INTERVAL 6 WEEK, '%Y-%m-%d 00:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-machines-daily.txt");
      
      // weekly
      $query = "SELECT
                  DATE_FORMAT(DATE_ADD(date, INTERVAL (6 - WEEKDAY(date)) DAY), '%Y-%m-%d') AS date_d,
                  AVG(value) AS machines
                FROM
                  telemetry
                WHERE
                  param = 'copilot-connected-machines' AND
                  date > DATE_FORMAT(now() - INTERVAL 1 YEAR, '%Y-%m-%d 00:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-machines-weekly.txt");
      
      // monthly
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-01') AS date_d,
                  AVG(value) AS machines
                FROM
                  telemetry
                WHERE
                  param = 'copilot-connected-machines'
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-machines-monthly.txt");
      
      // mcplots jobs processing busy time
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d.%H') AS date_h,
                  SUM(IF(param = 'busy-time', value, 0)) AS busytime_cpu,
                  SUM(IF(param = 'busy-time-wall', value, 0)) AS busytime_wall
                FROM
                  telemetry
                WHERE
                  param IN ('busy-time', 'busy-time-wall') AND
                  date > DATE_FORMAT(now() - INTERVAL 4 DAY, '%Y-%m-%d %H:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-busytime-hourly.txt");
      
      // daily
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-%d') AS date_d,
                  SUM(IF(param = 'busy-time', value, 0)) AS busytime_cpu,
                  SUM(IF(param = 'busy-time-wall', value, 0)) AS busytime_wall
                FROM
                  telemetry
                WHERE
                  param IN ('busy-time', 'busy-time-wall') AND
                  date > DATE_FORMAT(now() - INTERVAL 6 WEEK, '%Y-%m-%d 00:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-busytime-daily.txt");
      
      // weekly
      $query = "SELECT
                  DATE_FORMAT(DATE_ADD(date, INTERVAL (6 - WEEKDAY(date)) DAY), '%Y-%m-%d') AS date_d,
                  SUM(IF(param = 'busy-time', value, 0)) AS busytime_cpu,
                  SUM(IF(param = 'busy-time-wall', value, 0)) AS busytime_wall
                FROM
                  telemetry
                WHERE
                  param IN ('busy-time', 'busy-time-wall') AND
                  date > DATE_FORMAT(now() - INTERVAL 1 YEAR, '%Y-%m-%d 00:00:00')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-busytime-weekly.txt");
      
      // monthly
      $query = "SELECT
                  DATE_FORMAT(date, '%Y-%m-01') AS date_d,
                  SUM(IF(param = 'busy-time', value, 0)) AS busytime_cpu,
                  SUM(IF(param = 'busy-time-wall', value, 0)) AS busytime_wall
                FROM
                  telemetry
                WHERE
                  param IN ('busy-time', 'busy-time-wall')
                GROUP BY 1";
      dump_query($query, "cache/stats/stats-busytime-monthly.txt");
      
      // run gnuplot to make images:
      exec("gnuplot stats.gnuplot");
  }
  
  function display_status() {
    // current state:
    echo "<p>\n";
    $query = mysql_query("SELECT date, param FROM telemetry ORDER BY id DESC LIMIT 1");
    $row = mysql_fetch_assoc($query);
    $lastUpdate = $row["date"];
    $lastRecord = $row["param"];
    
    // map between telemetry record name and state of production machinery:
    $stateMap = array(
      "runs-list-added" => "submitting jobs",
      "copilot-jobs-queue-len" => "submitting jobs",
      "copilot-jobs-queue-total" => "submitting jobs",
      "submit-to-boinc" => "submitting jobs",
      "copilot-connected-machines" => "receiving jobs",
      "copilot-done-queue-len" => "receiving jobs",
      "good-jobs" => "receiving jobs",
      "bad-jobs" => "receiving jobs",
      "good-cpuusage" => "receiving jobs",
      "bad-cpuusage" => "receiving jobs",
      "corrupt-jobs" => "receiving jobs",
      "dup-jobs" => "receiving jobs",
      "busy-time" => "idle",
      "busy-time-wall" => "idle",
    );
    
    $state = $stateMap[$lastRecord];
    
    echo "Last update: " . $lastUpdate . " (" . floor((time() - strtotime($lastUpdate)) / 60) . " minutes ago)<br>\n";
    echo "Current state: " . $state . "<br>\n";
    echo "</p>\n";
    echo "\n";
    
    // re-calc statistics and re-draw plots only if data is expired
    if (!file_exists("cache/stats/stats.bin") ||
      (filemtime("cache/stats/stats.bin") < strtotime($lastUpdate) && ($state == "idle"))) {
      exec("mkdir -p cache/stats");
      file_put_contents("cache/stats/stats.bin", serialize(calc_stats()));
      calc_plots_data();
    }
    
    // read cached statistics data:
    $stat = unserialize(file_get_contents("cache/stats/stats.bin"));
    
    echo "<p>\n";
    echo "<b>Jobs statistics</b><br>\n";
    
    echo "<table>\n";
    echo "<tr>";
    echo "  <th></th>";
    echo "  <th>Last day</th>";
    echo "  <th>Last week</th>";
    echo "  <th>Last month</th>";
    echo "  <th>Last year</th>";
    echo "  <th>Total</th>";
    echo "</tr>\n";
    
    foreach (array("sub", "drain", "rcv", "succ", "fail", "lost") as $i) {
      echo "<tr>";
      foreach (array("name", 1, 7, 30, 365, "x") as $j)
        echo "  <td>" . $stat[$j][$i] . "</td>";
      echo "</tr>\n";
    }
    
    echo "</table>\n";
    
    echo "<br>\n";
    
    echo "Total number of generated events: " . round($stat["x"]["events"] / 1e9, 1) . " <i>billions</i><br>\n";
    echo "</p>\n";
    echo "\n";
    
    
    // plots
    $plots = $_GET["plots"];
    if (! in_array($plots, array("hourly", "daily", "weekly", "monthly")))
      $plots = "hourly";
    
    echo "<p id=\"plots\">\n";
    echo "<b>Performance plots</b>\n";
    echo "<a href=\"?view=status&plots=hourly#plots\" "  . (($plots == "hourly")  ? "class=\"selected\"" : "") . ">hourly</a>\n";
    echo "<a href=\"?view=status&plots=daily#plots\" "  . (($plots == "daily")  ? "class=\"selected\"" : "") . ">daily</a>\n";
    echo "<a href=\"?view=status&plots=weekly#plots\" "  . (($plots == "weekly")  ? "class=\"selected\"" : "") . ">weekly</a>\n";
    echo "<a href=\"?view=status&plots=monthly#plots\" "  . (($plots == "monthly")  ? "class=\"selected\"" : "") . ">monthly</a>\n";
    echo "<br>\n";
    echo "</p>\n";
    echo "\n";
    
    echo "<p>\n";
    echo "<b>Jobs rates</b><br>\n";
    echo "<a href=\"cache/stats/stats-jobs-$plots.txt\"><img src=\"cache/stats/stats-jobs-$plots.png\"></a><br>\n";
    echo "<a href=\"cache/stats/stats-jobsratios-$plots.txt\"><img src=\"cache/stats/stats-jobsratios-$plots.png\"></a><br>\n";
    echo "</p>\n";
    echo "\n";
    
    echo "<p>\n";
    echo "<b>Contributed CPU time</b><br>\n";
    echo "CPU time contributed by volunteers for jobs processing<br>\n";
    echo "<a href=\"cache/stats/stats-cpu-$plots.txt\"><img src=\"cache/stats/stats-cpu-$plots.png\"></a><br>\n";
    echo "</p>\n";
    echo "\n";
    
    // hide plots as Copilot infrastructure are phased-out (last job received on April 2017)
    /*
    echo "<p>\n";
    echo "<b>Connected machines</b><br>\n";
    echo "Number of volunteers machines connected to copilot host<br>\n";
    echo "<a href=\"cache/stats/stats-machines-$plots.txt\"><img src=\"cache/stats/stats-machines-$plots.png\"></a><br>\n";
    echo "</p>\n";
    echo "\n";
    */
    
    if ($plots == "hourly") {
      echo "<p>\n";
      echo "<b>Jobs queues</b><br>\n";
      echo "Input (";
      echo "<a href=\"cache/stats/stats-queue-in-total.txt\">total</a> and ";
      echo "<a href=\"cache/stats/stats-queue-in.txt\">public</a>) and ";
      echo "<a href=\"cache/stats/stats-queue-out.txt\">output</a> queues sizes<br>\n";
      echo "<img src=\"cache/stats/stats-queues.png\">\n";
      echo "</p>\n";
      echo "\n";
    }
    
    echo "<p>\n";
    echo "<b>Busy time</b><br>\n";
    echo "Amount of time MCPLOTS spend for jobs processing<br>\n";
    echo "<a href=\"cache/stats/stats-busytime-$plots.txt\"><img src=\"cache/stats/stats-busytime-$plots.png\"></a><br>\n";
    echo "</p>\n";
    echo "\n";
    
    echo "<p>\n";
    echo "<b>Jobs flow</b><br>\n";
    echo "<img src=\"img/jobs-flow.png\">\n";
    echo "</p>\n";
    echo "\n";
  }
  
  function display_control() {
    // this query selects all revisions which match any of:
    // 1) belongs to latest 3 revisions
    // 2) marked for production or publication
    // 3) have non-empty 'runs' field
    
    $query = 
     "SELECT *
      FROM production
      LEFT JOIN (
        SELECT
          revision AS revision_runs,
          COUNT(run) AS num_runs,
          SUM(events) AS sum_events,
          SUM(success > 0) AS num_success
        FROM runs
        GROUP BY revision
      ) AS runs_summary ON revision = revision_runs
      LEFT JOIN (
        SELECT
          max(date) AS date_tel,
          revision AS revision_tel
        FROM telemetry
        GROUP BY revision
      ) AS tel_summary ON revision = revision_tel
      WHERE
        revision > (
          SELECT revision
          FROM production
          ORDER BY revision DESC
          LIMIT 3, 1
        ) OR
        boinc != 0 OR
        lxbatch != 0 OR
        num_runs > 0
      ORDER BY revision DESC";
    
    $query = mysql_query($query);
    
    echo "<form method=\"post\" action=\"?view=control\">\n";
    echo "<table id=\"production\">\n";
    echo "<tr>" .
         "<th>revision</th> " .
  //       "<th>lxbatch</th> " .
         "<th>boinc</th> " .
         "<th>events, <i>bln.</i></th> " .
         "<th>coverage</th> " .
  //       "<th>seed</th> " .
  //       "<th>added</th> " .
         "<th>updated</th>" .
         "</tr>\n";
    
    $can_control = check_access();
    
    while ($row = mysql_fetch_assoc($query)) {
      $rev = $row["revision"];
      $revlink = "http://svnweb.cern.ch/world/wsvn/mcplots/trunk/scripts/mcprod/?op=log&isdir=1&rev=" . $rev;
      $lxbatch = ($row["lxbatch"] > 0) ? "checked" : "";
      $input_state = $can_control ? "" : "disabled";
      $boinc = ($row["boinc"] > 0) ? "checked" : "";
      $updated = ($row["date_tel"] > $row["added"]) ? $row["date_tel"] : $row["added"];
      $age = (time() - strtotime($updated)) / 86400;
      $agec = ($age < 1) ? "day" : (($age < 7) ? "week" : "old");
      
      // do not display old entries (3 months) without events:
      if (($row["boinc"] == 0) && ($row["sum_events"] == 0) && ($age > 90)) continue;
      
      echo "<tr>" .
           "<td> <a href=\"" . $revlink . "\">" . $rev . "</a></td> " .
  //         "<td> <input disabled type=checkbox name=lxbatch[] value=$rev $lxbatch></td> " .
           "<td> <input $input_state type=checkbox name=boinc[] value=$rev $boinc></td> " .
           "<td>" . (($row["num_runs"] != "" ) ? round($row["sum_events"] / 1e9, 1) : "" ) . "</td> " .
           "<td>" . (($row["num_runs"] != "" ) ? ("<a href=\"?view=revision&rev=$rev\" rel=\"nofollow\">" . $row["num_success"] . " / " . $row["num_runs"] . "</a>") : "") . "</td> " .
  //         "<td>" . $row["seed"] . "</td> " .
  //         "<td>" . $row["added"] . "</td> " .
           "<td class=\"$agec\">" . $updated . "</td>" .
           "</tr>\n";
    }
    
    echo "</table>\n";
    
    if ($can_control)
      echo "<input type=submit value=Apply>\n";
    
    echo "</form>\n";
  }
  
  // print table of runs which match $display criterion
  function display_runs($rev) {
    $display = $_GET["display"];
    $options = array("all", "succ", "unsucc", "masked");
    
    // sanity check
    if (!in_array($display, $options)) $display = "all";
    
    // print menu
    echo "<p>\n";
    echo "Filter: ";
    foreach ($options as $opt) {
      echo "<a href=\"?view=runs&rev=$rev&display=$opt\" " . ($display == $opt ? "class=\"selected\"" : "") . ">$opt</a>\n";
    }
    echo "</p>\n";
    echo "\n";
    
    echo "<p>\n";
    echo "<input id=\"filterEdit\" type=\"search\" placeholder=\"Run search\" value=\"\" onchange=\"applyFilter()\">\n";
    echo "<label><input id=\"invertFlag\" type=\"checkbox\" onchange=\"applyFilter()\"><span>Invert match</span></label>\n";
    echo "<span id=\"filterState\"></span>\n";
    echo "</p>\n";
    
    $filter = "";
    switch ($display) {
      case   "succ": $filter = " AND attempts > 0 AND success > 0"; break;
      case "unsucc": $filter = " AND attempts > 0 AND success = 0"; break;
      case "masked": $filter = " AND attempts = 0"; break;
    }
    
    $query = mysql_query("SELECT * FROM runs WHERE revision = $rev" . $filter);
    
    echo "<table id=\"runs\">\n";
    echo "<tr>" .
         "<th>run</th> " .
         "<th>events</th> " .
         "<th>attempts</th> " .
         "<th>success</th> " .
         "<th>failure</th> " .
         "<th>lost</th> " .
         "</tr>\n";
    
    while ($row = mysql_fetch_assoc($query)) {
      $nlost = $row["attempts"] - ($row["success"] + $row["failure"]);
      echo "<tr>" .
           "<td>" . $row["run"] . "</td> " .
           "<td>" . $row["events"] . "</td> " .
           "<td>" . $row["attempts"] . "</td> " .
           "<td>" . $row["success"] . "</td> " .
           "<td>" . $row["failure"] . "</td>" .
           "<td>" . $nlost . "</td>" .
           "</tr>\n";
    }
    
    echo "</table>\n";
  }
  
  function display_revision($rev) {
    // runs/jobs/events summary
    $query = "SELECT
                COUNT(*) AS nRuns,
                SUM(attempts > 0 AND success > 0) AS nRunsSucc,
                SUM(attempts > 0 AND success = 0) AS nUnsucc,
                SUM(attempts = 0) AS nMasked,
                SUM(events) AS nEvt,
                SUM(attempts) AS nAtt,
                SUM(success) AS nSucc,
                SUM(failure) AS nFail
              FROM
                runs
              WHERE
                revision = $rev";
    $query = mysql_query($query);
    $row = mysql_fetch_assoc($query);
    
    $nRcv = $row["nSucc"] + $row["nFail"]; // received jobs
    $nLost = $row["nAtt"] - $nRcv;         // lost jobs
    
    echo "<p>\n";
    echo "<b>Runs summary</b><br>\n";
    echo "<a href=\"?view=runs&rev=$rev&display=all\">Total</a>: " . $row["nRuns"] . " runs<br>\n";
    echo "<a href=\"?view=runs&rev=$rev&display=succ\">Successful</a>: " . $row["nRunsSucc"] . "<br>\n";
    echo "<a href=\"?view=runs&rev=$rev&display=unsucc\">Unsuccessful</a>: " . $row["nUnsucc"] . "<br>\n";
    echo "<a href=\"?view=runs&rev=$rev&display=masked\">Masked</a>: " . $row["nMasked"] . "<br>\n";
    echo "</p>\n";
    echo "\n";
    
    echo "<p>\n";
    echo "<b>Jobs summary</b><br>\n";
    echo "Submitted: " . $row["nAtt"] . " jobs<br>\n";
    echo "Succeeded: " . $row["nSucc"] . "<br>\n";
    echo "Failed: " . $row["nFail"] . "<br>\n";
    echo "Lost: " . $nLost . "<br>\n";
    echo "</p>\n";
    echo "\n";
    
    echo "<p>\n";
    echo "<b>Events summary</b><br>\n";
    echo "Total: " . number_format($row["nEvt"], 0, '.', ' ') . " events<br>\n";
    echo "</p>\n";
    echo "\n";
    
    // average CPU and disk per job
    $query = "SELECT
                AVG(cpuusage) AS cpu,
                AVG(diskusage) AS disk
              FROM
                jobs
              WHERE
                revision = $rev AND
                exitcode = 0";
    $query = mysql_query($query);
    $avg = mysql_fetch_assoc($query);
    
    // no resource usage information is available
    if ($avg["cpu"] == "") return;
    
    // jobs resources usage: CPU and disk
    $query = "SELECT
                5*(cpuusage DIV 300) AS runtime,
                count(*) AS njobs
              FROM
                jobs
              WHERE
                revision = $rev AND
                exitcode = 0
              GROUP BY 1";
    dump_query($query, "cache/stats/runtime.txt");
    
    $query = "SELECT
                diskusage DIV 1024 AS disk,
                count(*) AS njobs
              FROM
                jobs
              WHERE
                revision = $rev AND
                exitcode = 0
              GROUP BY 1";
    dump_query($query, "cache/stats/diskusage.txt");
    
    // run gnuplot to make images:
    exec("gnuplot resources.gnuplot");
    exec("mv cache/stats/runtime.txt cache/stats/runtime-$rev.txt");
    exec("mv cache/stats/runtime.png cache/stats/runtime-$rev.png");
    exec("mv cache/stats/diskusage.txt cache/stats/diskusage-$rev.txt");
    exec("mv cache/stats/diskusage.png cache/stats/diskusage-$rev.png");
    
    echo "<p>\n";
    echo "<b>Jobs run time</b><br>\n";
    echo "Average: " . round($avg["cpu"] / 60, 1) . " <i>minutes</i><br>\n";
    echo "Run time distribution<br>\n";
    echo "<a href=\"cache/stats/runtime-$rev.txt\"><img src=\"cache/stats/runtime-$rev.png\"></a><br>\n";
    echo "</p>\n";
    echo "\n";
    
    echo "<p>\n";
    echo "<b>Jobs disk usage</b><br>\n";
    echo "Average: " . round($avg["disk"] / 1024, 2) . " <i>megabytes</i><br>\n";
    echo "Disk usage distribution<br>\n";
    echo "<a href=\"cache/stats/diskusage-$rev.txt\"><img src=\"cache/stats/diskusage-$rev.png\"></a><br>\n";
    echo "</p>\n";
    echo "\n";
  }
  
  function display_hosts() {
    $system = $_GET["system"];
    $display = $_GET["display"];
    
    // sanitize input
    if (!is_numeric($system)) $system = "";
    if (!in_array($display, array("all", "active", "problematic"))) $display = "active";
    
    $filterSystem = ($system == "") ? "TRUE" : "system = $system";
    $dispAll = ($display == "all");
    $dispActive = ($display == "active");
    $dispProblem = ($display == "problematic");
    
    // print menu
    echo "<p>\n";
    echo "System: ";
    echo "<a href=\"?view=hosts&system=&display=$display\" " . (($system == "") ? "class=\"selected\"" : "") . ">all</a>\n";
    echo "<a href=\"?view=hosts&system=1&display=$display\" " . (($system == 1) ? "class=\"selected\"" : "") . ">vLHC</a>\n";
    echo "<a href=\"?view=hosts&system=2&display=$display\" " . (($system == 2) ? "class=\"selected\"" : "") . ">vLHCdev</a>\n";
    echo "<a href=\"?view=hosts&system=3&display=$display\" " . (($system == 3) ? "class=\"selected\"" : "") . ">LHC</a>\n";
    echo "</p>\n";
    echo "\n";
    
    echo "<p>\n";
    echo "Filter: ";
    echo "<a href=\"?view=hosts&system=$system&display=all\" " . ($dispAll ? "class=\"selected\"" : "") . ">all</a>\n";
    echo "<a href=\"?view=hosts&system=$system&display=active\" " . ($dispActive ? "class=\"selected\"" : "") . ">active</a>\n";
    echo "<a href=\"?view=hosts&system=$system&display=problematic\" " . ($dispProblem ? "class=\"selected\"" : "") . ">problematic</a>\n";
    echo "</p>\n";
    echo "\n";
    
    $query = "SELECT
                system,
                userid,
                hostid,
                cpu_time AS cpu,
                n_good_jobs AS ngood,
                (n_jobs - n_good_jobs) AS nbad,
                n_jobs_1m,
                n_good_jobs_1m,
                n_events AS nevents
              FROM
                api
              WHERE
                $filterSystem";
    $query = mysql_query($query);
    
    echo "<table id=\"contributions\">\n";
    echo "<tr>" .
         "<th>system</th> " .
         "<th>userid</th> " .
         "<th>hostid</th> " .
         "<th>CPU time, <i>s</i></th> " .
         "<th>Good jobs</th> " .
         "<th>Failed jobs</th> " .
         "<th>Recent fail rate, <i>%</i></th>" .
         "<th>Total events</th> " .
         "</tr>\n";
    
    $user0 = "x";
    
    while ($row = mysql_fetch_assoc($query)) {
      $system = $row["system"];
      $userid = $row["userid"];
      $hostid = $row["hostid"];
      $cpu = $row["cpu"];
      $ngood = $row["ngood"];
      $nbad = $row["nbad"];
      $ntot1m = $row["n_jobs_1m"];
      $ngood1m = $row["n_good_jobs_1m"];
      $nevents = $row["nevents"];
      $failr1m = ($ntot1m > 0) ? 1 - $ngood1m / $ntot1m : 0;
      $failr1m = round(100 * $failr1m);
      
      if (is_null($userid)) $userid = "Unknown";
      if (is_null($hostid)) $hostid = "Unknown";
      
      $userlink = "?view=user&system=$system&userid=" . $userid;
      $hostlink = "?view=user&system=$system&userid=" . $userid . "#" . $hostid;
      
      $isInactive = ($ntot1m == 0); // host is inactive during last month
      $isProblem =  ($ntot1m > 100) && ($failr1m > 50); // > 50% jobs are failed
      
      // check filter
      if ($dispProblem && ! $isProblem) continue;
      if ($dispActive && $isInactive) continue;
      
      // style inactive or problematic hosts
      $rstyle = ($isInactive) ? "inactive" : "";
      $cstyle = ($isProblem) ? "problem" : "";
      
      // output empty cell if host does not has failed jobs
      if ($nbad == 0) $nbad = "";
      if ($failr1m == 0) $failr1m = "";
      
      // human-readable $system string
      switch ($system) {
        case 1: $system = "vLHC"; break;
        case 2: $system = "vLHCdev"; break;
        case 3: $system = "LHC"; break;
      }
      
      $isNewUser = ($userid != $user0);
      $user0 = $userid;
      
      echo "<tr class=\"" . $rstyle . "\">" .
           "<td>" . $system . "</td>" .
           "<td>" . ($isNewUser ? "<a href=\"" . $userlink . "\" rel=\"nofollow\">" . $userid . "</a>" : "") . "</td> " .
           "<td><a href=\"" . $hostlink . "\" rel=\"nofollow\">" . $hostid . "</a></td> " .
           "<td>" . number_format($cpu, 0, ".", " ") . "</td>" .
           "<td>" . $ngood . "</td>" .
           "<td>" . $nbad . "</td>" .
           "<td class=\"" . $cstyle . "\">" . $failr1m . "</td>" .
           "<td>" . number_format($nevents, 0, ".", " ") . "</td>" .
           "</tr>\n";
    }
    
    echo "</table>\n";
  }
  
  function display_contributions() {
    $data = array();
    
    // extract performance counters per batch system
    $query = "SELECT
                system,
                COUNT(DISTINCT system, userid) AS nusers,
                COUNT(*) AS nhosts,
                SUM(cpu_time) AS cpu,
                SUM(disk_usage) AS disk,
                SUM(n_events) AS events
              FROM
                api
              GROUP BY 1";
    $query = mysql_query($query);
    while ($row = mysql_fetch_assoc($query))
      $data[$row["system"]] = $row;
    
    // extract active users counters
    $query = "SELECT
                system,
                COUNT(DISTINCT system, userid) AS nusers_active,
                COUNT(*) AS nhosts_active,
                SUM((n_jobs_1m > 100)*(n_good_jobs_1m / n_jobs_1m < 0.5)) AS nhosts_problem
              FROM
                api
              WHERE
                n_jobs_1m > 0
              GROUP BY 1";
    $query = mysql_query($query);
    while ($row = mysql_fetch_assoc($query))
      $data[$row["system"]] += $row;
    
    // calculate totals
    function array_totals($totals, $item) {
      foreach ($item as $k => $v) $totals[$k] += $v;
      return $totals;
    }
    
    $data["total"] = array_reduce($data, "array_totals", array());
    
    //echo "<pre>"; print_r($data); echo "</pre>";
    
    echo "<table id=\"contrib\">\n";
    echo "<tr>\n";
    echo "  <th>System</th>\n";
    echo "  <th>Summary</th>\n";
    echo "</tr>\n";
    
    foreach ($data as $system => $row) {
      $cpuCY = round($row["cpu"] / 86400 / 365, 1);
      $diskTb = round($row["disk"] / 1024 / 1024 / 1024, 1);
      $events1e9 = round($row["events"] / 1e9, 1);
      
      // human-readable $system string
      switch ($system) {
        case 1: $systemName = "vLHC"; break;
        case 2: $systemName = "vLHCdev"; break;
        case 3: $systemName = "LHC"; break;
        default: $systemName = $system; break;
      }
      
      echo "<tr>\n";
      echo "  <td>" . $systemName . "</td>\n";
      echo "  <td>\n";
      
      echo "<p>\n";
      echo "Users: " . $row["nusers"] . " / active: " . $row["nusers_active"] . "<br>\n";
      echo "Hosts: <a href=\"?view=hosts&system=" . $system . "&display=all\">" . $row["nhosts"] . "</a> / " .
           "active: <a href=\"?view=hosts&system=" . $system . "&display=active\">" . $row["nhosts_active"] . "</a> / " .
           "problematic: <a href=\"?view=hosts&system=" . $system . "&display=problematic\">" . $row["nhosts_problem"] . "</a><br>\n";
      echo "CPU usage: " . $cpuCY . " <i>CPU*years</i><br>\n";
      echo "Disk usage: " . $diskTb . " <i>terabytes</i><br>\n";
      echo "Events generated: " . $events1e9 . " <i>billions</i><br>\n";
      echo "</p>\n";
      
      echo "  </td>\n";
      echo "</tr>\n";
    }
    
    echo "</table>\n";
    
    echo "<p>\n";
    echo "'Active' is a number of users/hosts which completed at least 1 job during last month.<br>\n";
    echo "'Problematic' is a number of active hosts which completed > 100 jobs and have > 50% jobs fail rate during last month.<br>\n";
    echo "All data on 'Contributions' page corresponds to statistics collected starting from 2012-05-18.<br>\n";
    echo "</p>\n";
    echo "\n";
  }
  
  function display_user($system, $userid) {
    switch ($system) {
      case 1:
        $project = "vLHCathome";
        $projectlink = "http://lhcathome2.cern.ch/vLHCathome/";
        break;
      
      case 2:
        $project = "vLHCathome-dev";
        $projectlink = "http://lhcathomedev.cern.ch/vLHCathome-dev/";
        break;
      
      case 3:
        $project = "LHCathome";
        $projectlink = "https://lhcathome.cern.ch/lhcathome/";
        break;
      
      default:
        $project = "Unknown";
        $projectlink = "";
        break;
    }
    
    $userlink = $projectlink . "show_user.php?userid=" . abs($userid);
    $filterUser = "system = $system AND " . (($userid == "Unknown") ? "userid IS NULL" : "userid = $userid");
    
    $query = "SELECT
                SUM(cpu_time) AS cpu,
                SUM(cpu_bad) AS cpubad,
                SUM(n_good_jobs) AS ngood,
                SUM(n_jobs - n_good_jobs) AS nbad,
                SUM(n_events) AS nevents,
                COUNT(DISTINCT hostid) AS nhosts,
                date1G,
                date10G
              FROM
                api
              WHERE
                $filterUser";
    $query = mysql_query($query);
    $row = mysql_fetch_assoc($query);
    
    $njobs = $row["ngood"] + $row["nbad"];
    
    $achi = array();
    if ($row["date1G"] != "")  $achi[] = "1G is reached on " . $row["date1G"];
    if ($row["date10G"] != "") $achi[] = "10G is reached on " . $row["date10G"];
    $achimsg = (count($achi) > 0) ? "(" . implode(", ", $achi) . ")" : "";
    
    echo "<p>\n";
    echo "<b>User summary</b> (<a href=\"" . $userlink . "\">" . $project . " profile</a>)<br>\n";
    echo "Hosts: " . $row["nhosts"] . "<br>\n";
    echo "Total CPU time: " . $row["cpu"] . " <i>s</i> (" . $row["cpubad"] . " <i>s</i> by failed jobs)<br>\n";
    echo "Total jobs: " . $njobs . " (" . $row["nbad"] . " failed, " . round(100*$row["nbad"]/$njobs) . "%)<br>\n";
    echo "Total events: " . $row["nevents"] . " " . $achimsg . "<br>\n";
    echo "</p>\n";
    echo "\n";
    
    $hosts = array();
    $query = "SELECT hostid, n_jobs_1m, n_good_jobs_1m FROM api WHERE $filterUser";
    $query = mysql_query($query);
    while ($row = mysql_fetch_assoc($query)) {
      $hosts[] = $row;
    }
    
    foreach ($hosts as $host) {
      $hostid = $host["hostid"];
      $njobs1m = $host["n_jobs_1m"];
      $njobsgood1m = $host["n_good_jobs_1m"];
      $isActive = ($njobs1m > 0);
      
      if (is_null($hostid)) $hostid = "Unknown";
      $filterHost = ($hostid == "Unknown") ? "hostid IS NULL" : "hostid = $hostid";
      
      $baseName = "host-$system-$userid-$hostid";
      
      // prepare jobs plot
      $query = "SELECT
                  DATE(retdate) AS date_d,
                  SUM(exitcode = 0) AS ngood,
                  SUM(exitcode != 0) AS nbad,
                  COUNT(*) AS total
                FROM
                  jobs
                WHERE
                  $filterUser AND $filterHost
                GROUP BY 1
                HAVING
                  date_d IS NOT NULL";
      dump_query($query, "cache/stats/$baseName.txt");
      
      if ($isActive) {
        $pars = "call 'host.gnuplot' " .
                date('"Y-m-d"', strtotime("-8 week")) . " " .
                date('"Y-m-d"', strtotime("+1 day")) . " " .
                "\"$baseName\"";
        file_put_contents("cache/stats/$baseName.gnuplot", $pars);
        exec("gnuplot cache/stats/$baseName.gnuplot");
      }
      
      $hostlink = $projectlink . "show_host_detail.php?hostid=" . $hostid;
      $hostname = ($hostid == "Unknown") ? $hostid : "<a href=\"" . $hostlink . "\">" . $hostid . "</a>";
      $njobsbad1m = $njobs1m - $njobsgood1m;
      
      echo "<p>\n";
      echo "<b id=\"" . $hostid . "\">Host " . $hostname . "</b><br>\n";
      if ($isActive) {
        echo "(last month) total jobs: " . $njobs1m . " (" . $njobsbad1m . " failed, " . round(100*$njobsbad1m/$njobs1m) . "%)<br>\n";
        echo "<a href=\"cache/stats/$baseName.txt\"><img src=\"cache/stats/$baseName.png\"></a><br>\n";
      }
      else {
        echo "(last month) inactive, <a href=\"cache/stats/$baseName.txt\">past progress log</a><br>\n";
      }
      
      echo "</p>\n";
      echo "\n";
    }
  }
  
  // check control access
  // TODO: CERN SSO for access control
  function check_access() {
    $ip = $_SERVER["REMOTE_ADDR"];
    
    // list of IP addresses of control machines
    // allow control from:
    //   none
    $nets = array();
    
    foreach ($nets as $net)
      if (strncmp($net, $ip, strlen($net)) === 0)
        return true;
    
    // no matches - return false
    return false;
  }
  
  function main() {
    // open database
    if (!mysql_connect("localhost", "mcplots")) exit;
    mysql_select_db("mcplots");
    
    // list tables in the database
    $tables = array();
    $query = mysql_query("SHOW TABLES");
    while ($row = mysql_fetch_row($query)) {
      $tables[] = $row[0];
    }
    
    // hide the page if the server is not for MC production
    if (!in_array("production", $tables)) {
      echo "<p>Not a MC production server.</p>\n";
      return;
    }
    
    // extract query
    $view = $_GET["view"]; // the page to display
    $rev = $_GET["rev"];   // the revision number
    $system = $_GET["system"];
    $userid = $_GET["userid"];
    
    // sanity checks:
    if ($view == "") $view = "status";
    if (!is_numeric($rev)) $rev = "0";
    if (!is_numeric($system)) $system = "1";
    if (!is_numeric($userid) && $userid != "Unknown") $userid = "0";
    
    // print menu
    echo "<p>\n";
    echo "<a href=\"?view=status\" "  . (($view == "status")  ? "class=\"selected\"" : "") . ">Status</a>\n";
    $lock = (check_access()) ? " <img src=\"img/unlocked.png\">" : "";
    echo "<a href=\"?view=control\" " . (($view == "control") ? "class=\"selected\"" : "") . ">Control" . $lock . "</a>\n";
    if ($view == "revision") {
      echo "<a href=\"?view=revision&rev=$rev\" class=\"selected\">Revision $rev</a>\n";
    }
    if ($view == "runs") {
      echo "<a href=\"?view=runs&rev=$rev\" class=\"selected\">Runs $rev</a>\n";
    }
    echo "<a href=\"?view=contrib\" " . (($view == "contrib") ? "class=\"selected\"" : "") . ">Contributions</a>\n";
    if ($view == "hosts") {
      echo "<a href=\"?view=hosts\" class=\"selected\">Hosts</a>\n";
    }
    if ($view == "user") {
      echo "<a href=\"?view=user&system=$system&userid=$userid\" class=\"selected\">User $userid</a>\n";
    }
    echo "</p>\n";
    echo "\n";
    
    if (check_access()) accept_commands();
    
    switch ($view) {
      case "control" : display_control();      break;
      case "revision": display_revision($rev); break;
      case "runs"    : display_runs($rev);     break;
      case "contrib" : display_contributions(); break;
      case "hosts"   : display_hosts();        break;
      case "user"    : display_user($system, $userid); break;
      default:         display_status();       break;
    }
  }
  
  main();
  
  echo "<hr>\n";
  echo "<p><span title=\"" . $t->summaryLine() . "\">Page generation took " . ms($t->elapsedFromStart()) . " ms</span></p>\n";
  
  mysql_close();
?>

</body>

</html>
