<?php
  class Config
  {
    //structure containg abbreviations of .conf file
    public $abbrs = array();
    //E.g.:
    //$key_name="something from .conf file before = in section [abbreviations]";
    //$c->abbr[$key_name]
    //$c->abbr[$key_name]["submenu"]=$submenu
    //$c->abbr[$key_name]["name"]=$name
    //$c->abbr[$key_name]["plotter"]=$plotter

    //structure containg styles of .conf file
    public $styles = array();
    //E.g.:
    //$key_name="something from .conf file before = in section [styles]";
    //$c->styles[$key_name]=$style

    //structure containg tunes and tunegroups of .conf file
    public $tunegroups = array();
    //E.g.:
    //[$menu][$submenu]="something from .conf file before = in section [tunegroups] splitted menu.submenu";
    //$c->tunegroups[$menu][$submenu][0]=$tune1
    //$c->tunegroups[$menu][$submenu][1]=$tune2
    //$c->tunegroups[$menu][$submenu][2]=$tune3

    //structure containg beamgroups of .conf file
    public $beamgroups = array();
    //E.g.:
    //$key_name="something from .conf file before = in section [beamsgroups]";
    //$c->beamgroups[$key_name][0]=$beam1
    //$c->beamgroups[$key_name][1]=$beam2
    //$c->beamgroups[$key_name][2]=$beam3

    // constructor with input parameter of .conf file name
    public function __construct($conffile) {
      // clean plots cache if obsolete
      $this->cleanCache($conffile);
      
      // get config content as array of strings
      $conf = file($conffile);
      
      $section = ""; // section name
      foreach ($conf as $line) {
        // skip comments and empty lines
        if ($line[0] == "#" || strlen(trim($line)) == 0) continue;
        
        // read section name
        if ($line[0] == "[") {
          $section = substr($line, 0, strpos($line, "]") + 1);
          continue;
        }
        
        // read parameter
        switch ($section) {
          case "[abbreviations]": $this->addAbbr($line); break;
          case "[styles]":        $this->addStyle($line); break;
          case "[tunegroups]":    $this->addTgrp($line);  break;
          case "[beamgroups]":    $this->addBgrp($line); break;
        }
      }
    }
    
    // check and clean plots cache if obsolete
    // (config file $conffile is newer than 'cache/plots' directory)
    private function cleanCache($conffile) {
      $cachedir = 'cache/plots';
      
      $conftime = filemtime($conffile);  // timestamp of config
      $cachetime = filemtime($cachedir); // timestamp of plots cache
      
      if (!is_dir($cachedir) || $conftime > $cachetime) {
        // drop old cache (in background)
        $cache0 = 'cache/plots.obsolete.' . rand();
        exec("mv $cachedir $cache0");
        exec("rm -rf $cache0 >& /dev/null &");
        
        // and create new empty plots cache
        exec("mkdir -p $cachedir");
        exec("chmod -R a+w $cachedir");
      }
    }
    
    // function to add from line to abbrs datastructure
    // format: key = submenu ! name ! plotter
    private function addAbbr($line) {
      list($key, $value) = explode("=", $line, 2);
      list($sub, $name, $plot) = explode("!", $value);
      $key  = trim($key);
      $sub  = trim($sub);
      $name = trim($name);
      $plot = trim($plot);
      $this->abbrs[$key] = array("submenu" => $sub, "name" => $name, "plotter" => $plot);
    }
    
    // function to add from line to styles datastructure
    // format: key = style
    private function addStyle($line) {
      list($key, $value) = explode("=", $line);
      $key   = trim($key);
      $value = trim($value);
      $value = preg_replace("/  */", " ", $value); // replace all doublespaces by single spaces
      $this->styles[$key] = $value;
    }

    //function to add from line to tunegroups datastructure
    // format: key = tune, tune, tune, ...
    // key can be specified as "generator.submenu"
    private function addTgrp($line) {
      list($key, $value) = explode("=", $line);
      $key = trim($key);
      
      $tunes = explode(",", $value);
      $tunes = array_map('trim', $tunes);  // trim each element
      
      $this->tunegroups[$key] = $tunes;
    }

    //function to add from line to beamgroup datastructure
    // format: key = beam, beam, beam, ...
    private function addBgrp($line) {
      list($name, $value) = explode("=", $line);
      $name = trim($name);
      
      // remove '/' characted to avoid escape codes in URLs
      $key = str_replace("/", "", $name);
      
      // extract beams list
      $beams = explode(",", $value);
      $beams = array_map('trim', $beams);  // trim each element
      $this->beamgroups[$key] = $beams;
      
      // add original name to abbreviations map
      $this->AddAbbr($key . " = ! " . $name . " ! ");
    }

    // this is a wrapper for $abbr map to address
    // the case then abbreviation is not defined in the `$abbr` map
    public function name($a, $prefix = "") {
      if (array_key_exists($prefix . "." . $a, $this->abbrs))
        return $this->abbrs[$prefix . "." . $a]["name"];
      
      if (array_key_exists($a, $this->abbrs))
        return $this->abbrs[$a]["name"];
      
      return $a;
    }

    // the same 'submenu' field
    public function submenu($a, $prefix = "") {
      if (array_key_exists($prefix . "." . $a, $this->abbrs))
        return $this->abbrs[$prefix . "." . $a]["submenu"];
      
      return $this->abbrs[$a]["submenu"];
    }

    // the same for 'plotter' field
    public function plotter($a, $prefix = "") {
      if (array_key_exists($prefix . "." . $a, $this->abbrs))
        return $this->abbrs[$prefix . "." . $a]["plotter"];
      
      if (array_key_exists($a, $this->abbrs))
        return $this->abbrs[$a]["plotter"];
      
      return $a;
    }
    
    // return the plotting style
    function style($row) {
      // use "generator.tune" as style tag for mc histograms
      // and "data" tag for data histograms
      $tag = ($row["type"] == "mc") ? $row["generator"] . "." . $row["tune"] : "data";
      
      // get style definition string
      if (array_key_exists($tag, $this->styles))
        return $this->styles[$tag];
      
      // style is missing, get the default style
      return $this->styles["*"];
    }
  }

  // parse string with generators versions from URL
  function unpackStr($xstr) {
    $items = explode("--1", $xstr);
    $gvlist = array();
    
    foreach ($items as $i) {
      list($g, $v) = explode("~", $i);
      $gvlist[$g][0] = $v;
    }
    
    return $gvlist;
  }
?>
