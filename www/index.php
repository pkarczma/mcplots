<!DOCTYPE html>
<html>

<head>
  <title>MCplots</title>
  <link rel="shortcut icon" href="img/logo.png"/>
  <link rel="stylesheet" type="text/css" href="style.css"/>
  <script async src="mcplots.js"></script>
</head>

<body>

<?php
  // start profiler
  include "profiler.php";
  $t = new Profiler();

  // extract the query
  list($q_page, $q_beamgroup, $q_process, $q_observable, $q_tunegroup, $q_gen_version, $q_valid_gtvr) = explode(",", $_GET["query"]);
  $q_gen_version = rawurldecode($q_gen_version);
  
  // open database
  if (!mysql_connect("localhost", "mcplots")) exit;
  mysql_select_db("mcplots");



  include "config.php";
  // prepare abbreviations maps, styles map, tunegroups map, beamgroups maps
  $c = new Config("mcplots.conf");
  
  //delimiter used in validation pages to divide variables
  $safeDelimiter = "--";
  
  // menu for validation pages
  function print_validation_menu() {
    global $q_valid_gtvr, $q_observable;
    global $q_generator, $q_tune, $q_version, $q_ref;
    global $safeDelimiter;
    
    $query = mysql_query("SELECT DISTINCT generator, tune FROM histograms WHERE type = 'mc' ORDER BY 1, 2");
    $menu = array();
    
    while ($row = mysql_fetch_row($query)) {
      $generator  = $row[0];
      $tune       = $row[1];
      
      $menu[$generator][$tune][] = $row;
    }

    list($q_generator, $q_tune, $q_version, $q_ref) = explode($safeDelimiter, $q_valid_gtvr);

    //Facebook like button workaround (++ is replaced by two spaces and it causes a problem)
    $o_generator = $q_generator;
    //change spaces by ++
    $q_generator = str_replace("  ","++",$q_generator);
    //change it in script filename as well (but it has to be connected with generator to avoid unwanted changes
    $q_observable = str_replace($o_generator,$q_generator,$q_observable);

    
    //echo "$q_valid_gtvr:$q_generator--$q_tune--$q_version"; //debug
    
    // print menu
    echo "<h3>Generator / Tune</h3>\n";
    foreach (array_keys($menu) as $generator) {
      //select default if nothing is selected
      if ($q_generator == "" ) $q_generator = $generator;
      
      echo "<div>&rarr; <span class=\"submenu-label\" onClick=\"toggle(this)\">" . $generator . "</span><br>\n";
      
      // make all items of submenu visible if one of the items selected
      $class = ($generator == $q_generator) ? "submenu-items" : "submenu-items-hidden";
      echo "<div class=\"$class\">\n";
      
      foreach (array_keys($menu[$generator]) as $tune) {
        if ($q_tune == "" ) $q_tune = $tune;
        $sel = (($tune == $q_tune) && ($generator == $q_generator) ) ? " class=\"selected\"" : "";
        $validationUrl = prepare_link(array("valid","","","","","",$generator.$safeDelimiter.$tune));
        printf("&rarr; <a href=\"%s\" %s>%s</a><br>\n",
                   $validationUrl,
                   $sel,
                   $tune);
      }
      echo "</div>\n";
      echo "</div>\n";
      echo "\n";
    }
  }
  
  // menu for tuning validation pages
  function print_tuningvalidation_menu() {
    global $q_valid_gtvr, $q_observable;
    global $q_generator, $q_tune, $q_version, $q_ref;
    global $safeDelimiter;
    
    $query = mysql_query("SELECT DISTINCT generator, version FROM histograms WHERE type = 'mc' ORDER BY 1, 2");
    $menu = array();
    
    while ($row = mysql_fetch_row($query)) {
      $generator  = $row[0];
      $version    = $row[1];
      
      $menu[$generator][$version][] = $row;
    }

    list($q_generator, $q_version, $q_tune, $q_ref) = explode($safeDelimiter, $q_valid_gtvr);

    //Facebook like button workaround (++ is replaced by two spaces and it causes a problem)
    $o_generator = $q_generator;
    //change spaces by ++
    $q_generator = str_replace("  ","++",$q_generator);
    //change it in script filename as well (but it has to be connected with generator to avoid unwanted changes
    $q_observable = str_replace($o_generator,$q_generator,$q_observable);

    
    //echo "$q_valid_gtvr:$q_generator--$q_version--$q_tune"; //debug
    
    // print menu
    echo "<h3>Generator / Version</h3>\n";
    foreach (array_keys($menu) as $generator) {
      //select default if nothing is selected
      if ($q_generator == "" ) $q_generator = $generator;
      
      echo "<div>&rarr; <span class=\"submenu-label\" onClick=\"toggle(this)\">" . $generator . "</span><br>\n";
      
      // make all items of submenu visible if one of the items selected
      $class = ($generator == $q_generator) ? "submenu-items" : "submenu-items-hidden";
      echo "<div class=\"$class\">\n";
      
      foreach (array_keys($menu[$generator]) as $version) {
        if ($q_version == "" ) $q_version = $version;
        $sel = (($version == $q_version) && ($generator == $q_generator) ) ? " class=\"selected\"" : "";
        $validationUrl = prepare_link(array("validgen","","","","","",$generator.$safeDelimiter.$version));
        printf("&rarr; <a href=\"%s\" %s>%s</a><br>\n",
                   $validationUrl,
                   $sel,
                   $version);
      }
      echo "</div>\n";
      echo "</div>\n";
      echo "\n";
    }
  }

  // menu for all other pages
  function print_plots_menu() {
    global $q_beamgroup, $q_gen_version, $q_tunegroup, $q_process, $q_observable;
    global $beamslist, $q_valid_gtvr, $analyses_list;
    global $c;
    
    echo "<h3>Analysis filter:</h3>\n";
    echo "  &rarr;Beam: ";
    foreach (array_keys($c->beamgroups) as $beamgroup) {
      // by default select first group
      if ($q_beamgroup == "") $q_beamgroup = $beamgroup;

      $sel = ($beamgroup == $q_beamgroup && ($q_valid_gtvr == "")) ? " class=\"selected\"" : "";

      printf("<a href=\"%s\" %s>%s</a>\n",
             prepare_link(array("plots",$beamgroup,"","","",$q_gen_version,"")),
             $sel,
             $c->name($beamgroup));
    }
    echo "<br>\n";

    //link to show after choosing analysis in drop down
    $urlAll = prepare_link(array("plots","",$q_process,$q_observable,$q_tunegroup,$q_gen_version,""));
    $urlOne = prepare_link(array("plots","","","",$q_tunegroup,$q_gen_version,""),true);
    
    echo "  &rarr;Analysis:\n";
    //select box for filtering analysis
    echo "<div>\n";
    echo "<select id=\"analysis\" onChange=\"navigate(this.value);\">\n";
    //print all options
    $query = mysql_query("SELECT DISTINCT reference FROM histograms
                          ORDER BY reference"); 
    while ($row = mysql_fetch_row($query)) {
      $ref = $row[0];
      $label = str_replace("_", " ", $ref);
      $sel = ($q_valid_gtvr == $ref ? "selected" : "");
      $url = ($ref == "") ? $urlAll : $urlOne . $ref;
      echo "    <option value=\"$url\" $sel>$label</option>\n";
    }
    echo "  </select>\n";
    echo "</div>\n";
    
    // TODO: 'latest' analysis filter is outdated and so hidden now
    $sel = ($q_valid_gtvr == "latest") ? " class=\"selected\"" : "";
    //printf("  &rarr;<a href=\"%s\" %s>Latest analyses</a><br>\n",
    //         prepare_link(array("plots","","","","",$q_gen_version,"latest")),
    //         $sel);
    echo "\n";
    
    // prepare array of selected analyses
    $analyses_list = array($q_valid_gtvr);
    if ($q_valid_gtvr == "latest") {
      // "latest" is a special case: set to hardcoded list
      $analyses_list = array("ATLAS_2012_I1183818", "LHCB_2011_I917009", "CMS_2012_I1184941", "CMS_2012_I1102908", "CMS_2012_I1087342", "LHCF_2012_I1115479", "TOTEM_2011_I930960", "CMS_2012_I1193338", "ATLAS_2011_I894867", "TOTEM_2012_DNDETA", "CMS_2011_S9215166", "ATLAS_2012_I1084540", "ATLAS_2011_S9126244", "LHCB_2010_I867355", "ALICE_2012_I1181770", "TOTEM_2012_002", "CMS_2012_PAS_FWD_11_003", "CMS_2012_PAS_QCD_11_010", "LHCB_2012_I1119400", "MC_GAPS");
    }
    
    $beamslist= "";
    if ($q_valid_gtvr == ""){ //if no filter analysis
      // get beams of beamgroup
      $beams = $c->beamgroups[$q_beamgroup];
      $beamslist = "'" . implode("', '", $beams) . "'";
      $sqlCondition = " beam IN ($beamslist) ";
    }
    else {
      $sqlCondition = " reference IN ('" . implode("', '", $analyses_list) . "') ";
      $query = mysql_query("SELECT DISTINCT beam FROM histograms WHERE $sqlCondition");
      while ($row = mysql_fetch_row($query)){
        $beamslist .= ",'" . $row[0] . "'";
      }
      $beamslist = substr($beamslist,1,strlen($beamslist));
    }

    // prepare list of all uniq combinations of 'process - observable'
    $query = mysql_query("SELECT DISTINCT process, observable
                            FROM histograms
                           WHERE $sqlCondition
                           ORDER BY 1, 2");

    $menu0 = array();

    while ($row = mysql_fetch_row($query)) {
      $process    = $row[0];
      $observable = $row[1];
      $submenu    = $c->submenu($observable,$process);

      $menu0[$process][$submenu][] = $observable;
    }
    
    // sort menu according the order in config file
    $menu = array();
    $pord = array_keys(array_intersect_key($c->abbrs, $menu0)); // items specified in config
    $petc = array_diff(array_keys($menu0), $pord); // unspecified
    $psort = array_merge($pord, $petc); // specified + unspecified
    foreach ($psort as $p) $menu[$p] = $menu0[$p];
    
    // print menu
    foreach (array_keys($menu) as $process) {
      echo "<h3>" . $c->name($process) . "</h3>\n";

      foreach (array_keys($menu[$process]) as $submenu) {
        $nitems = count($menu[$process][$submenu]);
        if ($nitems > 1 && $submenu != "") {
          // this is submenu
          // print menu label
          echo "<div>&rarr; <span class=\"submenu-label\" onClick=\"toggle(this)\">" . $submenu . "</span><br>\n";

          // make all items of submenu visible if one of the items selected
          $class = (($process == $q_process) && (in_array($q_observable, $menu[$process][$submenu]))) ? "submenu-items" : "submenu-items-hidden";
          echo "<div class=\"$class\">\n";
        }

        foreach ($menu[$process][$submenu] as $observable) {
          $sel = (($process == $q_process) && ($observable == $q_observable)) ? " class=\"selected\"" : "";

          printf("&rarr; <a href=\"%s\" %s>%s</a><br>\n",
                 prepare_link(array("plots",$q_beamgroup,$process,$observable,$q_tunegroup,$q_gen_version,$q_valid_gtvr)),
                 $sel,
                 $c->name($observable,$process));
        }

        if ($nitems > 1 && $submenu != "") {
          echo "</div>\n";
          echo "</div>\n";
          echo "\n";
        }
      }
    }
  }

  // === prepare menu ===
  $versUrl = prepare_link(array("versions",$q_beamgroup,"","","",$q_gen_version,$q_valid_gtvr));
  
  echo "<div id=\"menu\">\n";
  echo "<h2>Menu</h2>\n";
  echo "&rarr; <a href=\"?query=frontpage\">Front Page</a><br>\n";
  echo "&rarr; <a href=\"http://lhcathome.web.cern.ch/projects/test4theory\">LHC@home / Test4Theory</a><br>\n";
  echo "&rarr; <a href=\"" . $versUrl . "\">Generator Versions</a><br>\n";
  echo "&rarr; <a href=\"?query=valid\">Generator Validation</a><br>\n";
  echo "&rarr; <a href=\"?query=validgen\">Tuning Validation</a><br>\n";
  echo "&rarr; <a href=\"?query=news\">Update History</a><br>\n";
  echo "&rarr; <a href=\"http://arxiv.org/abs/1306.3436\">User Manual and Reference</a><br>\n";
  echo "\n";
  
  if (in_array($q_page, array("valid", "validdetail", "imgdetail"))) {
    print_validation_menu();
  }
  else if (in_array($q_page, array("validgen"))) {
    print_tuningvalidation_menu();
  }
  else {
    print_plots_menu();
  }
  
  echo "</div>\n";
  echo "\n";

  // === prepare plots ===

  // this function prepare steering file for plotter tool
  // $key is a customization option to generate slightly different steering files
  // for different sections of the site: index, valid, validgen
  function prepare_plotter_steer(array $rows, $fsteer, $img, $key = "index") {
    global $c;
    $row0 = $rows[0];
    $cuts0 = $c->plotter($row0["cuts"]);
    
    $labels = array();
    foreach ($rows as $row) {
      if ($row["type"] != "mc") continue;
      
      switch ($key) {
        // collect names and versions of generators
        case "index": $label = $c->name($row["generator"]) . " " . $row["version"]; break;
        
        // collect tunes of generators
        case "valid": $label = $c->name($row["tune"], $row["generator"]); break;
        
        // collect versions of generators
        case "validgen": $label = $c->name($row["version"], $row["generator"]); break;
        
        default: $label = ""; break;
      }
      
      $labels[$label] = true;
    }
    
    $script = "# BEGIN PLOT\n" .
              "Title=" . $c->plotter($row0["observable"], $row0["process"]) . (($cuts0 != "") ? (" (" . $cuts0 . ")") : "") . "\n" .
              "upperLeftLabel=" . $row0["energy"] . " GeV " . $row0["beam"] . "\n" .
              "upperRightLabel=" . $c->plotter($row0["process"]) . "\n" .
              "textField1=" . implode(", ", array_keys($labels)) . "\n" .
              "textField2=" . $row0["reference"] . "\n" .
              "outputFileName=$img\n" .
              "drawRatioPlot=1\n" .
              "# END PLOT\n" .
              "\n";
    
    $i = 0;
    
    foreach ($rows as $row) {
      // get plotting style and split it into components
      list($r, $g, $b, $lineStyle, $lineWidth, $markerStyle, $markerSize) = explode(" ", $c->style($row));
      
      $legend = $row["experiment"];
      
      if ($row["type"] == "mc") {
        // override color
        if ($key == "valid")
          list($r, $g, $b, ) = explode(" ", $c->styles["line" . $i++], 4);
        
        // set legend
        switch ($key) {
          case "index": $legend = $c->name($row["tune"], $row["generator"]); break;
          case "valid": $legend = $row["version"]; break;
          case "validgen": $legend = $row["tune"]; break;
          default: $legend = ""; break;
        }
      }
      
      $script .= "# BEGIN HISTOGRAM\n" .
                 "filename=" . $row["fname"] . "\n" .
                 "markerStyle=$markerStyle\n" .
                 "markerSize=$markerSize\n" .
                 "lineStyle=$lineStyle\n" .
                 "lineWidth=$lineWidth\n" .
                 "color=$r $g $b\n" .
                 "legend=$legend\n" .
                 "reference=" . (($row["type"] == "mc") ? "0" : "1") . "\n" .
                 "# END HISTOGRAM\n" .
                 "\n";
    }

    file_put_contents($fsteer, $script);
  }
  
  // this function converts reference string to the url of reference article
  function get_reflink($xreference) {
    
    // try to get Spires ID (example: ALICE_2010_S8625980)
    preg_match("/_S([0-9]+)$/", $xreference, $matches);
    if ($matches[0] != "") {
      $spiresID = $matches[1];
      return "http://inspirehep.net/search?p=find+key+" . $spiresID;
    }
    
    // try to get inSPIRE ID (example: ATLAS_2011_I926145)
    preg_match("/_I([0-9]+)$/", $xreference, $matches);
    if ($matches[0] != "") {
      $inspireID = $matches[1];
      return "http://inspirehep.net/search?p=recid+" . $inspireID;
    }
    
    // try to get ATLAS conference ID (example: ATLAS_2010_CONF_2010_031)
    preg_match("/^ATLAS_[0-9]{4}_CONF_([0-9]{4})_([0-9]+)$/", $xreference, $matches);
    if ($matches[0] != "") {
      $atlasLink = "ATLAS-CONF-" . $matches[1] . "-" . $matches[2];
      return "http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/" . $atlasLink;
    }
    
    // try to get CMS paper ID (example: CMS_EWK_10_012)
    preg_match("/^CMS_([A-Z]{3})_([0-9]{2})_([0-9]{3})$/", $xreference, $matches);
    if ($matches[0] != "") {
      $cmsLink = "CMS-" . $matches[1] . "-" . $matches[2] . "-" . $matches[3];
      return "http://cds.cern.ch/search?f=reportnumber&p=" . $cmsLink;
    }
    
    // try to get CMS paper ID (example: CMS_2012_PAS_QCD_11_010)
    preg_match("/^CMS_([0-9]{4})_([A-Z]{3})_([A-Z]{3})_([0-9]{2})_([0-9]{3})$/", $xreference, $matches);
    if ($matches[0] != "") {
      $cmsLink = "CMS-" . $matches[2] . "-" . $matches[3] . "-" . $matches[4] . "-" . $matches[5];
      return "http://cds.cern.ch/search?f=reportnumber&p=" . $cmsLink;
    }
    
    // try to get TOTEM paper ID (example: TOTEM_2012_002)
    preg_match("/^TOTEM_([0-9]{4})_([0-9]{3})$/", $xreference, $matches);
    if ($matches[0] != "") {
      $cmsLink = "TOTEM-" . $matches[1] . "-" . $matches[2];
      return "http://cds.cern.ch/search?f=reportnumber&p=" . $cmsLink;
    }
    
    // return link to analysis on Rivet page:
    return "http://projects.hepforge.org/rivet/analyses#" . $xreference;
  }
  
  function print_groups() {
    global $q_beamgroup, $q_process, $q_observable, $q_tunegroup, $q_gen_version;
    global $beamslist, $q_valid_gtvr;
    global $c;
    
    // query for all available combinations of generator/tune for selected beam/process/observable:
    $query = "SELECT DISTINCT generator, tune FROM histograms
              WHERE beam IN ($beamslist) AND
                    process = '$q_process' AND
                    observable = '$q_observable' AND
                    type = 'mc'";
    $query = mysql_query($query);
    
    $tunes = array();
    while ($row = mysql_fetch_assoc($query)) {
      $generator = $row["generator"];
      $tune = $row["tune"];
      $tunes[] = $generator . "." . $tune;
    }
    $tunes[] = "@custom"; // add '@custom' pseudo-tune to display 'Custom' group
    
    // keep only those groups which have at least one of $tunes
    $groups = array();
    foreach ($c->tunegroups as $key => $value) {
      if (count(array_intersect($tunes, $value)) > 0 || $q_tunegroup == $key) {
        list($menu, $submenu) = explode(".", $key);
        $groups[$menu][$submenu] = $value;
        
        // by default select first non-empty tune group
        if ($q_tunegroup == "") {
          $q_tunegroup = $key;
        }
      }
    }
    
    // extract menu.submenu from group name
    list($q_tunegroupm) = explode(".", $q_tunegroup);
    
    // print tune groups
    echo "<table id=\"groups\">\n";
    echo "<tr>\n";
    echo "  <td>Generator&nbsp;Group:</td>\n";
    echo "  <td>\n";
    
    // go through all menus
    foreach (array_keys($groups) as $menu) {
      list($submenu0) = array_keys($groups[$menu]); // assign first element
      $tunegroup = ($submenu0 == "") ? $menu : $menu . "." . $submenu0;
      $sel = ($menu == $q_tunegroupm) ? " class=\"selected\"" : "";
      
      printf("  <a href=\"%s\" %s>%s</a>\n",
             prepare_link(array("plots",$q_beamgroup,$q_process,$q_observable,$tunegroup,$q_gen_version,$q_valid_gtvr)),
             $sel,
             $menu);
    }
    echo "  </td>\n";
    echo "</tr>\n";
    
    $submenus = array_keys($groups[$q_tunegroupm]);
    $submenu0 = $c->tunegroups[$q_tunegroupm];
    
    $displayCustom = ($submenu0 == array("@custom"));
    $displaySubmenu = (!$displayCustom && $submenus != array(""));
    
    if ($displaySubmenu) {
      echo "<tr>\n";
      echo "  <td>Subgroup:</td>\n";
      echo "  <td>\n";
      
      foreach ($submenus as $submenu) {
        if ($submenu == "") continue;
        
        $tunegroup = ($submenu == "") ? $q_tunegroupm : $q_tunegroupm . "." . $submenu;
        $sel = ($tunegroup == $q_tunegroup) ? " class=\"selected\"" : "";
        
        printf("  <a href=\"%s\" %s>%s</a>\n",
               prepare_link(array("plots",$q_beamgroup,$q_process,$q_observable,$tunegroup,$q_gen_version,$q_valid_gtvr)),
               $sel,
               $submenu);
      }
      echo "  </td>\n";
      echo "</tr>\n";
    }
    else if ($displayCustom) {
      echo "<tr>\n";
      echo "  <td></td>\n";
      echo "  <td>\n";
      
      echo "  <form id=\"custom\" method=\"get\">\n";
      echo "  <input type=hidden name=query value=\"" . $_GET["query"] . "\">\n";
      
      // query for all available combinations of generator/tune for selected beam/process/observable:
      $query = "SELECT DISTINCT generator, version, tune FROM histograms
                WHERE beam IN ($beamslist) AND
                      process = '$q_process' AND
                      observable = '$q_observable' AND
                      type = 'mc'
                ORDER BY generator, version, tune";
      $query = mysql_query($query);
      
      $options = array();
      while ($row = mysql_fetch_assoc($query)) {
        $gen = $row["generator"];
        $ver = $row["version"];
        $options[$gen][$ver][] = $row["tune"];
      }
      
      echo "<table>\n";
      $prevgen = "";
      foreach ($options as $gen => $vers) {
        foreach ($vers as $ver => $tunes) {
          $dispgen = ($gen != $prevgen) ? $gen : "";
          $prevgen = $gen;
          echo "<tr>\n";
          echo "<td>$dispgen</td><td>$ver</td>\n";
          echo "<td>\n";
          
          foreach ($tunes as $tune) {
            $option = $gen . " " . $ver . " " . $tune;
            $chk = in_array($option, $_GET["custom"]) ? "checked" : "";
            
            echo "  <label><input type=checkbox name=custom[] value=\"$option\" $chk><span>$tune</span></label>\n";
          }
          
          echo "</td>\n";
          echo "</tr>\n";
        }
      }
      echo "  </table>\n";
      
      echo "  <input type=submit value=\"Display\">\n";
      
      echo "  </form>\n";
      echo "  </td>\n";
      echo "</tr>\n";
    }
    
    echo "</table>\n";
    echo "\n";
  }
  
  function print_plots() {
    global $q_beamgroup, $q_process, $q_observable, $q_tunegroup, $q_gen_version;
    global $beamslist, $q_valid_gtvr, $analyses_list;
    global $c, $t;
    
    // print submenu name and colon only if the submenu name is not empty
    $sub = $c->submenu($q_observable,$q_process);
    $sub = ($sub != "") ? $sub . " : " : "";
    
    echo "<h2 id=\"title\">" . $c->name($q_process) . " : " . $sub . $c->name($q_observable,$q_process) . "</h2>\n";
    echo "\n";
    
    print_groups();
    
    $submenu0 = $c->tunegroups[$q_tunegroup];
    $displayCustom = ($submenu0 == array("@custom"));
    
    if ($displayCustom) {
      $custom = $_GET["custom"];
      
      // return if nothing is selected for display:
      if (count($custom) == 0) return;
      
      $tunes = array();
      $userGenVers = array();
      foreach ($custom as $option) {
        list($generator, $version, $tune) = explode(" ", $option);
        
        $tunes[] = $generator . "." . $tune;
        if (! in_array($version, $userGenVers[$generator]))
          $userGenVers[$generator][] = $version;
      }
    }
    else {
      $tunes = $submenu0;
      
      // get array of generators from 5th URL value
      $userGenVers = unpackStr($q_gen_version);
    }
    
    $userGens = array_keys($userGenVers);
    
    $tuneslist = "'" . implode("', '", $tunes) . "'" . ", '.'"; // add empty tune to select data histograms as well: ", ''"

    $query = "SELECT * FROM histograms
              WHERE beam IN ($beamslist) AND
                    process = '$q_process' AND
                    observable = '$q_observable' AND
                    CONCAT(generator, '.', tune) IN ($tuneslist)
              ORDER BY energy DESC, beam, cuts, type, generator, version, experiment, tune";
    
    // prepare map of plots
    $query = mysql_query($query);

    $plots = array();
    while ($row = mysql_fetch_assoc($query)) {
      $eb     = $row["beam"] . " @ " . $row["energy"] . " GeV";
      $cuts   = $row["cuts"];
      $type   = $row["type"];
      $generator = $row["generator"];
      $version = $row["version"];
      $plots[$eb][$cuts][$type][$generator][$version][] = $row;
    }

    foreach (array_keys($plots) as $eb) {
      //if filter is set - check whether show beam and energy or not
      if ($q_valid_gtvr != "") {
        $skipIt = true;
        foreach (array_keys($plots[$eb]) as $cuts) {
          $myrows  = (array) $plots[$eb][$cuts]["data"][""][""];
          if (in_array($myrows[0]["reference"], $analyses_list)) {
            $skipIt = false;
            break;
          }
        }
        if ($skipIt) continue;
      }

      $id = str_replace(" @ ", "", $eb);
      $id = str_replace(" GeV", "", $id);
      echo "<h3 id=\"$id\"><a href=\"#$id\">$eb</a></h3>\n";
      echo "<div>\n";
      echo "\n";
      
      foreach (array_keys($plots[$eb]) as $cuts) {
        $t->start();
        
        //if filter is set then check whether show certain image file
        if ($q_valid_gtvr != "") {
          $myrows  = (array) $plots[$eb][$cuts]["data"][""][""];
          $skipIt = ! in_array($myrows[0]["reference"], $analyses_list);
          if ($skipIt) continue;
        }
        // extract list of DATA histograms:
        $rows  = (array) $plots[$eb][$cuts]["data"][""][""];
        
        // image file name suffix
        $suffix = "";
        
        // extract list of MC histograms:
        foreach (array_keys((array) $plots[$eb][$cuts]["mc"]) as $generator) {
          // array of all histograms of all versions of $generator:
          $versions = $plots[$eb][$cuts]["mc"][$generator];
          
          if (in_array($generator, $userGens)) {
            // user set the preference on the $generator (in "Generators and Versions")
            
            // extract selected versions:
            $userVersions = $userGenVers[$generator];
            
            // prepare image suffix
            $suffix .= $generator . "-" . implode("-", $userVersions) . "-";
            
            foreach ($userVersions as $version) {
              // check if selected $version of the $generator is amongst histograms:
              if (in_array($version, array_keys($versions))) {
                // TODO: implement filtering according to tunes
                //       in case of 'Custom' generator group
                $rows = array_merge($rows, $versions[$version]);
              }
            }
          }
          else {
            // use the latest available version what we have
            $maxver = max(array_keys($versions));
            $rows  = array_merge($rows, $versions[$maxver]);
          }
        }
        
        // at this point array $rows contents all histograms which we want
        // to display on the plot

        $energy = $rows[0]["energy"];
        $beam   = $rows[0]["beam"];

        // construct file name
        $fname = str_replace(" ", "_", "$q_process-$q_observable-$q_tunegroup-$cuts-$beam-$energy-$suffix");
        
        // replace '*' characters by 'x' to prevent globbing by convert tool
        // (to avoid large performance penalty due to enumeration of all files
        //  in cache/plots directory which could have millions of files)
        $fname = str_replace("*", "x", $fname);
        
        $fsteer = "cache/plots/$fname.script";
        $img = "cache/plots/$fname";

        $t->stamp("plot-prep");
        
        // skip image generation if it already exists in cache/
        if (! file_exists("$img.small.png")) {
          // prepare steering file
          prepare_plotter_steer($rows, $fsteer, $img);
          $t->stamp("plot-steer");
          
          // execute plotter to prepare .eps and .pdf files
          exec("./plotter.exe " . escapeshellarg($fsteer) . " >> cache/plotter.log 2>&1");
          $t->stamp("plot-plotter");
          
          // and convert .eps to .png
          // the raster (.png) plot looks rough being produced
          // by plotter (ROOT), that is why we deside to use
          // additional step with `convert` utility
          exec("convert -density 100 " . escapeshellarg("$img.eps") . " " . escapeshellarg("$img.png"));
          $t->stamp("plot-convert-png");
          
          exec("convert -density  50 " . escapeshellarg("$img.eps") . " " . escapeshellarg("$img.small.png"));
          $t->stamp("plot-convert-png-small");
          
          // optimize .png file size (lossless, by 30-40%)
          exec("optipng -q -zs0 -f0 " . escapeshellarg("$img.png"));
          exec("optipng -q -zs0 -f0 " . escapeshellarg("$img.small.png"));
          $t->stamp("plot-optipng");
        }

        // print cell with one plot
        echo "<div class=\"plot\">\n";
        echo "<h4>" . $c->name($cuts) . "</h4>\n";
        
        // alt text for the plot:
        $alt = "Plot of $q_observable in $energy GeV $beam collisions";
        
        // print image
        echo "<div><a href=\"$img.png\"><img src=\"$img.small.png\" alt=\"$alt\"></a></div>\n";
        
        // print "caption"
        echo "<div class=\"caption\">\n";
        echo " <a href=\"$img.pdf\">[pdf]</a>\n";
        echo " <a href=\"$img.eps\">[eps]</a>\n";
        echo " <a href=\"$img.png\">[png]</a>\n";
        
        echo " <span class=\"caption-toggler\" onClick=\"togglespan(this)\">show details &rarr;</span><br>\n";
        echo " <span class=\"caption-items-hidden\">\n";
        
        foreach ($rows as $row) {
          if ($row["type"] == "mc") {
            echo "  [<a href=\"" . $row["fname"] . "\">" . $c->name($row["tune"], $row["generator"]) . "</a>] ";
            echo "<a href=\"" . str_replace(".dat", ".params", $row["fname"]) . "\">param</a><br>\n";
          }
          else {
            echo "  [<a href=\"" . $row["fname"] . "\">" . $row["experiment"] . "</a>] ";
            echo "<a href=\"". get_reflink($row["reference"]) . "\">reference</a><br>\n";
          }
        }
        
        echo "  [<a href=\"$fsteer\">steer</a>]\n";
        
        echo " </span>\n";
        echo "</div>\n";
        
        echo "</div>\n";
        echo "\n";
        
        $t->stamp("plot-html");
      }

      echo "</div>\n";
      echo "\n";
    }

    if (count($plots) == 0) {
      echo "<h4>There are no histograms for selected combination of process/observable/tune.</h4>\n";
    }
  }

  function spec_format($xnum, $xsign = false) {
    $lnum=$xnum;
    if (!is_numeric($xnum)) return $xnum;
    if (number_format(abs($xnum),6)=="0.000000") return 0;
    //branch for 0.XXXX number
    if (floor(abs($lnum))==0){
      $cntdec=1;
      while (floor(abs($lnum))==0){
        $cntdec++;
        $lnum=$lnum*10;
      }
      $ynum=round($xnum,$cntdec);
    }
    //branch for XXX.00 number
    else{
      $cntdec=0;
      while (floor(abs($lnum))!=0){
        $cntdec--;
        $lnum=$lnum/10;
      }
      $cntdec=$cntdec+2;
      $ynum=round($xnum,$cntdec);
    }
    $lsig=($xsign && ($ynum > 0)) ? "+":"";
    return $lsig.number_format($ynum,$cntdec);
  }

  function get_color($chi2value, $b1 = 1, $b2 = 4) {
    if (!is_numeric($chi2value)) return "nocolor";
    if ($chi2value < $b1){
      return "goodchi2"; //OK green class
    }
    elseif ($chi2value < $b2){
      return "midchi2"; //diff orange
    }
    else {
      return "badchi2";    //bad red
    }
  }

  function get_deltColor($delta, $dark = false) {
    if ($delta < -0.1) {
      return ($dark ? "goodDchi2" : "goodDtchi2"); //OK dark green (background) : OK green
    }
    elseif ($delta < 0.1) {
      return ($dark ? "midDchi2" : "midDtchi2"); //text white background dark : only text white
    }
    else {
      return ($dark ? "badDchi2" : "badDtchi2"); //bad dark red (background) : bad red
    }
  }


  function prepare_link($inputArray,$force = false){
    /* function to prepare link with proper count of commas and positions
     * [0] - page target
     * [1] - beam (ee, pp/ppbar)
     * [2] - process
     * [3] - observable
     * [4] - tune group
     * [5] - generator and version
     * [6] - analysis filter OR for validation: generator--tune--version--reference or
     *
     * $force - if true then comma will be ther even though value is not there
     */
    $outpuLink = "";
    $cnter = 0;
    $lsize = count($inputArray);
    //array_reverse($inputArray)
    foreach(array_reverse($inputArray) as $parameter){
      $cnter++;
      $comma = ($cnter != $lsize ? "," : "");
      $outpuLink = ($parameter != "") ? ($comma . rawurlencode($parameter) . $outpuLink)
                                      : (($outpuLink != "" || $force) ? $comma.$outpuLink : $outpuLink);
    }
    $outpuLink = ($outpuLink != "") ? "?query=" . $outpuLink : "" ;
    return $outpuLink;
  }

  echo "<div id=\"wrapper\">\n";
  echo "<div id=\"content\">\n";

$validationNote = "<p><b>Note:</b> this view is still in a development stage. Exercise care and common sense when interpreting results.</p><p><b>Use this view</b> to look for changes between generator versions (technical validation). For each version, it can also be used as a condensed summary of the overall quality of the description of the various categories of data (physics validation).</p><p>Numbers in the tables correspond to chi2 values. To make them more physically meaningful, the MC predictions are assigned a flat 5% 'theory uncertainty', as a baseline sanity limit for the achievable theoretical accuracy with present-day MC models. A few clear cases of <a href=\"http://en.wikipedia.org/wiki/Garbage_In,_Garbage_Out\">GIGO</a> are excluded, but some problematic cases remain. Thus, e.g., if a calculation returns a too small cross section for a dimensionful quantity, the corresponding chi2 value will be large, even though the shape of the distribution may be well described. It could be argued how this should be treated, how much uncertainty should be allowed for each observable, whether it is reasonable to include observables that a given model is not supposed to describe, etc. These are questions that we do not believe can be meaningfully (or reliably) addressed by a fully automated site containing tens of thousands of model/observable combinations. In the end, the interpretation of the information we display is up to you, the user.</p><p><i>&larr; Select which generator / tune you want to see the validation view for, or go back to the normal mcplots view</i>.</p>\n";

$tuneValidationNote = "<p><b>Note:</b> this view is still in a development stage. Exercise care and common sense when interpreting results.</p><p><b>Use this view</b> to compare different tunes of a generator (tuning validation). </p><p>Numbers in the tables correspond to chi2 values. To make them more physically meaningful, the MC predictions are assigned a flat 5% 'theory uncertainty', as a baseline sanity limit for the achievable theoretical accuracy with present-day MC models. A few clear cases of <a href=\"http://en.wikipedia.org/wiki/Garbage_In,_Garbage_Out\">GIGO</a> are excluded, but some problematic cases remain. Thus, e.g., if a calculation returns a too small cross section for a dimensionful quantity, the corresponding chi2 value will be large, even though the shape of the distribution may be well described. The interpretation of the information we display is up to you, the user.</p><p><i>&larr; Select which generator / version you want to see the validation view for, or go back to the normal mcplots view</i>.</p>\n"; 

 switch ($q_page) {
    case "frontpage"   : readfile("frontpage.html");   break;
    case "news"        : readfile("news.html");        break;
    case "versions"    : include("versions.php");      break;
    case "valid"       : include("valid.php");         break;
    case "validgen"    : include("validgen.php");      break;
    case "validdetail" : include("validdetail.php");   break;
    case "imgdetail"   : include("imgdetail.php");     break;
    case "plots"       : $q_process != "" ? print_plots() : readfile("frontpage.html"); break;
    default            : readfile("frontpage.html");   break;
  }

  echo "</div>\n";
  echo "</div>\n";
  echo "\n";

  echo "<div id =\"footer\">\n";
  echo "<p><span title=\"" . $t->summaryLine() . "\">Page generation took " . ms($t->elapsedFromStart()) . " ms</span> | <a href=\"dev.html\">dev</a></p>\n";
  echo "</div>";
  
  mysql_close();
?>

</body>

</html>
