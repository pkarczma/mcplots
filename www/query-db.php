<!DOCTYPE html>
<html>

<head>
  <title>Histograms query tool</title>
  <style>
    label {
      padding: 3px;
      background: #EEE;
      white-space: nowrap;
    }
    label:hover {
      background: #AAA;
    }
    input:checked + span {
      background: #8C8;
    }
    p {
      margin-top: 0em;
      margin-bottom: 0.5em;
    }
    td {
      vertical-align: top;
    }
  </style>
</head>

<body>

<h1>Histograms query tool</h1>
<p>
  <a href="#query">Query</a>
  <a href="#displayoptions">Display options</a>
  <a href="#sqlquery">SQL query</a>
  <a href="#searchresults">Search results</a>
</p>


<?php
  // start profiler
  include "profiler.php";
  $t = new Profiler();
  
  // open database
  if (!mysql_connect("localhost", "mcplots")) exit;
  mysql_select_db("mcplots");
  
  // enumerate available keys
  $keys_available = array();
  $query = mysql_query("SHOW COLUMNS FROM histograms");
  while ($row = mysql_fetch_row($query)) {
    $keys_available[] = $row[0];
  }
  
  // fields names available for search
  $keys_search = array("type", "process", "observable", "tune", "experiment", "reference", "beam", "energy", "cuts", "generator", "version");
  $keys_search = array_intersect($keys_search, $keys_available);
  
  // enumerate available values
  $vals_available = array();
  foreach ($keys_search as $key) {
    $query = mysql_query("SELECT DISTINCT $key FROM histograms ORDER BY 1");
    
    while ($row = mysql_fetch_row($query)) {
      $vals_available[$key][] = $row[0];
    }
  }
  
  $t->stamp("dbenum");
  
  // parse and sanitize input parameters
  $fields = $_GET["display"];
  $fields = array_intersect($fields, $keys_available);
  if (count($fields) == 0) $fields = array("id", "fname");
  
  $uniq = ($_GET["uniq"] == 1);
  $sort = ($_GET["sort"] == 1);
  
  $vals_query = array();
  foreach ($_GET as $key => $vals) {
    if (!in_array($key, $keys_search)) continue;
    
    foreach ($vals as $val) {
      if (!in_array($val, $vals_available[$key])) continue;
      
      $vals_query[$key][] = $val;
    }
  }
  
  $t->stamp("input");
  
  // prepare query form
  echo "<form method=\"GET\">\n";
  echo "\n";
  
  echo "<h4 id=\"query\">Query:</h4>\n";
  
  echo "<table>\n";
  
  foreach ($vals_available as $key => $vals) {
    echo "<tr>\n";
    echo "  <td>$key:</td>\n";
    
    echo "  <td>\n";
    foreach ($vals as $val) {
      $chk = (in_array($val, $vals_query[$key]) ? "checked" : "");
      echo "    <label><input type=\"checkbox\" name=\"${key}[]\" value=\"$val\" $chk><span>$val</span></label>\n";
    }
    echo "  </td>\n";
    
    echo "</tr>\n";
  }
  
  echo "</table>\n";
  echo "\n";
  
  echo "<h4 id=\"displayoptions\">Display options:</h4>\n";
  
  // which table fields to display:
  echo "<table>\n";
  echo "<tr>\n";
  echo "  <td>Fields:</td>\n";
  echo "  <td>\n";
  
  foreach ($keys_available as $name) {
    $chk = (in_array($name, $fields) ? "checked" : "");
    echo "    <label><input type=\"checkbox\" name=\"display[]\" value=\"$name\" $chk><span>$name</span></label>\n";
  }
  
  echo "  </td>\n";
  echo "</tr>\n";
  
  // display flags
  echo "<tr>\n";
  echo "  <td>Etc:</td>\n";
  echo "  <td>\n";
  echo "    <label><input type=\"checkbox\" name=\"uniq\" value=\"1\" " . ($uniq ? "checked" : "") ."><span>Display only unique combinations of selected fields</span></label><br>\n";
  echo "    <label><input type=\"checkbox\" name=\"sort\" value=\"1\" " . ($sort ? "checked" : "") ."><span>Sort results</span></label>\n";
  echo "  </td>\n";
  echo "</tr>\n";
  echo "</table>\n";
  echo "\n";
  
  echo "<input type=\"submit\" value=\"Search\">\n";
  echo "</form>\n";
  
  echo "\n";
  echo "<hr>\n";
  
  $t->stamp("form");
  
  if (count($vals_query) > 0) {
    // construct query string
    echo "<h4 id=\"sqlquery\">SQL query:</h4>\n";
    
    $parts = array();
    foreach ($vals_query as $key => $vals) {
      $parts[] = "$key IN ('" . implode("', '", $vals) . "')";
    }
    
    $qstr = "SELECT " . ($uniq ? "DISTINCT " : " ") . implode(", ", $fields) . " FROM histograms WHERE " . implode(" AND ", $parts) . ($sort ? " ORDER BY " . implode(", ", range(1, count($fields))) : "");
    echo $qstr . "<br>\n";
    
    echo "\n";
    echo "<hr>\n";
    
    // run search query
    echo "<h4 id=\"searchresults\">Search results:</h4>\n";
    
    $query = mysql_query($qstr);
    
    while ($row = mysql_fetch_assoc($query)) {
      foreach ($row as $col => $val) {
        if ($col == "fname")
          echo "<a href=\"$val\">$val</a> ";
        else
          echo "$val ";
      }
      echo "<br>\n";
    }
    
    echo "\n";
    echo "<hr>\n";
  }
  
  $t->stamp("query");
  
  echo "<span title=\"" . $t->summaryLine() . "\">Page generation took: " . ms($t->elapsedFromStart()) . " ms</span>\n";
  
  mysql_close();
?>

</body>

</html>
