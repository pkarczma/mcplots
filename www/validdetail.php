<?php

  //$q_beamgroup, $q_process, $q_generator, $q_tune, $q_version, $q_ref from index.php
  $isData = ($q_ref == "data");
  //$isData variable is false when comparing 2 generators
  $ltext = $isData ? "" : "vs. $q_ref";
  
  $gNew=$q_version;
  $gOld=$q_ref; // this doesn't need to be generator

  $shownote=false;
  $shownote2=false;

  function err_handle($chi2) {
    global $shownote;
    global $shownote2;
    
    //distinguish different bin size error
    if ($chi2 >= 0) return $chi2; // no error
    elseif ($chi2 == -1) {
      $shownote = true;
      return "n/a*";
    }
    elseif($chi2 == -2) {
      $shownote2 = true;
      return "n/a**";
    }
    else {
      return $chi2;
    }
  }

  $dispproc = $c->name($q_process);
  $dispgen = $c->name($q_tune, $q_generator);
  echo "<h2>Detail for $dispgen $q_version $ltext</h2>\n";
  echo $validationNote;

  $param = $q_generator.$safeDelimiter.$q_tune;
  echo "<h3>" . $c->name($q_beamgroup) . " &rarr; $dispproc</h3>\n";

  //prepare sql condition
  $beams = $c->beamgroups[$q_beamgroup];
  $beamslist = "'" . implode("', '", $beams) . "'";


  //get data from database
  $query = mysql_query("SELECT *
                        FROM histograms
                        WHERE type='data' AND process='$q_process' AND beam in ($beamslist)
                        ORDER BY observable, cuts, energy DESC");

  $mydata = array();
  //process sql result into array
  while ($row = mysql_fetch_assoc($query)) {
    $observable  = $row["observable"];
    $energy      = $row["energy"];
    $cuts        = $row["cuts"];


    $mydata[$observable][$cuts][$energy][] = $row;
  }

  $query = mysql_query("SELECT *
                        FROM histograms
                        WHERE generator = '$q_generator' AND tune = '$q_tune'
                          AND version = '$q_version'
                          AND process = '$q_process' AND beam in ($beamslist)
                        ORDER BY observable, cuts, energy DESC");
  

  $mytable = array();
  $mytheader = array();
  //process sql result into array
  while ($row = mysql_fetch_assoc($query)) {
    $observable  = $row["observable"];
    $energy      = $row["energy"];
    $cuts        = $row["cuts"];

    $mytable[$observable][$cuts][$energy][] = $row;
    $mytheader[$energy] = 0;
  }

  if (!$isData){
    //selecting data for old (q_ref) version of generator
    $query = mysql_query("SELECT *
                        FROM histograms
                        WHERE generator = '$q_generator' AND tune = '$q_tune'
                          AND version = '$q_ref'
                          AND process = '$q_process' AND beam in ($beamslist)
                        ORDER BY observable, cuts, energy DESC");


    $mytableOld = array();
    //process sql result into array
    while ($row = mysql_fetch_assoc($query)) {
      $observable  = $row["observable"];
      $energy      = $row["energy"];
      $cuts        = $row["cuts"];


      $mytableOld[$observable][$cuts][$energy][] = $row;
    }
  }

  //sorting array descneding > so we have energies prepared from highest to lowest
  $sortedthead = array_keys($mytheader);
  rsort($sortedthead);
  
  //table begin
  echo "<table class=\"validation\">\n";

  //table Header begin
  echo "  <tr>\n";
  echo "    <th class=\"dmid medText bott-bord-thick\">Observable</th>\n";
  echo "    <th class=\"dmid medText bott-bord-thick\">Cut</th>\n";
  echo "    <th class=\"dmid medText bott-bord-thick\">Energy</th>\n";
  if (!$isData) {
    echo "    <th class=\"dmid medText bott-bord-thick left-bord-thick\">&chi;<sup>2</sup> ($gNew)</th>\n";
    echo "    <th class=\"mcmid medText bott-bord-thick\" >&Delta;</th>\n";
    echo "    <th class=\"dmid medText bott-bord-thick\" >&chi;<sup>2</sup> ($gOld)</th>\n";
  }
  else {
    echo "    <th class=\"dmid medText bott-bord-thick left-bord-thick\">&chi;<sup>2</sup></th>\n";
  }
  echo "  </tr>\n";
  //table Header end

  //table body begin >>>>>
  //loop through observables and cuts (creating rows)
  foreach (array_keys($mytable) as $observable ){
    $ldispObs="";
    $lcuts="";
    foreach (array_keys($mytable[$observable]) as $cuts){
      foreach ($sortedthead as $energy) {
        $row=$mytable[$observable][$cuts][$energy][0];
        //first case generator comparison
        if (!$isData){
          $rowOld=$mytableOld[$observable][$cuts][$energy][0];
          if (($row["fname"] == "") || ($rowOld["fname"] == "")) continue;
        }
        //second case data comparison
        else {
          if ($row["fname"] == "") continue;
        }
        //validation row begin common for both cases >>>>
        echo "  <tr>\n";
        $dispObs = $c->name($observable, $q_process);
        $dispCut = $c->name($cuts);
        if ($ldispObs == $dispObs){
          $ldispObs="";
          $tdclass="mid-bord";
        }
        else{
          $ldispObs=$dispObs;
          $tdclass="end-bord";
        }
        echo "    <td class=\"mn $tdclass\"> $ldispObs </td>\n";
        echo "    <td class=\"mn left-bord-thin\"> $dispCut </td>\n";
        $ldispObs=$dispObs; //store for next loop

        echo "    <td class=\"mn left-bord-thin\"> $energy </td>\n";

        $pair=array("act" => $gNew, "ref" => "data");
        $lcut=$row["cuts"];
        
        $fname = str_replace(" ", "_", "$q_generator-$q_tune-$q_beamgroup-$q_process-$observable-$energy-$lcut-".$pair["act"]."-".$pair["ref"]);
        $fname = str_replace("/", "_", $fname);
        $fname = str_replace("*", "x", $fname);
        $txtf = "cache/plots/$fname.txt";
        $lines = file_get_contents($txtf);
        list($chi2val, ) = explode(";", $lines, 2);
        $chi2val = err_handle($chi2val);
        
        //first column of chi2 values (if only data then there is only this one column
        //if comparison then this column and two other)
        $rightBorder=($isData ? "right-bord-thick" : "");
        echo "    <td class=\"mn $rightBorder " . get_color($chi2val) . "\">";
        $param = $q_generator.$safeDelimiter.$q_tune.$safeDelimiter.$pair["act"].$safeDelimiter.$pair["ref"];
        printf("<a class=\"clblack\" href=\"%s\">%s</a></td>\n",
                prepare_link(array("imgdetail",$q_beamgroup,$q_process,$fname,$q_valid_gtvr,"",$param)),
                spec_format($chi2val));

        //adding 2 other columns (delta and previous version of generator
        if (!$isData){
          $pair=array("act" => $gOld, "ref" => "data");
          //mc1 mc2 data comparison steer file
          $fname_t = str_replace(" ", "_", "$q_generator-$q_tune-$q_beamgroup-$q_process-$observable-$energy-$lcut-".$gNew."-".$gOld);
          $fname_t = str_replace("/", "_", $fname_t);
          $fname_t = str_replace("*", "x", $fname_t);

          $fname = str_replace(" ", "_", "$q_generator-$q_tune-$q_beamgroup-$q_process-$observable-$energy-$lcut-".$pair["act"]."-".$pair["ref"]);
          $fname = str_replace("/", "_", $fname);
          $fname = str_replace("*", "x", $fname);
          $txtf = "cache/plots/$fname.txt";
          $lines = file_get_contents($txtf);
          list($chi2valOld, ) = explode(";", $lines, 2);
          $chi2valOld = err_handle($chi2valOld);
          
          if (is_numeric($chi2val) && is_numeric($chi2valOld)){
            $dif = $chi2val - $chi2valOld;
            $dchi=((abs($dif) < 1000) ? ((spec_format($chi2val))-(spec_format($chi2valOld))) : ($dif)); //lastavg is avg from previous
          }
          else{
            $dchi="-";
          }
          //delta
          echo "    <td class=\"mcmid medText notBold\">";
          $param = $q_generator.$safeDelimiter.$q_tune.$safeDelimiter.$gNew.$safeDelimiter.$gOld;
          printf("<a class=\"%s\" href=\"%s\">%s</a></td>\n",
                  get_deltColor($dchi),
                  prepare_link(array("imgdetail",$q_beamgroup,$q_process,$fname_t,$q_valid_gtvr,"",$param)),
                  spec_format($dchi,true));
                    
          //second generator
          echo "    <td class=\"mn ".get_color($chi2valOld)." right-bord-thick\">";
          $param = $q_generator.$safeDelimiter.$q_tune.$safeDelimiter.$pair["act"].$safeDelimiter.$pair["ref"];
          printf("<a class=\"clblack\" href=\"%s\">%s</a></td>\n",
                  prepare_link(array("imgdetail",$q_beamgroup,$q_process,$fname,$q_valid_gtvr,"",$param)),
                  spec_format($chi2valOld));
        }
        echo "  </tr>\n";
      }
      
      //columns of a row end <<<
      //validation row end <<<<
    }
  }
  echo "</table>\n";
  echo "<br />\n";
  if ($shownote){
   echo "*  not applicable: because number of bins of data histogram is different than number of bins of theory histogram <br/>";
  }
  if ($shownote2){
   echo "** not applicable: because there is not any valid bin<br/>";
  }
  //tablebody end <<<<<
 
?>
