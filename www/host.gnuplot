# host performance
set terminal png size 650,200
set output "cache/stats/$2.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %d"
set ylabel "#jobs/day"
set grid xtics ytics
set key left top

plot ["$0":"$1"] "cache/stats/$2.txt" using 1:4 with linespoints lt 3 pt 4 ps 0.5 title "total", \
                 "cache/stats/$2.txt" using 1:3 with linespoints lt 1 pt 4 ps 0.5 title "failed"
