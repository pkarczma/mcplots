<?php
  header("Content-Type: application/json");
  header("Access-Control-Allow-Origin: *");
  header("Cache-Control: max-age=1800");
  
  // database queries in PHP < 5.3 always return all fields as strings
  // this function is intended to do type-conversion to integers if applicable
  // TODO: this is a work-around, which will not be necessary with PHP >= 5.3 + mysqlnd,
  //       to be removed after update to CC7
  // See also: http://stackoverflow.com/questions/2317061/mysql-query-preserve-the-data-type-created-in-the-table-on-return
  function int_cast($a) {
    foreach ($a as $k => $v)
      if (is_numeric($v))
        settype($a[$k], 'int');
      else if (is_array($v))
        $a[$k] = int_cast($v);
    
    return $a;
  }
  
  function action_top_users() {
    $nusers = $_GET["top_users"];
    $key = "n_events"; // default sort key
    if (array_key_exists("key", $_GET))
      $key = $_GET["key"];
    
    // sanity check
    if (! is_numeric($nusers)) return;
    if ($nusers > 100) return;
    if (! in_array($key, array("cpu_time", "n_events", "n_jobs", "n_good_jobs", "n_hosts"))) return;
    
    $query = "SELECT
                system,
                userid AS user_id,
                SUM(cpu_time) AS cpu_time,
                SUM(n_events) AS n_events,
                SUM(n_jobs) AS n_jobs,
                SUM(n_good_jobs) AS n_good_jobs,
                COUNT(hostid) AS n_hosts
             FROM
               api
             GROUP BY 1, 2
             ORDER BY $key DESC
             LIMIT $nusers";
    $query = mysql_query($query);
    
    $list = array();
    
    while ($row = mysql_fetch_assoc($query))
      $list[] = int_cast($row);
    
    echo json_encode($list);
  }
  
  function action_user() {
    $user = $_GET["user"];
    
    if (strpos($user, "-") !== false) {
      list($system, $userid) = explode("-", $user);
    }
    else {
      // compatibility: 'system' could be missing
      $system = 1;
      $userid = $user;
    }
    
    // sanity check
    if (! is_numeric($system)) return;
    if (! is_numeric($userid)) return;
    
    // set buffer size of GROUP_CONCAT function to hold at least 1000 hostids
    mysql_query("SET group_concat_max_len = 8192");
    
    // get user stats
    $query = "SELECT
                system,
                userid AS user_id,
                SUM(cpu_time) AS cpu_time,
                SUM(n_events) AS n_events,
                SUM(n_jobs) AS n_jobs,
                SUM(n_good_jobs) AS n_good_jobs,
                COUNT(hostid) AS n_hosts,
                GROUP_CONCAT(hostid) AS hosts
             FROM
               api
             WHERE
               system = $system AND userid = $userid
             HAVING
               system IS NOT NULL";
    $query = mysql_query($query);
    if ($row = mysql_fetch_assoc($query)) {
      $row["hosts"] = ($row["hosts"] != "") ? explode(",", $row["hosts"]) : array();
      echo json_encode(int_cast($row));
    }
    else
      echo "{}";
  }
  
  function action_host() {
    $host = $_GET["host"];
    list($system, $userid, $hostid) = explode("-", $host);
    
    // sanity check
    if (! is_numeric($system)) return;
    if (! is_numeric($userid)) return;
    if (! is_numeric($hostid)) return;
    
    $query = "SELECT
                system,
                userid AS user_id,
                hostid AS host_id,
                cpu_time,
                n_events,
                n_jobs,
                n_good_jobs
             FROM
               api
             WHERE
               system = $system AND userid = $userid AND hostid = $hostid";
    $query = mysql_query($query);
    if ($row = mysql_fetch_assoc($query))
      echo json_encode(int_cast($row));
    else
      echo "{}";
  }
  
  function action_totals() {
    $query = "SELECT
                SUM(cpu_time) AS cpu_time,
                SUM(n_events) AS n_events,
                SUM(n_jobs) AS n_jobs,
                SUM(n_good_jobs) AS n_good_jobs,
                COUNT(DISTINCT system, userid) AS n_users,
                COUNT(*) AS n_hosts
             FROM
               api";
    $query = mysql_query($query);
    if ($row = mysql_fetch_assoc($query))
      echo json_encode(int_cast($row));
    else
      echo "{}";
  }
  
  function action_achievement() {
    $key = $_GET["achievement"];
    $value = $_GET["value"];
    
    // sanity check
    if (! in_array($key, array("n_events"))) return;
    switch ($value) {
      case  "1000000000": $fname =  "date1G"; break;
      case "10000000000": $fname = "date10G"; break;
      default: return; break;
    }
    
    // get list of users
    $query = "SELECT
                system,
                userid AS user_id,
                SUM(n_events) AS n_events,
                $fname AS date
              FROM
                api
              GROUP BY 1, 2
              HAVING $key > $value";
    $query = mysql_query($query);
    
    $users = array();
    while ($row = mysql_fetch_assoc($query))
      $users[] = $row;
    
    echo json_encode(int_cast($users));
  }
  
  // open database
  if (!mysql_connect("localhost", "mcplots")) exit;
  mysql_select_db("mcplots");
  
  // get API function name
  $params = array_keys($_GET);
  
  switch ($params[0]) {
    case "top_users"   : action_top_users();   break;
    case "user"        : action_user();        break;
    case "host"        : action_host();        break;
    case "totals"      : action_totals();      break;
    case "achievement" : action_achievement(); break;
  }
  
  mysql_close();
?>
